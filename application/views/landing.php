<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Fabiana Moura - Projetos Personalizados</title>
    <meta name="description" content="Empresa de decoração e projetos personalizados de eventos sociais adultos, casamentos e festas infantis.">
    <meta name="keywords" content="Decoração, Eventos, Festas, Casamento, Debutante, Infantil, Projeto, Personalizado, Aniversário" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2012 Trupe Design" />

    <meta name="viewport" content="width=device-width,initial-scale=0.9,maximum-scale=0.9">

  <meta property="og:title" content="Fabiana Moura - Projetos Personalizados"/>
  <meta property="og:site_name" content="Fabiana Moura - Projetos Personalizados"/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="<?=base_url('_imgs/layout/compartilhamento.jpg')?>"/>
  <meta property="og:url" content="<?=base_url()?>"/>
  <meta property="og:description" content="Empresa de decoração e projetos personalizados de eventos sociais adultos, casamentos e festas infantis."/>

    <base href="<?= base_url() ?>">
    <script> var BASE = '<?= base_url() ?>'</script>

    <?CSS(array('reset', 'fontface/stylesheet', 'landing'))?>  
    

    <?if(ENVIRONMENT == 'development'):?>
      
      <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.8.0.min'))?>
      
    <?else:?>

      <?JS(array('modernizr-2.0.6.min'))?>
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
      <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

    <?endif;?>

  </head>
  <body>

  <div id="links">

    <div id="logo"></div>

    <div class="link infantil"></div>

    <a href="infantil" id="inf" title="Festas Infantis">
      <div class="texto infantil">Festas<br>Infantis</div>
      <div class="seta infantil"></div>
    </a>

    <div class="faixa-escura infantil"></div>

    <div class="link adulto"></div>

    <a href="adulto" id="adu" title="Eventos Adultos">
      <div class="texto adulto">Eventos<br>Adultos</div>
      <div class="seta adulto"></div>
    </a>

    <div class="faixa-escura adulto"></div>

  </div>  
  
<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36836454-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>
  
  </body>
</html>