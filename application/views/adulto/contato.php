<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">
	<div class="centro">

		<div id="contato">

			<div id="form">

				<div class="resposta">
					Sua mensagem foi enviada!<br>
					Entraremos em contato assim que possível. Obrigado!
				</div>

				<form method="post" action="infantil/contato/enviar" id="form-contato">

					<div class="coluna">

						<input type="text" name="nome" placeholder="Nome" id="input-nome" required>

						<input type="email" name="email" placeholder="E-mail" required id="input-email">

						<input type="text" name="telefone" placeholder="Telefone" id="input-telefone">

						<input type="text" name="tipo" placeholder="Tipo de Evento" id="input-tipo">

					</div>

					<div class="coluna">

						<textarea name="mensagem" placeholder="Mensagem" id="input-mensagem"></textarea>

					</div>

					<div class="large">
						<input type="submit" value="ENVIAR &raquo;">
					</div>

				</form>

			</div>

			<?php if ($info[0]->telefone): ?>
				<div id="telefone">
					<?=$info[0]->telefone?>
				</div>
			<?php endif ?>

			<?php if ($info[0]->email): ?>
				<div id="email">
					<a href="mailto:<?=$info[0]->email?>" title="Entre em Contato"><?=$info[0]->email?></a>
				</div>
			<?php endif ?>

			<?php if ($info[0]->facebook): ?>
				<div id="facebook">
					<a href="<?=prep_url($info[0]->facebook)?>" target="_blank" title="Nossa página no Facebook">Curta nossa página no Facebook! &raquo;</a>
				</div>	
			<?php endif ?>

		</div>

		<div id="imagem-footer"></div>

	</div>
</div>