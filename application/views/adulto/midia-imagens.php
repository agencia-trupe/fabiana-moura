<?php if ($qry): ?>

	<div class="imagens">

		<?php foreach ($qry as $key => $value): ?>
			<img src="<?=base_url('_imgs/midia/'.$origem.'/'.$value->imagem)?>">		
		<?php endforeach ?>
	
	</div>

<?php else: ?>

	<h2>Nenhuma imagem encontrada</h2>

<?php endif; ?>

<style type="text/css">

	h2{
		font-family: 'Verdana', sans-serif;
		font-size:20px;
		color:#820F52;
		text-align:center;
		display: block;
	}
	.imagens{
		font-size:0;
	}
	.imagens img{
		margin:5px 0;
	}

</style>