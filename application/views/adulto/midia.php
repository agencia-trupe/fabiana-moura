<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">
	<div class="centro">

		<?php if ($lista_midia): ?>

			<div id="midia-container">

				<div id="animate">
					<div class="pagina-midia">

						<?php foreach ($lista_midia as $key => $value): ?>

							<?php $classe = (($key + 1)%5 == 0 && $key > 0) ? ' nomargin' : '' ?>

							<?php if ($value->externo): ?>
								<a href="<?=$value->externo?>" target="_blank" title="<?=$value->titulo?>" class="<?=$classe?>">	
							<?php else: ?>
								<a href="home/imagensMidia/<?=$value->id?>" title="<?=$value->titulo?>" class="fancy-modal<?=$classe?>">
							<?php endif ?>
								<img src="_imgs/midia/adulto/capas/<?=$value->imagem?>">
								<div class="texto">
									<h2>
										<?=$value->titulo?>
									</h2>
									<div class="data">
										<?=mes($value->data, true).'/'.ano($value->data)?>
									</div>
								</div>
							</a>

							<?php if(($key + 1)%10 == 0) echo "</div><div class='pagina-midia' style='display:none;'>" ?>
							
						<?php endforeach ?>

					</div>
				</div>

				<div id="navegacao">
					<a href="#" title="Página Anterior" id="nav-Prev">&laquo;</a>
					<a href="#" title="Próxima Página" id="nav-Next">&raquo;</a>
				</div>

			</div>
			
		<?php endif ?>

	</div>
</div>