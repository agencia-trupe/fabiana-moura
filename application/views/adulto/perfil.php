<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">
	
	<div class="centro">

		<div id="texto-1">
			<h1>A EMPRESA</h1>
			<p>
				<?=$textos[0]->texto_empresa?>
			</p>
		</div>

		<div id="texto-2">

			<div class="coluna">
				<h1>FABIANA MOURA</h1>
				<p>
					<?=nl2br($textos[0]->texto_fabiana)?>
				</p>
			</div>

			<div class="imagem">
				<img src="_imgs/perfil/adulto/<?=$textos[0]->imagem?>" alt="Fabiana Moura">
			</div>

		</div>

	</div>

	<div class="faixa-marrom apagado">
		<div class="centro">
			<p>
				<?=$textos[0]->texto_outro?>
			</p>
		</div>
	</div>

</div> <!-- fim da div main -->	