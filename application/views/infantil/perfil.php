<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">
	<div class="centro">

		<div id="texto-1" class="texto">
			<div class="fore">
				<h1>A EMPRESA</h1>
				<p>
					<!-- Seguindo uma forte tendência do mercado, a empresa oferece projetos de decoração totalmente personalizados e exclusivos traduzindo com muito cuidado e dedicação os sonhos e desejos de cada cliente. Tudo para fugir do óbvio e evitar reprises, os projetos são pensados em cada detalhe, sempre respeitando gostos e tendências sendo concretizados em ambientações mágicas e encantadoras. Prezamos por um atendimento acolhedor e pessoal e por um resultado de qualidade e excelência. -->
					<?=nl2br($textos[0]->texto_empresa)?>
				</p>
			</div>
			<div class="back"></div>
		</div>

		<div id="texto-2" class="texto">
			<div class="fore">
				<div class="img" style="background:url('_imgs/perfil/infantil/<?=$textos[0]->imagem?>') center center no-repeat"></div>
			</div>
			<div class="back"></div>
		</div>

		<div id="texto-3" class="texto">
			<div class="fore">
				<p>
					<!-- Nossos projetos<br> seguem uma linha de design<br> diferenciada; são projetos<br> realizados com uma identidade<br> visual, com um conceito atrelado, com<br> o cuidado sobre o detalhes obtendo<br> assim um resultado muito<br> original, harmônico e de<br> bom gosto. -->
					<?=nl2br($textos[0]->texto_outro)?>
				</p>
			</div>
			<div class="back"></div>
		</div>

		<div id="texto-4" class="texto">
			<div class="fore">
				<h1>FABIANA MOURA</h1>
				<p>
					<!--
					A vocação de Fabiana para o universo das artes e criação foi estimulada desde a infância, quando esteve envolvida em cursos de música, dança e artes plásticas. Cultivou sempre um interesse particular pela decoração direcionando facilmente sua futura opção profissional. Formada pela faculdade de Arquitetura e Urbanismo Mackenzie, iniciou sua carreira atuando na áera de design de interiores quando em 2009 migrou para o segmento de decoração de eventos onde encontrou sua verdadeira paixão.<br>
					Hoje, Fabiana realiza-se plenamente transformando a celebração de datas importantes em eventos especiais e inesquecíveis.
					-->
					<?=nl2br($textos[0]->texto_fabiana)?>
				</p>
			</div>
			<div class="back"></div>
		</div>

	</div> <!-- fim da div centro -->
</div> <!-- fim da div main -->	