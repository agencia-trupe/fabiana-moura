<header class="header header-<?=$this->router->class?>">

	<div id="banner-topo"><div class="centro"><a href="<?=base_url()?>" title="Página Inicial">Página Inicial</a></div></div>

	<nav>
		<ul>
			<li><a href="infantil/home" title="Página Inicial" id="mn-home" <?if($this->router->class=='home')echo" class='ativo'"?>>HOME</a></li>
			<li><a href="infantil/perfil" title="Perfil" id="mn-perfil" <?if($this->router->class=='perfil')echo" class='ativo'"?>>PERFIL</a></li>
			<li><a href="infantil/projetos" title="Projetos Personalizados" id="mn-projetos" <?if($this->router->class=='projetos')echo" class='ativo'"?>>PROJETOS PERSONALIZADOS</a></li>
			<li><a href="infantil/midia" title="Na Mídia" id="mn-midia" <?if($this->router->class=='midia')echo" class='ativo'"?>>NA MÍDIA</a></li>
			<li><a href="http://fabianamouraprojetospersonalizados.blogspot.com.br/" target="_blank" title="Blog" id="mn-blog" <?if($this->router->class=='blog')echo" class='ativo'"?>>BLOG</a></li>
			<li><a href="infantil/contato" title="Contato" id="mn-contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>CONTATO</a></li>
		</ul>		
	</nav>
	
</header>