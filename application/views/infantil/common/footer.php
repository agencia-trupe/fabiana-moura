
	<footer>
		<div class="centro">
			<div class="f-left">
				&copy; <?=Date('Y')?> Fabiana Moura - Todos os direitos reservados
			</div>
			<div class="f-right">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</div>
	</footer>
  
<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36836454-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>

  <?JS(array('cycle', 'fancybox', 'infantil'))?>
  
</body>
</html>
