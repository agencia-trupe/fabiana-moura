<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">
	<div class="centro">

		<div id="lateral">
			
			<ul>
				<?php foreach ($lista_projetos as $key => $value): ?>
					<li>
						<a href="infantil/projetos/index/<?=$value->slug?>" title="<?=$value->titulo?>" data-target="<?=$value->id?>" <?if($marcar && $marcar == $value->slug)echo" class='ativo'"?>><?=$value->titulo?></a>
					</li>	
				<?php endforeach ?>
			</ul>

			<div id="thumbs">

				<div id="imagens">
					
				</div>
				
				<div id="navegacao">
					<a href="" title="Página Anterior" id="nav-Prev">&laquo;</a>
					<a href="" title="Próxima Página" id="nav-Next">&raquo;</a>
				</div>
			</div>

		</div>

		<div id="ampliada">
			
		</div>

	</div>
</div>