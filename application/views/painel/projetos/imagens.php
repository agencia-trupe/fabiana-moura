<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index/infantil')?>" class="lista<?if($parent->area=='infantil')echo" active"?>">Projetos - Infantil</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/index/adulto')?>" class="lista<?if($parent->area=='adulto')echo" active"?>">Projetos - Adulto</a>	
</div>

<?php if (isset($registro) && $registro): ?>

	<form id="form-img" method="post" action="<?='painel/'.$this->router->class.'/editarImagem/'.$registro->id?>" enctype="multipart/form-data">

		<label>
			Imagem Atual<br>
			<img src="_imgs/fotos/<?=$registro->imagem?>"><br>
			Substituir Imagem<br>
			<input type="file" id="input-img" name="userfile">
		</label>

		<input type="hidden" name="id_parent" value="<?=$parent->id?>">
		 
		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?php else: ?>

	<form method="post" id="form-img" action="<?='painel/'.$this->router->class.'/inserirImagem/'.$parent->area?>" enctype="multipart/form-data">

		<label>
			Imagem<br>
			<input type="file" id="input-img" name="userfile">
		</label>

		<input type="hidden" name="id_parent" value="<?=$parent->id?>">
		
		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?php endif ?>

<br>

<?if($imagens):?>

	<table>

		<thead>
			<tr>
				<th>Imagens</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<tbody>

			<? foreach ($imagens as $key => $value): ?>

				<tr id="row_<?=$value->id?>">
					<td><img src="_imgs/projetos/<?=$parent->area?>/thumb/<?=$value->imagem?>"></td>
					<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluirImagem/'.$value->id.'/'.$parent->id)?>">Excluir</a></td>
					<td><a href="#" data-id="<?=$value->id?>" class="move">mover</a></td>
				</tr>
				
			<? endforeach; ?>

		</tbody>

	</table>

<?endif;?>

<style type="text/css">
	table tbody tr{
		width:100%;
	}
</style>

<script defer>

	$('document').ready( function(){

	    $("table tbody").sortable({
	        update : function () {
	            serial = [];
	            $('table tbody').children('tr').each(function(idx, elm) {
	                serial.push(elm.id.split('_')[1])
	            });
	            $.post(BASE+'/ajax/gravaOrdem', { data : serial , tabela : 'projetos_imagens', origem : '<?=$parent->area?>'}, function(retorno){
	            	//console.log(retorno);
	            });
	        },
	        helper: function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			},
			handle : $('.move')
	    }).disableSelection();

	});
</script>