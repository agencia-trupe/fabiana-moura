<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index/infantil')?>" class="lista<?if($origem=='infantil')echo" active"?>">Projetos - Infantil</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/index/adulto')?>" class="lista<?if($origem=='adulto')echo" active"?>">Projetos - Adulto</a>	
</div>


<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Título</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td><?=$value->titulo?></td>
				<td><a class="imagens" href="<?=base_url('painel/'.$this->router->class.'/imagens/'.$value->id)?>">Imagens</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhum Projeto</h2>

<?endif;?>