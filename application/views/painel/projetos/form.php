<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index/infantil')?>" class="lista<?if($registro->area=='infantil')echo" active"?>">Slides da Home - Infantil</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/index/adulto')?>" class="lista<?if($registro->area=='adulto')echo" active"?>">Slides da Home - Adulto</a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Texto - Empresa<br>
			<textarea name="texto_empresa" style="height:150px;"><?=$registro->texto_empresa?></textarea>
		</label>

		<label>Texto - Fabiana Moura<br>
			<textarea name="texto_fabiana" style="height:150px;"><?=$registro->texto_fabiana?></textarea>
		</label>

		<label>Texto - Outro<br>
			<textarea name="texto_outro" style="height:150px;"><?=$registro->texto_outro?></textarea>
		</label>

		<label>Imagem<br>
			<?php if ($registro->imagem): ?>
				<img src="_imgs/perfil/<?=$registro->area?>/<?=$registro->imagem?>"><br>	
			<?php endif ?>
			<input type="file" name="userfile">
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Texto - Empresa<br>
			<textarea name="texto_empresa" style="height:150px; resize:none;"></textarea>
		</label>

		<label>Texto - Fabiana Moura<br>
			<textarea name="texto_fabiana" style="height:150px; resize:none;"></textarea>
		</label>

		<label>Texto - Outro<br>
			<textarea name="texto_outro" style="height:150px; resize:none;"></textarea>
		</label>

		<label>Imagem<br>
			<input type="file" name="userfile" required>
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>