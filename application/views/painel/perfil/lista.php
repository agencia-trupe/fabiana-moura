<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index/infantil')?>" class="lista<?if($origem=='infantil')echo" active"?>">Texto de Perfil - Infantil</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/index/adulto')?>" class="lista<?if($origem=='adulto')echo" active"?>">Texto de Perfil - Adulto</a>	
</div>


<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Texto</th>
				<th>Imagem</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td><?=word_limiter($value->texto_empresa, 15)?></td>
				<td><img src="_imgs/perfil/<?=$origem?>/<?=$value->imagem?>"></td>
				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhum Texto</h2>

<?endif;?>