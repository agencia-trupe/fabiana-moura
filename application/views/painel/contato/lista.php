<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar <?=$titulo?></a>
</div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Telefone</th>
				<th>E-mail</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td><?=$value->telefone?></td>
				<td><?=$value->email?></td>
				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhuma Informação de Contato Cadastrada</h2>

<?endif;?>