<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index/infantil')?>" class="lista">Slides da Home - Infantil</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/index/adulto')?>" class="lista">Slides da Home - Adulto</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir Slide</a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Site<br>
			<select disabled>
				<option value="infantil" <?if($registro->area=='infantil')echo" selected"?>>Infantil</option>
				<option value="adulto" <?if($registro->area=='adulto')echo" selected"?>>Adulto</option>
			</select>
		</label>

		<input type="hidden" name="area" value="<?=$registro->area?>">

		<label>Imagem<br>
			<img src="_imgs/home/<?=$registro->area?>/<?=$value->imagem?>"><br>
			<input type="file" name="userfile">
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Site<br>
			<select name="area" required>
				<option value="infantil">Infantil</option>
				<option value="adulto">Adulto</option>
			</select>
		</label>

		<label>Imagem<br>
			<input type="file" name="userfile" required>
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>