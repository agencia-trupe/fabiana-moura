<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index/infantil')?>" class="lista<?if($origem=='infantil')echo" active"?>">Slides da Home - Infantil</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/index/adulto')?>" class="lista<?if($origem=='adulto')echo" active"?>">Slides da Home - Adulto</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Slide</a>
</div>


<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Imagem</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr id="row_<?=$value->id?>">
				<th><img src="_imgs/home/<?=$value->area?>/<?=$value->imagem?>" style="width:150px"></th>
				<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
				<td><a href="#" data-id="<?=$value->id?>" class="move">mover</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhum Slide</h2>

<?endif;?>

<style type="text/css">
	table tbody tr{
		width:100%;
	}
</style>

<script defer>

	$('document').ready( function(){

	    $("table tbody").sortable({
	        update : function () {
	            serial = [];
	            $('table tbody').children('tr').each(function(idx, elm) {
	                serial.push(elm.id.split('_')[1])
	            });
	            $.post(BASE+'/ajax/gravaOrdem', { data : serial , tabela : 'slides', origem : '<?=$origem?>'}, function(retorno){
	            	//console.log(retorno);
	            });
	        },
	        helper: function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			},
			handle : $('.move')
	    }).disableSelection();

	});
</script>