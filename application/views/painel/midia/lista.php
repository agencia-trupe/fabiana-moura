<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index/infantil')?>" class="lista<?if($origem=='infantil')echo" active"?>">Mídia - Infantil</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/index/adulto')?>" class="lista<?if($origem=='adulto')echo" active"?>">Mídia - Adulto</a>	
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Item de Mídia</a>	
</div>


<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Título</th>
				<th>Data</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td><?=$value->titulo?></td>
				<td><?=mes($value->data, TRUE).' '.ano($value->data)?></td>
				<?php if ($value->externo): ?>
					<td><a href="<?php echo $value->externo ?>" target="_blank">Link Externo</a></td>
				<?php else: ?>
					<td><a class="imagens" href="<?=base_url('painel/'.$this->router->class.'/imagens/'.$value->id)?>">Imagens</a></td>
				<?php endif ?>
				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
				<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhum Item</h2>

<?endif;?>