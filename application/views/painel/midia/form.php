<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index/infantil')?>" class="lista">Mídia - Infantil</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/index/adulto')?>" class="lista">Mídia - Adulto</a>	
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir Item de Mídia</a>	
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Site<br>
			<select disabled>
				<option value="infantil" <?if($registro->area=='infantil')echo" selected"?>>Infantil</option>
				<option value="adulto" <?if($registro->area=='adulto')echo" selected"?>>Adulto</option>
			</select>
		</label>

		<input type="hidden" name="area" value="<?=$registro->area?>">

		<label>Título<br>
			<input type="text" name="titulo" required value="<?=$registro->titulo?>">
		</label>

		<label>Data<br>
			<input type="text" id="monthpicker" name="data" required  value="<?=mes($registro->data, false, true).'/'.ano($registro->data)?>">
		</label>

		<label>Link externo (preencher somente caso o Item de Mídia for um link externo)<br>
			<input type="text" name="externo" value="<?=$registro->externo?>">
		</label>		

		<label>Imagem de Capa<br>
			<?php if ($registro->imagem): ?>
				<img src="_imgs/midia/<?=$registro->area?>/capas/<?=$registro->imagem?>"><br>	
			<?php endif ?>
			<input type="file" name="userfile_capa">
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Site<br>
			<select name="area" required>
				<option value="infantil">Infantil</option>
				<option value="adulto">Adulto</option>
			</select>
		</label>

		<label>Título<br>
			<input type="text" name="titulo" required>
		</label>

		<label>Data<br>
			<input type="text" id="monthpicker" name="data" required>
		</label>

		<label>Link externo (preencher somente caso o Item de Mídia for um link externo)<br>
			<input type="text" name="externo">
		</label>

		<label>Imagem de Capa<br>
			<input type="file" name="userfile_capa" required>
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>