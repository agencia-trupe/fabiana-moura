<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Midia extends MY_Frontcontroller {

   function __construct(){
   	parent::__construct('A');
   }

   function index(){

		$data['lista_midia'] = $this->db->order_by('data', 'desc')->get_where('midia', array('area' => 'adulto'))->result();

		if(!isset($data['lista_midia'][0]))
			return false;

		$this->load->view('adulto/midia', $data);
   }
}