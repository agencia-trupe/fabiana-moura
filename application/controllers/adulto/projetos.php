<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projetos extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct('A');
   }

   function index($abrir = false){

   		$data['lista_projetos'] = $this->db->get_where('projetos', array('area' => 'adulto'))->result();

   		if(!$abrir)
   			$abrir = $data['lista_projetos'][0]->slug;

   		

   		$data['marcar'] = $abrir;

   		$data['projeto'] = $this->db->get_where('projetos', array('area' => 'adulto', 'slug' => $abrir))->result();

   		if(!isset($data['projeto'][0]))
   			redirect('adulto/projetos/index');

   		$data['imagens'] = $this->db->get_where('projetos_imagens', array('id_parent' => $data['projeto'][0]->id))->result();

   		$this->load->view('adulto/projetos', $data);
   }

}