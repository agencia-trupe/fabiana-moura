<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct('A');
   }

   function index(){

   		$data['textos'] = $this->db->get_where('perfil', array('area' => 'adulto'))->result();

   		$this->load->view('adulto/perfil', $data);
   }

}