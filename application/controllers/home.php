<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_controller {

   function __construct(){
   		parent::__construct();
   }

   function index(){
   		$this->load->view('landing');
   }

   function imagensMidia($id){

      $data['qry'] = $this->db->order_by('ordem', 'ASC')->get_where('midia_imagens', array('id_parent' => $id))->result();
      
      if(isset($data['qry'][0])){

         $qry_parent = $this->db->get_where('midia', array('id' => $data['qry'][0]->id_parent))->result();

         $data['origem'] = $qry_parent[0]->area;
         
         $this->load->view($qry_parent[0]->area.'/midia-imagens', $data);
      }else{
         $this->load->view('infantil/midia-imagens', $data);
      }

      
   }
}