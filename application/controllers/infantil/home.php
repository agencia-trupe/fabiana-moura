<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct('I');
   }

   function index(){

   		$data['slides'] = $this->db->order_by('ordem', 'ASC')->get_where('slides', array('area' => 'infantil'))->result();

   		$this->load->view('infantil/home', $data);
   }

}