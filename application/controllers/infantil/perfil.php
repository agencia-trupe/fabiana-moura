<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct('I');
   }

   function index(){

   		$data['textos'] = $this->db->get_where('perfil', array('area' => 'infantil'))->result();

   		$this->load->view('infantil/perfil', $data);
   }

}