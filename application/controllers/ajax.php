<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

   	function __construct(){
   		parent::__construct();

   		if(!$this->input->is_ajax_request())
   			redirect('home');
   	}

   	function pegaProjeto(){

   		$id = $this->input->post('id');

   		echo json_encode($this->db->order_by('ordem', 'ASC')->get_where('projetos_imagens', array('id_parent' => $id))->result());
   	}

}
