<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'contato';

		$this->dados = array(
			'telefone',
			'email',
			'facebook'
		);
		$this->dados_tratados = array(
			'facebook' => str_replace('http://www.facebook.com/', '', $this->input->post('facebook'))
		);
	}

}