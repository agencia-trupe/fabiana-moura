<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Midia_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'midia';
		$this->tabela_imagens = 'midia_imagens';
		
		$this->dados = array(
			'titulo',
			'data',
			'imagem',
			'area',
			'externo'
		);
		$this->dados_tratados = array(
			'slug' => url_title($this->input->post('titulo'), '_', TRUE),
			'imagem' => $this->sobeCapa($this->input->post('area')),
			'data' => formataData('01/'.$this->input->post('data'), 'br2mysql')
		);
	}

	function pegarTodos($origem){
		return $this->db->get_where($this->tabela, array('area' => $origem))->result();
	}

	function imagens($id_parent, $id_imagem = FALSE){
		if(!$id_imagem){
			return $this->db->order_by('ordem', 'ASC')->get_where($this->tabela_imagens, array('id_parent' => $id_parent))->result();
		}else{
			$query = $this->db->order_by('ordem', 'ASC')->get_where($this->tabela_imagens, array('id' => $id_imagem))->result();
			if(isset($query[0]))
				return $query[0];
			else
				return FALSE;
		}
	}

	function inserirImagem($origem){
		$imagem = $this->sobeImagem($origem);
		if($imagem !== FALSE){
			$this->db->set('imagem', $imagem);
		
			return $this->db->set('id_parent', $this->input->post('id_parent'))->insert($this->tabela_imagens);
		}else
			return false;
	}

	function sobeCapa($area){
		$this->load->library('upload');

		$original = array(
			'dir' => '_imgs/midia/'.$area.'/capas/',
			'x' => 163,
			'y' => 220,
			'corte' => 'resize_crop',
			'campo' => 'userfile_capa'
		);

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '3000',
		  'max_height' => '3000');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$original['campo']]) && $_FILES[$original['campo']]['error'] != 4){
		    if(!$this->upload->do_upload($original['campo'])){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
		        
		        $this->image_moo
	                 ->load($original['dir'].$filename)
	                 ->$original['corte']($original['x'], $original['y'])
	                 ->save($original['dir'].$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}

	function sobeImagem($area){
		$this->load->library('upload');

		$original = array(
			'dir' => '_imgs/midia/'.$area.'/',
			'x' => '980',
			'y' => '99999',
			'corte' => 'resize',
			'campo' => 'userfile'
		);

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '3000',
		  'max_height' => '3000');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$original['campo']]) && $_FILES[$original['campo']]['error'] != 4){
		    if(!$this->upload->do_upload($original['campo'])){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
		        
		        $this->image_moo
	                 ->load($original['dir'].$filename)
	                 ->$original['corte']($original['x'], $original['y'])
	                 ->save($original['dir'].$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}

}