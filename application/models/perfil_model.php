<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'perfil';
		
		$this->dados = array(
			'imagem',
			'area',
			'texto_empresa',
			'texto_fabiana',
			'texto_outro'
		);
		$this->dados_tratados = array(
			'imagem' => $this->sobeImagem($this->input->post('area'))
		);
	}

	function pegarTodos($origem){
		return $this->db->get_where($this->tabela, array('area' => $origem))->result();
	}	

	function sobeImagem($area){
		$this->load->library('upload');

		if ($area == 'infantil'){
			$x = '222';
			$y = '222';
		}else{
			$x = '200';
			$y = '330';
		}


		$original = array(
			'dir' => '_imgs/perfil/'.$area.'/',
			'x' => $x,
			'y' => $y,
			'corte' => 'resize_crop',
			'campo' => 'userfile'
		);

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '3000',
		  'max_height' => '3000');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$original['campo']]) && $_FILES[$original['campo']]['error'] != 4){
		    if(!$this->upload->do_upload($original['campo'])){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
		        
		        $this->image_moo
	                 ->load($original['dir'].$filename)
	                 ->$original['corte']($original['x'], $original['y'])
	                 ->save($original['dir'].$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}

}