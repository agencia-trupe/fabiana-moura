<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class MY_Frontcontroller extends CI_controller {

    var $headervar,
        $footervar,
        $menuvar,
        $origem;

    function __construct($origem, $css = '', $js = '') {
        parent::__construct();
        
        $this->origem = $origem;
        $this->headervar['load_css'] = $css;
        $this->headervar['load_js'] = $js;
        //$this->output->enable_profiler(TRUE);
    }
    
    function _output($output){

        if ($this->origem == 'I')
            $path = "infantil/";
        else
            $path = "adulto/";

        if(!$this->input->is_ajax_request()):

            echo $this->load->view($path.'common/header', $this->headervar, TRUE).
                 $this->load->view($path.'common/menu', $this->menuvar, TRUE).
                 $output.
                 $this->load->view($path.'common/footer', $this->footervar, TRUE);

        else:

            echo $output;

        endif;
    }

}
?>