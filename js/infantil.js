var init = function(){
	

	if($('.main').hasClass('main-home')){

		if($('body').hasClass('internas'))
			$('body').removeClass('internas');

		if($('#banners #animate').length)
			$('#banners #animate').cycle({
				timeout : 2000,
				prev : $('#btn-Prev'),
				next : $('#btn-Next')
			});

	}

	if($('.main').hasClass('main-perfil')){

		if(!$('body').hasClass('internas'))
			$('body').addClass('internas');

		$('.main-projetos #lateral ul li a').click( function(e){
			e.preventDefault();
		});

		setTimeout( function(){
			$('#texto-2').addClass('sobre');
		}, 200);
	}

	if($('.main').hasClass('main-projetos')){

		if(!$('body').hasClass('internas'))
			$('body').addClass('internas');

		$('#lateral ul li a').click( function(e){
			e.preventDefault();

			$('#lateral ul li a').removeClass('ativo');

			$(this).addClass('ativo');

			abreProjeto($(this).attr('data-target'));
		});

		if($('#lateral ul li a.ativo').length){
			abreProjeto($('#lateral ul li a.ativo').attr('data-target'));
		}

	}

	if($('.main').hasClass('main-midia')){

		if(!$('body').hasClass('internas'))
			$('body').addClass('internas');

		$('.fancy-modal').fancybox({
			type    : 'iframe',
			width   : 1010,
			height: 800,
			padding : 10,
			padding: 10,
            title: this.title,
            overlayColor : '#820F52'
		});

		if($('.pagina-midia').length > 1){
			$('#navegacao').show('slow', function(){
				$('#midia-container #animate').cycle({
					'prev' : $('#navegacao #nav-Prev'),
					'next' : $('#navegacao #nav-Next'),
					timeout : 0,
					before : function(currSlideElement, nextSlideElement, options, forwardFlag){
						var altura = parseInt($(nextSlideElement).css('height'));
						$('#animate').css('height', altura);
					}
				});
			});
		}

	}	

	if($('.main').hasClass('main-contato')){

		if(!$('body').hasClass('internas'))
			$('body').addClass('internas');

		var distancia = 500;
		$('.main-contato .centro').css('height', distancia);
		$('.main-contato .centro #imagem-footer').css('opacity', 1);

		$('#form-contato').submit( function(e){

			e.preventDefault();

			$.post(BASE+'infantil/contato/enviar', {
				nome     : $('#input-nome').val(),
				email    : $('#input-email').val(),
				telefone : $('#input-telefone').val(),
				tipo     : $('#input-tipo').val(),
				mensagem : $('#input-mensagem').val()
			}, function(){

				$('#form form').fadeOut('normal', function(){
					$('#form .resposta').fadeIn('normal', function(){
						$('#form .resposta').delay(4000).fadeOut('normal', function(){
							$('#input-nome').val('');
							$('#input-email').val('');
							$('#input-telefone').val('');
							$('#input-tipo').val('');
							$('#input-mensagem').val('');
							$('#form form').fadeIn('normal');
						});
					});
				});

			});

		});
	}

	Modernizr.load([
		{
		  	test: Modernizr.borderradius,
		  	nope: ['js/jquery.corner.js' ,'js/polyfill-border.js']
		},
		{
			test: Modernizr.placeholder,
		  	nope: 'js/polyfill-placeholder.js'
		}
	]);
};

var abrePaginacao = function(){
	$('#lateral #thumbs #navegacao a').show('slow', function(){
		$('#lateral #thumbs #imagens').cycle({
			'prev' : $('#navegacao #nav-Prev'),
			'next' : $('#navegacao #nav-Next'),
			timeout : 0
		});		
	});
}

var abreProjeto = function(id){

	$.post(BASE+'/ajax/pegaProjeto', { id : id }, function(retorno){
		
		var retorno = JSON.parse(retorno);
		
		var destino = $('#lateral #thumbs #imagens');
		var abertas = $('#lateral #thumbs #imagens a:visible');

		$('#lateral #thumbs #imagens a').fadeOut('slow', function(){
			$(this).remove();
		});

		if($('#ampliada img:visible').length){
			$('#ampliada img').fadeOut('slow', function(){
				$(this).remove();
			});
		}

		var contador = 0;
		var str_imagens = "<div class='pagina'>";
		retorno.map( function(self){

			if(contador%6 == 0 && contador > 0)
				str_imagens += "</div><div class='pagina' style='display:none;'>";

			if(contador%2 == 0)
				str_imagens += "<a id='thumb_"+self.id+"' class='marg' href='_imgs/projetos/infantil/"+self.imagem+"'><img src='_imgs/projetos/infantil/thumb/"+self.imagem+"''></a>";
			else
				str_imagens += "<a id='thumb_"+self.id+"' href='_imgs/projetos/infantil/"+self.imagem+"'><img src='_imgs/projetos/infantil/thumb/"+self.imagem+"''></a>";

			contador++;
		});
		str_imagens += "</div>";
		
		destino.html(str_imagens);
		
		var paginas = $('#lateral #thumbs #imagens .pagina');

		if(paginas.length > 1){
			
			if($('#lateral #thumbs #imagens').cycle())
				$('#lateral #thumbs #imagens').cycle('destroy');

			abrePaginacao();
		}else
			$('#lateral #thumbs #navegacao a').hide('slow');

		$('#lateral #thumbs #imagens a').click( function(e){
			e.preventDefault();

			$('#lateral #thumbs #imagens a').removeClass('ativo');
			$(this).addClass('ativo');

			var imagem = $(this).attr('href');
			var destino = $('#ampliada');
			var id = $(this).attr('id').split('_').pop();
			var aberta = destino.find('img');

			if(aberta.length){
				var ampliada = $("<img id='ampliada_"+id+"' src='"+imagem+"'>");
				destino.append(ampliada);
				setTimeout( function(){
					aberta.css('opacity', 0);
					$('#ampliada_'+id).css('opacity', 1);
				}, 400);
			}else{
				var ampliada = $("<img id='ampliada_"+id+"' src='"+imagem+"'>");
				destino.append(ampliada);
				setTimeout( function(){
					$('#ampliada_'+id).css('opacity', 1);
				}, 400);
			}

		});

		$('#lateral #thumbs #imagens .pagina:first-child a:first-child').trigger('click');

	});
}


$('document').ready( function(){

	init();

	$('header nav ul li a').click( function(e){
		
		if($(this).attr('href') != 'http://fabianamouraprojetospersonalizados.blogspot.com.br/'){
			e.preventDefault();

			if(!$(this).hasClass('ativo')){

				var abrir = $(this).attr('href');
				var classe = abrir.split('/').pop();

				if(window.location.hash != '#'+classe)
					window.location.hash = classe;

				$('nav ul li a').removeClass('ativo');

				$(this).addClass('ativo');
				
				$("header").removeClass (function (index, css) {
				    return (css.match (/\bheader-\S+/g) || []).join(' ');
				});

				$("header").addClass('header-'+classe);

				setTimeout( function(){

					$.post(abrir, function(retorno){

						var esconder = $('.main:visible');
						var mostrar = $(retorno);

						mostrar.css({
							'opacity' : 0
						})

						esconder.css({
							'z-index' : -10,
							'opacity' : 0,
							'top' : -1 * (parseInt(esconder.css('height')) + 100)
						});

						setTimeout( function(){
							$('header').after(mostrar);
							esconder.remove();
							setTimeout(function(){
								$('.main').css({ 'opacity' : 1});

								setTimeout(init, 400);

							}, 200);
							
						}, 400)
						
					});

				}, 200);

			}
		}

	});

	$('#ampliada img').live('click', function(e){
		var imagem = $(this);
		if(imagem.length){
			var id = $(this).attr('id').split('_').pop();
			var thumb = $('#lateral #thumbs #imagens .pagina:visible a#thumb_'+id);
			if(thumb.length){
				var proxima = thumb.next('a:first');
				if(proxima.length){
					proxima.trigger('click');
				}else{
					if($('#lateral #thumbs #imagens .pagina').length > 1){
						$('#navegacao #nav-Next').trigger('click');
						setTimeout(function(){
							$('#lateral #thumbs #imagens .pagina:visible').next('.pagina:first').find('a:first').trigger('click');
						}, 1000);
					}else{						
						$('#lateral #thumbs #imagens .pagina:visible a:first').trigger('click');
					}
				}
			}
		}
	});

	newHash = window.location.hash.substring(1);
	if(newHash)
		$('#mn-'+newHash).trigger('click');

});