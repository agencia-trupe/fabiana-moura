<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">

	<h1><?=traduz('NOTÍCIAS')?></h1>

	<article>

		<div id="filtro-noticias">
			<select id="sel-categoria">
				<option value=""><?=traduz('FILTRAR POR CATEGORIA DE NOTÍCIAS')?></option>
				<?php if ($categorias): ?>
					<?php foreach ($categorias as $key => $value): ?>
						<?php if ($this->session->userdata('idioma_interface') == 3): ?>
							<option value="<?=$value->slug?>"><?=$value->titulo_es?></option>
						<?php else: ?>
							<option value="<?=$value->slug?>"><?=$value->titulo?></option>
						<?php endif ?>
					<?php endforeach ?>
				<?php else: ?>
					<option value=""><?=traduz('Nenhuma Categoria')?></option>
				<?php endif ?>
			</select>

			<form name="form-busca" method="post" action="noticias/busca">
				<input type="text" name="termo" placeholder="<?=traduz('BUSCAR EM NOTÍCIAS')?>">
				<input type="submit" value="&raquo;">
			</form>
		</div>

		<?php if ($filtro): ?>
			<h1 class="lista-titulo"><?=traduz('Exibindo notícias de')?> <div class="resultado"><?=$filtro?></div></h1>
		<?php else: ?>
			<h1 class="lista-titulo"><?=traduz('Últimas Notícias')?></h1>
		<?php endif ?>
		
		<ul>
			<?php foreach ($noticias as $key => $value): ?>
			<li>
				<a href="noticias/detalhes/<?=$value->slug?>" title="<?=$value->titulo?>">
					<?php if ($value->imagem): ?>
						<img src="_imgs/noticias/thumbs/<?=$value->imagem?>" alt="<?=$value->titulo?>">
					<?php endif ?>
					<div class="data"><?=formataData($value->data, 'mysql2br')?> &bull; <?=$value->nome_autor?></div>
					<div class="titulo"><?=$value->titulo?></div>
					<div class="olho"><?=$value->olho?></div>
				</a>
			</li>
			<?php endforeach ?>
		</ul>

		<?php if ($paginacao): ?>
			<div id="paginacao">
				<?php echo $paginacao ?>
			</div>
		<?php endif ?>
		
	</article>

	<aside>
		<div class="sidebar-titulo"><?=traduz('Últimos vídeos do evento')?>:</div>

		<div id="cycle-this">
			
			<ul>
				<?$contador=0;?>
				<?php foreach ($sidebar as $key => $value): ?>
					<li>
						<a href="videos/detalhes/<?=$value->slug?>" title="<?=$value->titulo?>">
							<img src="<?=$value->thumbnail?>">
							<div class="texto">
								<div class="titulo"><?=$value->titulo?></div>
								<div class="palestrante"><?=$value->palestrante?></div>
								<div class="data"><?=formataData($value->data, 'mysql2br')?></div>
							</div>
						</a>
					</li>
					<?$contador++?>
					<?if($contador == 12){
						echo "</ul><ul>";
						$contador = 0;
					}?>
				<?php endforeach ?>
			</ul>

		</div>

		<div id="botao-proxima">
			<a href="#" title="">&raquo;</a>
		</div>

		<a href="videos" title="Fazer nova busca" class="busca"><?=traduz('FAZER NOVA BUSCA DE VÍDEOS')?> &raquo;</a>		
	</aside>

	<script defer>
	$('document').ready( function(){

		$('#sel-categoria').change( function(){
			if($(this).val() != ''){
				window.location = BASE+'noticias/categoria/'+$(this).val();
			}
		});

	});
	</script>