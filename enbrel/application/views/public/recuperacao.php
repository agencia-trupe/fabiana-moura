<div class="login">
	
	<h1><?=traduz('RECUPERAÇÃO DE SENHA')?></h1>

	<?=traduz('TEXTO_RECUPERAÇÃO_SENHA')?>
	
	<form name="login" action="login/recuperar">
		<input type="text" name="login" placeholder="<?=traduz('LOGIN')?>" required><input type="submit" value="OK">
	</form>

</div>


<style type="text/css">
	@font-face {
	    font-family: 'Myriad Web Pro Cond';
	    src: url('../css/fontface/myriadwebpro-condensed-webfont.eot');
	    src: url('../css/fontface/myriadwebpro-condensed-webfont.eot?#iefix') format('embedded-opentype'),
	         url('../css/fontface/myriadwebpro-condensed-webfont.woff') format('woff'),
	         url('../css/fontface/myriadwebpro-condensed-webfont.ttf') format('truetype'),
	         url('../css/fontface/myriadwebpro-condensed-webfont.svg#myriad_web_pro_condensedRg') format('svg');
	    font-weight: normal;
	    font-style: normal;
	}

	.login{
		text-align:center;
		color:#293F66;
		font-family:'Verdana','Arial',sans-serif;
		font-size:12px;
	}
	.login h1{
		font-size:30px;
		font-family:'Myriad Web Pro Cond','Verdana','Arial', sans-serif;
	}
	.login form{
		margin:15px auto 10px auto;
		width:600px;
	}
	.login form input[type="password"],
	.login form input[type="text"]{
		margin:0;
		padding:0;
		border:1px #293F66 solid;
		-webkit-border-radius:3px;
		-moz-border-radius:3px;
		-ms-border-radius:3px;
		border-radius:3px;
		text-align:center;
		margin-right:3px;
		width:250px;
		height:20px;
		line-height:20px;
	}
	.login form input[type="submit"]{
		-webkit-border-radius:3px;
		-moz-border-radius:3px;
		-ms-border-radius:3px;
		border-radius:3px;		
		width:40px;
		text-align:center;
		height:20px;
		line-height:20px;
		border:0;
		padding:0;
		margin:0;
		background-color:#293F66;
		color:#FFF;
		font-size:12px;
		vertical-align:top;
		-webkit-transition:color .3s ease-in;  
	   	-moz-transition:color .3s ease-in;  
	   	-o-transition:color .3s ease-in;  
	   	transition:color .3s ease-in;		
		font-family:'Myriad Web Pro Cond','Verdana','Arial', sans-serif;
	}
	.login form input[type="submit"]:hover{ color:#00B0DC; }
	.login a.recuperacao{
		font-family:'Verdana','Arial', sans-serif;
		font-size:10px;
		text-decoration:none;
		color:#293F66;
	}
	.login a.recuperacao:active, .login a.recuperacao:visited{color:#293F66;}
	.login a.recuperacao:hover{text-decoration:underline;}
	.login form input[type="submit"]:hover{ cursor:pointer; }
	.login a.cadastro{
		display:block;
		margin:15px auto;
		background-color:#293F66;
		color:#00B0DC;
		width:260px;
		height:36px;
		line-height:150%;
		padding:12px 0;
		text-decoration:none;
		-webkit-border-radius:5px;
		-moz-border-radius:5px;
		-ms-border-radius:5px;
		border-radius:5px;
		-webkit-transition:all .3s ease-in;  
	   	-moz-transition:all .3s ease-in;  
	   	-o-transition:all .3s ease-in;  
	   	transition:all .3s ease-in;		
	}
	.login a.cadastro:active, .login a.cadastro:visited{color:#00B0DC}
	.login a.cadastro:hover{ color:#fff; }
	.login a.cadastro .maior{font-size:20px;}
	.login #separador{
		height:1px;
		background-color:#293F66;
		margin:30px 0;
	}
	.login .disclaimer-cadastro{
		font-size:10px;
	}
	.login .fail{
		padding:7px;
		color:#FF2A55;
		font-family:'Verdana','Arial', sans-serif;
		font-size:10px;
	}
	.success{
		padding:7px;
		color:#1EC81E;
		font-family:'Verdana','Arial', sans-serif;
		font-size:12px;
	}
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script defer>
var BASE = '<?=base_url()?>';
$('document').ready( function(){

	$('.login form').on('submit', function(e){
		e.preventDefault();
		var destino = $(this).attr('action');
		var usuario = $(this).find('input[type="text"]').val();
		
		$.post(BASE+destino, {
			login : usuario
		}, function(retorno){
		
			if(retorno == 1){

				var idioma = $('body').attr('data-lang');
				if (idioma == 'pt')
					var texto = "Uma senha temporária foi enviada para o e-mail cadastrado.<br>Utilize-a para acessar o sistema, e em seguida a altere para a senha de sua preferência";
				else
					var texto = "Uma senha temporária foi enviada para o e-mail cadastrado.<br>Utilize-a para acessar o sistema, e em seguida a altere para a senha de sua preferência";

				$('.login form').append($("<div class='success'>"+texto+"</div>"));

			}else{
				if($('.fail').length == 0){

					var idioma = $('body').attr('data-lang');
					if (idioma == 'pt')
						var texto = "Email não cadastrado.";
					else
						var texto = "Email não cadastrado.";

					$('.login form').append($("<div class='fail'>"+texto+"</div>"));
					$('.fail').delay(2000).fadeOut('normal', function(){
						$(this).remove();
					});
				}
			}
		});
	})
});
</script>