<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Reumato Online</title>
  <meta name="description" content="O Reumato Online é uma iniciativa da Pfizer com o objetivo de promover a atualização científica. Ele compila o antes, o durante e o depois do ACR por meio de textos e vídeos.">
  <meta name="keywords" content="Reumato, online, Reumato Online, Pfizer, Reumatologia, ACR, ACR2012, Congresso de Reumatologia, Sociedade Brasileira de Reumatologia, Iniciativa, Evento" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2012 Trupe Design" />

  <meta property="og:title" content="Reumato Online"/>
  <meta property="og:site_name" content="Reumato Online"/>
  <meta property="og:type" content="website"/>
  <meta property="og:url" content="<?=base_url()?>"/>
  <meta property="og:description" content="O Reumato Online é uma iniciativa da Pfizer com o objetivo de promover a atualização científica. Ele compila o antes, o durante e o depois do ACR por meio de textos e vídeos."/>
  <meta property="fb:app_id" content="299553933483705"/>
  <meta property="fb:admins" content="100002297057504" />
  
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>

  <?CSS(array('reset', 'base', 'fontface/stylesheet', 'fancybox/fancybox', 'home', 'cadastro'))?>  
  

  <?if(ENVIRONMENT == 'development'):?>
    
    <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.8.0.min'))?>
    
  <?else:?>

    <?JS(array('modernizr-2.0.6.min'))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

  <?endif;?>

</head>
<?
  if($this->session->userdata('language') == 1 || $this->session->userdata('language') == 3)
    $compare = $this->session->userdata('language');
  else
    $compare = default_lang();
?>
<body id="<? echo ($compare == 1) ? 'body-pt' : 'body-es'?>">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=299553933483705";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>