<header>

	<div class="centro">

		<div id="lang-select">
			<a href="linguagem/index/1" title="Versão Em Português" class="pt-br">português</a>
			<a href="linguagem/index/3" title="Versión en Español" class="es-es">español</a>
			<div class="fb-like" data-href="http://www.reumatonline.com/" data-send="false" data-width="300" data-show-faces="false" data-layout="button_count"></div>
		</div>

		<div id="login-box">

			<form name="form-login" method="post" action="login/logar">
				<input type="text" name="login" placeholder="<?=traduz('LOGIN')?>" required>
				<input type="password" name="senha" placeholder="<?=traduz('SENHA')?>" required>
				<input type="submit" value="OK">
			</form>

			<?php if ($this->session->flashdata('login-fail')): ?>

				<div class="login-fail"><?=traduz('Erro ao logar. Verifique nome de usuário e senha.')?></div>

				<script defer>

					$('document').ready( function(){
						$('header .centro').animate({ 'height' : '+=15' }, function(){
							$('.login-fail').show('normal', function(){
								$(this).delay(4000).slideUp('normal', function(){
									$('header .centro').animate({ 'height' : '-=15' });	
								});
							});
						});
					});

				</script>

			<?php endif ?>

			<a href="login/recuperacao" title="<?=traduz('esqueci minha senha')?>"><?=traduz('esqueci minha senha')?> &raquo;</a> | <a href="login/cadastro" title="<?=traduz('Efetuar Cadastro')?>"><?=traduz('Ainda não sou cadastrado. Cadastrar')?> &raquo;</a>

		</div>

	</div>

</header>

<div class="backdrop">

	<div class="centro">

		<div id="menu-container">

			<a href="<?=base_url()?>" id="link-home" title="Página Inicial"></a>

			<nav>

				<ul>
					<li><a href="login" title="<?=traduz('Veja os vídeos das palestras')?>" <?if($this->router->class=='videos')echo"class='ativo'"?>><?=traduz('VÍDEOS')?></a></li>
					<li><a href="login" title="<?=traduz('Últimas notícias')?>" <?if($this->router->class=='noticias')echo"class='ativo'"?>><?=traduz('NOTÍCIAS')?></a></li>
					<li><a href="login" title="<?=traduz('Conheça o ACR2012')?>" <?if($this->router->class=='acr2012')echo"class='ativo'"?>><?=traduz('ACR 2012')?></a></li>
					<li><a href="login" title="<?=traduz('Notícias de Washington DC')?>" <?if($this->router->class=='washington')echo"class='ativo'"?>><?=traduz('WASHINGTON DC')?></a></li>
					<li><a href="login" title="<?=traduz('Institucional')?>" <?if($this->router->class=='institucional')echo"class='ativo'"?>><?=traduz('INSTITUCIONAL')?></a></li>
				</ul>

				<form id="form-busca" name="form-busca" method="post" action="login">
					<input type="text" name="termo" placeholder="<?=traduz('buscar conteúdos')?>">
					<input type="submit" value="&raquo;">
				</form>

			</nav>

			<div id="banners-home">

				<?php if ($slides): ?>

					<?php foreach ($slides as $slide): ?>
						<img src="_imgs/slides/<?=$slide->imagem?>">
					<?php endforeach ?>

				<?php else: ?>
					<img src="_imgs/layout/slide-temp.png">
				<?php endif ?>

			</div>

		</div>