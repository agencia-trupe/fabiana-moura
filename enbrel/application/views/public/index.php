<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">

	<div class="videos">
		
		<div class="ampliado">

			<?php if ($video_destaque): ?>
				<?=fakeEmbed($video_destaque->thumbnail, 640, 360)?>
				<div class="data">
					<div><?=formataData($video_destaque->data, 'mysql2br')?></div>
				</div>
				<div class="texto">
					<h1><?=$video_destaque->titulo?></h1>
					<h2><?=$video_destaque->palestrante?></h2>
					<div class="olho">
						
						<?=$video_destaque->olho?>

					</div>
					<div class="ver-mais">
						<a href="login" title="<?=traduz('ver mais')?>"><?=traduz('ver mais')?> &raquo;</a>
					</div>
				</div>
			<?php endif ?>

		</div>

		<div class="lista">

			<a href="login" title="<?=traduz('ALTERAR MINHAS PREFERÊNCIAS DE EXIBIÇÃO')?>" class="botao-preferencias"><?=traduz('ALTERAR MINHAS PREFERÊNCIAS DE EXIBIÇÃO')?></a>

			<div class="ordem"><?=traduz('vídeos em ordem decrescente:')?></div>

			<ul>
				<?php if ($video_lista): ?>
					<?php foreach ($video_lista as $key => $value): ?>
						<li>
							<a href="login" title="<?=$value->titulo?>">
								<img src="<?=$value->thumbnail?>" alt="<?=$value->titulo?>">
								<div class="descricao">
									<div class="titulo"><?=$value->titulo?></div>
									<div class="palestrante"><?=$value->palestrante?></div>
									<div class="data"><?=formataData($value->data, 'mysql2br')?></div>
								</div>
							</a>
						</li>						
					<?php endforeach ?>
				<?php endif ?>
			</ul>

			<a href="login" title="<?=traduz('TODOS OS VÍDEOS')?>" class="botao-todos"><?=traduz('TODOS OS VÍDEOS')?></a>
			
		</div>

	</div>

	<div class="chamadas">

		<div class="coluna">
		
			<?php if ($aovivo[0]->transmitindo == 1): ?>
				<a class="aovivo" target="_blank" href="login" title="Assista os vídeos ao vivo">
					<div class="pad">
						<img src="_imgs/layout/icone-aovivo.png" alt="Transmissão Ao Vivo">
						<h1><?=traduz('Acompanhe aqui')?></h1>
						<?=traduz('transmissões ao vivo diretamente do evento em Washington DC.')?>
					</div>
					<div class="transmitindo">
						<?php if (default_lang() == 1): ?>
							<?=$aovivo[0]->mensagem_pt?>
						<?php else: ?>
							<?=$aovivo[0]->mensagem_es?>
						<?php endif ?>
					</div>
				</a>				
			<?php else: ?>
				<a class="aovivo" target="_blank" href="login" title="Não há vídeos sendo transmitidos no momento">
					<div class="pad">
						<img src="_imgs/layout/icone-aovivo.png" alt="Transmissão Ao Vivo">
						<h1><?=traduz('Aguarde')?></h1>
					</div>
					<div class="transmitindo">
						<div class="titulo"><?=traduz('TRANSMITINDO AGORA:')?></div>
						<?=traduz('Não há vídeos sendo transmitidos no momento.')?>
					</div>
				</a>				
			<?php endif ?>

			<a href="login/cadastro" id="cad_link" title="<?=traduz('Cadastre-se!')?>">
				<?=traduz('Cadastre-se!')?><br>
				<span class="menor"><?=traduz('Acesso a todos os conteúdos!')?></span>
			</a>

		</div>

		<div class="noticias">
			
			<h1 class="acontece"><?=traduz('ACONTECEU NO ACR2012')?></h1>

			<?php if ($noticias): ?>
				<?$i = 0; ?>
				<?php foreach ($noticias as $key => $value): ?>
					<a class="noticia <?if($i==2)echo"ultima"?>" href="login">
						<div class="corpo">
							<?php if ($value->imagem): ?>
								<img src="_imgs/noticias/thumbs/<?=$value->imagem?>">	
							<?php endif ?>
							<h1><?=$value->titulo?></h1>
							<div class="data"><?=formataData($value->data, 'mysql2br')?></div>
							<div class="texto">
								<p>
									<?=str_replace('<p>', '', str_replace('</p>', '', $value->olho))?>
								</p>
							</div>
						</div>
						<div class="saiba-mais"><?=traduz('ver mais')?> &raquo;</div>
					</a>
				<?php $i++; ?>					
				<?php endforeach ?>
			<?php endif ?>

		</div>

	</div>