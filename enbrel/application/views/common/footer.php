        <?php if ($banner): ?>
          <?php foreach ($banner as $key => $value): ?>
            <div id="banner">
              <a href="<?=$value->destino?>" title="<?=$value->titulo?>" target="_blank"><img src="_imgs/banners/<?=$value->imagem?>" alt="<?=$value->titulo?>"></a>      
            </div>      
          <?php endforeach ?>
        <?php endif ?>


      </div> <!-- fim da div main -->

    </div> <!-- fim da div centro -->

  </div> <!-- fim da div backdrop -->

  <footer>

    <div class="centro">
      
      <ul>
        <li><a href="videos" title="<?=traduz('Veja os vídeos das palestras')?>">&raquo; <?=traduz('VÍDEOS')?></a></li>
        <li><a href="noticias" title="<?=traduz('Últimas notícias')?>">&raquo; <?=traduz('NOTÍCIAS')?></a></li>
        <li><a href="acr2012" title="<?=traduz('Conheça o ACR2012')?>">&raquo; <?=traduz('ACR 2012')?></a></li>
        <li><a href="washington" title="<?=traduz('Notícias de Washington DC')?>">&raquo; <?=traduz('WASHINGTON DC')?></a></li>
        <li><a href="institucional" title="<?=traduz('Institucional')?>">&raquo; <?=traduz('INSTITUCIONAL')?></a></li>
        <li></li>
        <li><a href="home/perfil" title="<?=traduz('Seu Cadastro')?>">&raquo; <?=traduz('CADASTRO')?></a></li>
      </ul>

      <div class="assinatura">
        &copy; <?=date('Y')?> Segmento Farma<br>
        <?=traduz('Todos os direitos reservados')?><br>
        <?=traduz('Desenvolvimento:')?><br>
        <a href="" title="Segmento Farma" target="_blank"><img src="_imgs/layout/segmento.png"></a>
      </div>      

      <div id="disclaimer">
        This activity is not sanctioned by, nor a part of, the American College of Rheumatology.<br>
        Esta atividade não é sancionada, nem faz parte, do Colégio Americano de Reumatologia.
        <div style="margin:10px auto; padding:10px 0; border-top:1px #fff solid; text-align:center; width:515px;">
          Website destined for physicians residing in Latin America.<br>
          Sitio web dirigido a profesionales médicos que residen en América Latina.<br>
          Website destinados a profissionais médicos residentes na América Latina. 
        </div>
        <div style="margin:10px auto; padding:10px 0; border-top:1px #fff solid; text-align:center; width:515px;">
          <a id="linkcontato" href="mailto:<?=traduz('EMAILCONTATO')?>"><?=traduz('EMAILCONTATO')?></a>
        </div>
      </div>

    </div>
  
  </footer>
  
  
  <script type="text/javascript">
   
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-35309392-1']);
    _gaq.push(['_trackPageview']);
   
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
   
  </script>

  <?JS(array('jquery-ui-1.8.12.custom.min', 'jquery.ui.datepicker-pt-BR', 'fancybox','cycle', 'placeholder','front'))?>
  
</body>
</html>
