<header>

	<div class="centro">


		<div id="lang-select">
			<a href="linguagem/index/1" title="Versão Em Português" class="pt-br">português</a>
			<a href="linguagem/index/3" title="Versión en Español" class="es-es">español</a>
			<div class="fb-like" data-href="http://www.reumatonline.com/" data-send="false" data-width="300" data-show-faces="false" data-layout="button_count"></div>
		</div>

		<div id="login-box">

			<div class="perfil-area">
				<?=traduz('Olá')?>, <?=$this->session->userdata('nome')?> &bull; [<a href="login/logout" class="logout" title="Fazer Logout"><?=traduz('sair')?></a>] <a href="home/perfil" class="perfil-link" title="<?=traduz('ACESSAR MEU CADASTRO | PREFERÊNCIAS')?>"><?=traduz('ACESSAR MEU CADASTRO | PREFERÊNCIAS')?></a>
			</div>

		</div>

	</div>

</header>


<div class="backdrop">

	<div class="centro">

		<div id="menu-container">

			<a href="<?=base_url()?>" id="link-home" title="Página Inicial"></a>

			<nav>
				<ul>
					<li><a href="videos" title="<?=traduz('Veja os vídeos das palestras')?>" <?if($this->router->class=='videos')echo"class='ativo'"?>><?=traduz('VÍDEOS')?></a></li>

					<li><a href="noticias" title="<?=traduz('Últimas notícias')?>" <?if($this->router->class=='noticias')echo"class='ativo'"?>><?=traduz('NOTÍCIAS')?></a></li>

					<li><a href="acr2012" title="<?=traduz('Conheça o ACR2012')?>" <?if($this->router->class=='acr2012')echo"class='ativo'"?>><?=traduz('ACR 2012')?></a></li>

					<li><a href="washington" title="<?=traduz('Notícias de Washington DC')?>" <?if($this->router->class=='washington')echo"class='ativo'"?>><?=traduz('WASHINGTON DC')?></a></li>

					<li><a href="institucional" title="<?=traduz('Institucional')?>" <?if($this->router->class=='institucional')echo"class='ativo'"?>><?=traduz('INSTITUCIONAL')?></a></li>

				</ul>

				<form name="form-busca" method="post" action="busca">
					<input type="text" name="termo" placeholder="<?=traduz('buscar conteúdos')?>">
					<input type="submit" value="&raquo;">
				</form>
			</nav>

			<div id="banners-home">

				<?php if ($slides): ?>

					<?php foreach ($slides as $k => $slide): ?>

						<img src="_imgs/slides/<?=$slide->imagem?>" <?if($k>0)echo" style='display:none;'"?>>

					<?php endforeach ?>

				<?php else: ?>

					<img src="_imgs/layout/slide-temp.png">

				<?php endif ?>

			</div>

		</div>