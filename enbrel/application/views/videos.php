<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">

	<h1><?=traduz('VÍDEOS')?></h1>
	
	<form id="parametros_busca" action="videos/index" method="post">

		<h2 class="tit"><?=traduz('BUSCAR VÍDEOS')?></h2>

		<div class="coluna ordem">

			<h2><?=traduz('Ordenar por')?>:</h2>

			<label><input type="radio" name="ordem" value="desc" <?if(($ordem && $ordem == 'desc') || !$ordem)echo" checked"?>><?=traduz('data decrescente')?></label>
			<label><input type="radio" name="ordem" value="asc" <?if($ordem && $ordem == 'asc')echo" checked"?>><?=traduz('data crescente')?></label>

		</div>

		<div class="coluna filtro">

			<h2><?=traduz('Filtrar por')?>:</h2>

			<label><?=traduz('palestrante')?>: <select name="palest">
				<option value=""><?=traduz('Selecione')?>...</option>
				<?php if ($palestrantes): ?>
					<?php foreach ($palestrantes as $key => $value): ?>
						<option value="<?=$value->palestrante?>" <?if($palest && $palest==$value->palestrante)echo" selected"?>><?=$value->palestrante?></option>
					<?php endforeach ?>
				<?php endif ?>
			</select></label>

			<label><?=traduz('tema')?>: <select name="id_tema">
				<option value="0"><?=traduz('Selecione')?>...</option>
				<?php if ($temas): ?>
					<?php foreach ($temas as $key => $value): ?>
						<option value="<?=$value->id?>" <?if($tema && $tema==$value->id)echo "selected"?>><?=$value->titulo?></option>
					<?php endforeach ?>
				<?php endif ?>
			</select></label>

			<div class="fakelabel">
				<?=traduz('data')?>: <div id="datas"><input type="text" name="data_inicio" class="datepicker" <?if($dt_inicio)echo" value='".$dt_inicio."'"?>> <?=traduz('até')?> <input type="text" name="data_termino" class="datepicker" <?if($dt_termino)echo" value='".$dt_termino."'"?>></div>
			</div>


		</div>

		<div class="coluna idioma">
			<h2><?=traduz('Idioma')?>:</h2>
			<?=traduz('Exibindo apenas conteúdo em')?>: <strong><?php
			switch($this->session->userdata('idioma_conteudo')){
				case 1:
					echo "português";
					break;
				case 3:
					echo "español";
					break;
				case 4:
					echo "português e español";
					break;
			}
			?></strong><br>
			<?=traduz('Para alterar o idioma acesse seu cadastro alterando as')?> <a href="home/perfil" title="<?=traduz('Minhas Preferências')?>"><?=traduz('Minhas Preferências')?></a>
		</div>

		<div class="coluna botao">
			<input type="submit" value="<?=traduz('FILTRAR')?>">
		</div>

	</form>

	<div class="whitebox">
		
		<?php if ($videos): ?>

			<h3><?=traduz('Exibindo os vídeos recentes com as preferências selecionadas')?>:</h3>

			<?php $contador = 0 ?>

			<div class="vids">

				<?php foreach ($videos as $key => $value): ?>

					<?php $contador++ ?>

					<a href="videos/detalhes/<?=$value->slug?>" title="<?=$value->titulo?>" <?if($contador%3 == 0 && $contador > 0)echo" class='ultimo'"?>>
						<img src="<?=$value->thumbnail?>" alt="<?=$value->titulo?>">
						<div class="texto">
							<div class="titulo"><?=$value->titulo?></div>
							<div class="palestrante"><?=$value->palestrante?></div>
							<div class="data"><?=formataData($value->data, 'mysql2br')?></div>
						</div>
					</a>
					
				<?php endforeach ?>

			</div>

		<?php else: ?>
			<h1 style="text-align:center;"><?=traduz('Nenhum Vídeo Encontrado')?></h1>
		<?php endif ?>

		<?php if ($paginacao): ?>
			<div id="paginacao">
				<?=$paginacao?>
			</div>	
		<?php endif ?>

	</div>