<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">

	<h1><?=traduz('VÍDEOS')?></h1>

	<article>

		<a href="videos" class="voltar" title="">&laquo; <?=traduz('voltar')?></a>

		<div id="tema"><?=traduz('Tema')?>: <strong><?=$video[0]->nome_tema?></strong></div>

		<div id="embed"><?=embed($video[0]->url, 638, 357)?></div>

		<div id="data"><div><?=formataData($video[0]->data, 'mysql2br')?></div></div>

		<div id='texto'>
			<h1><?=$video[0]->titulo?></h1>
			<h2><?=$video[0]->palestrante?></h2>
			<?=$video[0]->texto?>
		</div>

		<?php if ($relacionamentos_noticias || $relacionamentos_links || $relacionamentos_washington): ?>
		
			<div id="relacionamentos"><?=traduz('CONTEÚDO RELACIONADO')?></div>

			<?php if ($relacionamentos_noticias): ?>
				<div class="tipo-relacionamento">
					<div class="titulo"><?=traduz('notícias')?></div>
					<div class="linha"></div>
				</div>

				<?php foreach ($relacionamentos_noticias as $key => $value): ?>
					<a class="rel-link" href="noticias/detalhes/<?=$value->source->slug?>" title="<?=$value->titulo?>">
						<div class="data"><?=formataData($value->source->data, 'mysql2br')?></div>
						<div class="categoria"><?=$value->titulo_categoria?></div>
						<div class="titulo"><?=$value->source->titulo?></div>
					</a>					
				<?php endforeach ?>

			<?php endif ?>

			<?php if ($relacionamentos_washington): ?>
				<div class="tipo-relacionamento">
					<div class="titulo"><?=traduz('washington DC')?></div>
					<div class="linha"></div>
				</div>

				<?php foreach ($relacionamentos_washington as $key => $value): ?>
					<a class="rel-link" href="washington/detalhes/<?=$value->source->slug?>" title="<?=$value->titulo?>">
						<div class="data"><?=formataData($value->source->data, 'mysql2br')?></div>
						<div class="categoria"><?=$value->titulo_categoria?></div>
						<div class="titulo"><?=$value->source->titulo?></div>
					</a>					
				<?php endforeach ?>

			<?php endif ?>			

			<?php if ($relacionamentos_links): ?>
				<div class="tipo-relacionamento">
					<div class="titulo"><?=traduz('links')?></div>
					<div class="linha"></div>
				</div>

				<?php foreach ($relacionamentos_links as $key => $value): ?>
					<a class="rel-link" href="<?=$value->endereco?>" target="_blank" title="<?=$value->titulo?>">
						<div class="titulo"><?=$value->titulo?></div>
					</a>					
				<?php endforeach ?>

			<?php endif ?>

		<?php endif ?>

	</article>

	<aside>

		<div class="sidebar-titulo"><?=traduz('Assista Também')?>:</div>

		<div id="cycle-this">
			
			<ul>
				<?$contador=0;?>
				<?php foreach ($sidebar as $key => $value): ?>
					<li>
						<a href="videos/detalhes/<?=$value->slug?>" title="<?=$value->titulo?>">
							<img src="<?=$value->thumbnail?>">
							<div class="texto">
								<div class="titulo"><?=$value->titulo?></div>
								<div class="palestrante"><?=$value->palestrante?></div>
								<div class="data"><?=formataData($value->data, 'mysql2br')?></div>
							</div>
						</a>
					</li>
					<?$contador++?>
					<?if($contador == 12){
						echo "</ul><ul>";
						$contador = 0;
					}?>
				<?php endforeach ?>
			</ul>

		</div>

		<!-- <div id="botao-proxima">
			<a href="#" title="">&raquo;</a>
		</div> -->

		<a href="videos" title="Fazer nova busca" class="busca"><?=traduz('FAZER NOVA BUSCA DE VÍDEOS')?> &raquo;</a>
	</aside>