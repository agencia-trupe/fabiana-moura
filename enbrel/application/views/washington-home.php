<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">

	<h1><?=traduz('WASHINGTON DC')?></h1>

	<aside>
		<img src="_imgs/layout/washington-home.jpg" alt="<?=traduz('Washington DC')?>">

		<div id="titulo-categorias"><?=traduz('Saiba mais sobre Washington DC')?></div>

		<ul>
			<?php foreach ($categorias as $key => $value): ?>
				<li><a href="washington/categoria/<?=$value->slug?>" title="<?=$value->titulo?>"><?=traduz('Ver notícias sobre')?>: <span class="tit"><?=$value->titulo?></span></a></li>	
			<?php endforeach ?>
		</ul>
		
	</aside>

	<article>

		<div id="titulo-washington">
			<?=traduz('Informações sobre o evento:<br>localização, hospedagem e transporte')?>
		</div>

		<?=traduz('TEXTO_WASHINGTON_1')?>

		<div id="embed">
			<iframe width="450" height="318" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Walter+E.+Washington+Convention+Center,+Washington,+DC,+United+States&amp;aq=0&amp;oq=Walter+E.+Washington+Convention+Center&amp;sll=38.904777,-77.022936&amp;sspn=0.011438,0.022724&amp;gl=br&amp;g=801+Mt+Vernon+Pl+NW,+Washington,+DC+20001,+EUA&amp;ie=UTF8&amp;hq=Walter+E.+Washington+Convention+Center,+Washington,+DC,+United+States&amp;ll=38.905713,-77.022983&amp;spn=0.017366,0.032015&amp;t=m&amp;output=embed"></iframe>
		</div>

		<?=traduz('TEXTO_WASHINGTON_2')?>

	</article>
