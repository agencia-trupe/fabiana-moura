<div class="form-cadastro">
	
	<form name="form-cadastro" id="cad-form" method="post" action="home/atualizarCadastro">
		
		<div class="coluna cadastro">
			
			<h1>CADASTRO | REGISTRO | REGISTER</h1>

			<label>
				nome completo | nombre completo | full name
				<input type="text" name="nome" required id="input-nome" value="<?=$dados[0]->nome?>">
			</label>

			<label>
				e-mail
				<input type="email" name="email" disabled id="input-email" value="<?=$dados[0]->email?>">
			</label>

			<label>
				senha | contraseña | password
				<input type="password" name="senha" id="input-senha">
			</label>

			<div class="multi">
				<label class="primeiro menor">
					especialidade médica | especialidad médica | medical specialty
					<select name="especialidade" required id="select-especialidade">
						<option value=""></option>
						<?php foreach ($ESPECIALIDADES as $key => $value): ?>
							<option value="<?=($key + 1)?>" <?if($dados[0]->id_especialidade==$key)echo' selected'?>><?=$value?></option>	
						<?php endforeach ?>
					</select>
				</label>
				<label class="segundo">
					CRM | ID
					<input type="text" name="crm" required id="input-crm" value="<?=$dados[0]->crm?>">
				</label>			
			</div>

			<label class="primeiro">
				país | country
				<input type="text" name="pais" required id="input-pais" value="<?=$dados[0]->pais?>">
			</label>

			<label class="aceite">
				termo de aceite | aceptación plazo | term acceptance &bull; <a href="#" class="troca-linguagem" data-target="portugues" title="Ler o Termo de Aceite em Português">português</a> <a href="#" class="troca-linguagem" data-target="espanhol" title="Lea lo Plazo de Aceptación en español">español</a> <a href="#" class="troca-linguagem" data-target="ingles" title="Read The Disclaimer in English">english</a>
				<div class="texto">
					<div class="disc" id="portugues">
						<h2>Condições de Uso</h2>
						
						<p>
							Seja bem-vindo ao Website Reumatonline. As informações contidas neste site são fornecidas somente para fins educativos e de informações gerais.
						</p>

						<p>
							Por favor, leia e analise essas Condições de Uso atentamente antes de acessar ou usar este site. Acessando ou usando este site, você reconhece que leu, entendeu e concordou com o Acordo de Condições de Uso. Se você não concordar com as Condições de Uso, você não deve acessar ou usar este site.
						</p>

						<h2>Uso do Site</h2>

						<p>
							As informações fornecidas neste site são para fins educativos e de informação geral. Determinadas seções deste site destinam-se a audiências particulares incluindo empregados da Pfizer, consumidores e acionistas, assim como membros da comunidade de saúde e o público em geral. O acesso e uso das informações contidas no site estão sujeitos a este Acordo de Condições de Uso. Acessando e usando este site, você aceita, sem limitações ou qualificação, este Acordo de Condições de Uso.
						</p>
						 
						<h2>Conteúdo</h2>

						<p>
							A Pfizer e a Segmento Farma envidarão esforços razoáveis para incluir informações precisas e atualizadas neste site, mas não há nenhuma garantia ou representação de nenhum tipo quanto a sua precisão, aceitação geral e totalidade. Você concorda em acessar e usar este site e o conteúdo dele por sua conta e risco. A Pfizer e a Segmento Farma negam todas as garantias, expressas ou implícitas, incluindo garantias de negociabilidade de adequação a uma finalidade particular. Nem a Pfizer nem qualquer parte envolvida na criação, produção ou distribuição deste site poderá ser responsabilizada por quaisquer danos que surjam do acesso, uso ou incapacidade de usar este site, ou quaisquer erros ou omissões no conteúdo dele. A Pfizer também não responderá por danos ou vírus que possam vir a infectar o computador ou equipamento ou outros bens decorrentes do acesso do usuário ao site ou da navegação por este ou por descarregar quaisquer materiais, dados, textos, imagens, vídeos ou áudios incluídos no site.
						</p>

						<h2>Identificação</h2>

						<p>	
							Você concorda em identificar, defender e não prejudicar a Pfizer e a Segmento Farma, seus dirigentes, diretores, empregados, agentes, fornecedores e outros parceiros de e contra todas as perdas, despesas, danos e custos, incluindo honorários advocatícios razoáveis, resultantes de qualquer violação por sua parte dessas Condições de Uso.
						</p>

						<h2>Privacidade</h2>

						<p>
							A Pfizer e a Segmento Farma respeitam a privacidade dos usuários de seus sites. Por favor, consulte a Política de Privacidade da Pfizer que explica os direitos e as responsabilidades dos usuários em relação à informação que é divulgada neste site.
						</p>

						<h2>Websites e Links de Terceiros</h2>
						 
						<p>
							Este site pode conter links ou referências a outros sites mantidos por terceiros sobre os quais a Pfizer e a Segmento Farma não têm controle tampouco endossa qualquer um dos sites aos quais seu site tem um link, considerando que a Pfizer e a Segmento Farma não verificaram nenhum dos referidos sites, o usuário está ciente de que a Pfizer e a Segmento Farma não se responsabilizam pelo conteúdo de páginas em sites alheios. Tais links são fornecidos meramente como uma conveniência. Da mesma forma, este site pode ser acessado a partir de links de terceiros sobre os quais a Pfizer e a Segmento Farma não têm nenhum controle. A Pfizer e a Segmento Farma não dão nenhuma garantia ou representações de qualquer tipo quanto a precisão, aceitação geral ou totalidade de qualquer informação contida nesses sites e não deve ser responsabilizada por nenhum dano ou prejuízo de qualquer tipo que surja a partir de tal conteúdo ou informação. A inclusão de qualquer link de terceiros não implica endosso ou recomendação por parte da Pfizer e da Segmento Farma.
						</p>

						<h2>Informação Médica</h2>

						<p>
							Este site pode conter informações gerais relacionadas a várias condições médicas e seu tratamento. Tais informações são fornecidas para fins informativos somente e não significa que substituam a orientação fornecida por um médico ou outro profissional de saúde qualificado. Você não deve utilizar a informação contida aqui para diagnosticar um problema de saúde ou aptidão ou doença. Você deve sempre consultar um médico ou outro profissional de saúde qualificado.
						</p>
						 
						<h2>Declarações para Ver Adiante</h2>

						<p>
							Este site pode conter declarações para ver adiante que estão sujeitas a riscos e incertezas que podem fazer com que resultados reais sejam diferentes daqueles observados, incluindo riscos detalhados nos relatórios anuais da Pfizer arquivados com a Comissão de Segurança e Permuta (a "SEC", Securities and Exchange Comission), incluindo o relatório da Pfizer mais recentemente arquivado na SEC.
						</p>

						<h2>Informação Não Confidencial</h2>

						<p>
							Sujeita a quaisquer termos aplicáveis e condições estabelecidas em nossa Política de Privacidade, qualquer comunicação ou outro material que você enviar para nós através da internet ou divulgar no site por correio eletrônico ou outra forma, tais como quaisquer perguntas, comentários, sugestões ou similar, é e será considerada como não confidencial e não protegida e a Pfizer e a Segmento Farma não terão nenhuma obrigação de qualquer tipo com relação a tal informação. A Pfizer e a Segmento Farma deverão estar livres para usar quaisquer ideias, conceitos, know-how ou técnicas contidas em tal comunicação para qualquer finalidade, incluindo, mas não limitada a, desenvolvimento, fabricação e comercialização de produtos.
						</p>

						<h2>Marcas Registradas</h2>

						<p>
							Todos os nomes de produto, apareçam ou não em grande impressão ou com o símbolo da marca, são marcas registradas da Pfizer, suas afiliadas, empresas relacionadas ou seus detentores de licença ou parceiros de empreendimento conjunto, a menos que observado de outra forma. O uso ou uso impróprio dessas marcas registradas ou de qualquer outro material, exceto quando permitido, é expressamente proibido e pode ser uma violação da legislação de direitos autorais, lei de marca registrada, lei de privacidade e publicidade e regulamentos e estatutos de comunicações. Por favor, esteja avisado que a Pfizer exerce ativa e agressivamente seus direitos de propriedade intelectual na mais ampla extensão da lei.
						</p>
						 
						<h2>Direitos Autorais (Copyrights)</h2>
						 
						<p>
							O inteiro teor deste site está sujeito à proteção de direitos autorais. Copyright © 2012 Pfizer inc. Todos os direitos reservados. O conteúdo deste site não pode ser copiado de forma diferente da referência individual comercial com todos os direitos autorais ou outras notas de propriedade retidas, e depois não pode ser recopiado, reproduzido ou de outra forma redistribuído. Exceto quando expressamente proibido acima, você não deve de outra forma copiar, mostrar, baixar, distribuir, modificar, reproduzir, republicar ou retransmitir qualquer informação, texto e/ou documentos contidos neste site ou qualquer parte deste em qualquer meio eletrônico ou em disco rígido, ou criar qualquer trabalho derivado com base nessas imagens, texto ou documentos, sem o consentimento expresso por escrito da Pfizer. Nada contido aqui será interpretado como conferido através de implicação, interdição judiciária ou caso contrário qualquer licença ou direito sob qualquer patente ou marca registrada da Pfizer, ou de qualquer terceiro.
						</p>

						<h2>Legislação Aplicável</h2>

						<p>
							Este site e seu conteúdo foram elaborados e serão regidos de acordo com as leis e regulamentos da República Federativa do Brasil. Embora a informação contida neste site seja acessível a usuários em outras localidades, as informações pertencentes a produtos Pfizer destinam-se a uso somente por profissionais de saúde em obediência à legislação e exigências regulatórias em vigor. Outros países podem ter leis, exigências regulatórias e práticas médicas que diferem das do Brasil. Este site possui links com outros sites produzidos pelas várias divisões operantes e subsidiárias, algumas das quais, fora do Brasil. Esses sites podem ter informações que sejam apropriadas somente àquele país de origem particular. A Pfizer reserva-se o direito de limitar o fornecimento de seus produtos ou serviços a qualquer pessoa, região geográfica ou jurisdição e/ou a limitar as quantidades ou quaisquer produtos ou serviços que fornecemos. O fato de haver uma menção a um produto ou serviço da Pfizer em um site de qualquer uma de suas divisões, subsidiárias ou coligadas fora do Brasil não significa que o referido produto ou serviço estará disponível na localidade específica do usuário. O usuário deverá ter como base as informações criadas especificamente para sua localidade. Qualquer oferta para qualquer produto ou serviço feita neste site será considerada como inválida nos locais onde for legalmente proibida. Qualquer ação legal ou procedimento relacionado a este site terá como foro competente o da cidade de São Paulo, Estado de São Paulo.
						</p>

						<h2>Diversos</h2>

						<p>
							Se qualquer disposição deste Acordo for considerada ilegal, inválida ou inexequível, então essa disposição deverá ser cortada sem afetar a exequibilidade de todas as disposições remanescentes. A Pfizer e a Segmento Farma reservam-se o direito de alterar ou excluir materiais deste site a qualquer momento e a seu critério.
						</p>

						<h2>Política de Privacidade</h2>

						<p>
							A Pfizer e a Segmento Farma reconhecem a importância de proteger a privacidade das informações que pode coletar de seus usuários on-line. Para essa finalidade, comprometem-se a envidar seus melhores esforços para respeitar a privacidade de seus usuários on-line. Elas pretendem equilibrar seus verdadeiros interesses comerciais na coleta e utilização das informações recebidas de você e sobre você, com suas expectativas razoáveis de privacidade. A seguir, veja a maneira como a Pfizer e a Segmento Farma lidam com as informações obtidas sobre você ao visitar nosso website.
						</p>
						 
						<h2>Dados Coletados</h2>
						 
						<p>
							Alguns sites da Pfizer enviam alguns bits de informações ao computador de um usuário, denominados "cookies". Quando um usuário retorna ao mesmo site, os cookies são enviados de volta somente para o site que o enviou. Os cookies podem dizer como e quando as páginas de um site são visitadas, e por quantas pessoas. Essa tecnologia não coleta informações identificadas como pessoais; as informações coletadas estão em uma forma acumulada e não identificável. Com as informações que obtemos através dessa tecnologia, esperamos aperfeiçoar nossos sites.
						</p>

						<h2>Informações Identificáveis como Pessoais</h2>

						<p>
							A Pfizer e a Segmento Farma coletam informações identificáveis como pessoais, tais como nomes, endereços, endereços eletrônicos etc., somente quando fornecido voluntariamente por um usuário de um site. A Pfizer e a Segmento Farma o notificarão quanto à utilização que pretende fazer dessas informações.		
						</p>

						<h2>Links para Outros Sites</h2>

						<p>
							Como recurso para seus usuários, a Pfizer e a Segmento Farma fornecem links para outros sites. Tentamos escolher cuidadosamente sites que acreditamos ser úteis e que atendam a nossos altos padrões.
						</p>
						<p>
							Entretanto, devido ao seu rápido poder de mudança, não podemos garantir os padrões de cada link de site que fornecemos nem nos responsabilizar pelo conteúdo de sites que não sejam da Pfizer.
						</p>

						<h2>Crianças</h2>

						<p>
							Não coletamos especificamente informações de crianças.
						</p>

						<h2>Alterações</h2>

						<p>
							Quaisquer alterações em nossa política de privacidade serão imediatamente comunicadas nesta página.
						</p>

					</div>
					<div class="disc" id="ingles">
						<h2>Conditions of Use</h2>
						
						<p>
							Welcome to the Website Reumatonline. The information on this website is provided for educational purposes only and general information.
						</p>

						<p>
							Please read and review these Terms of Use carefully before accessing or using this site. By accessing or using this site, you acknowledge that you have read, understood, and agreed to the Terms of Use Agreement If you do not agree to the Terms of Use, you must not access or use this site.
						</p>

						<h2>Use of Site</h2>

						<p>
							The information provided on this website it’s for educational purposes and general information. Certain sections of this website intended for particular audiences including Pfizer's employees, customers and shareholders, as well as members of the health community and the general public. The access and use of the information contained in this website is subject to Terms of Use Agreement By accessing and using this website, you accept, without limitation or qualification, this Terms of Use Agreement
						</p>
						 
						<h2>Content</h2>

						<p>
							Pfizer and Segmento Farma shall use reasonable efforts to include accurate and timely information on this website but makes no warranties or representations of any kind as to its accuracy, currency or completeness. You agree to access and use this website and the content thereof is at your own risk. Pfizer and Segmento Farma deny all warranties, express or implied, including warranties of merchantability of fitness for a particular purpose. Neither Pfizer nor any party involved in creating, producing or delivering this website be liable for any damages arising from access, use or inability to use this website, or any errors or omissions in the content thereof. A PFIZER also not liable for damages or viruses that may infect, your computer equipment or other property resulting from user's access to the website or inland by the same or download any materials, data, text, images, video or audio from the site.
						</p>

						<h2>Identification</h2>

						<p>	
							You agree to indemnify, defend and do no harm to Pfizer and Segmento Farma, its officers, directors, employees, agents, vendors and other partners from and against all losses, expenses, damages and costs, including reasonable attorneys' fees, resulting from any violation by you of these Terms of Use
						</p>

						<h2>Privacy</h2>

						<p>
							Pfizer and Segmento Farma respect the privacy of users of its websites. Please see the Privacy Policy for Pfizer which explains users' rights and responsibilities with respect to information that is disclosed on this website.
						</p>

						<h2>Links and Third Part Websites</h2>
						 
						<p>
							This website may contain links or references to other websites maintained by third parties over which Pfizer and Segmento Farma not have either control or endorse any of the sites to which your site has a link and considering that Pfizer and Segmento Farma not found none of these sites, you acknowledge that Pfizer and Segmento Farma not responsible for the content of pages on sites unrelated. Such links are provided merely as a convenience. Similarly, this website may be accessed from third party links over which Pfizer and Segmento Farma have no control. Pfizer and Segmento Farma make no warranties or representations of any kind as to the accuracy or completeness of any information contained in such websites and shall not be liable for any damage or injury of any kind arising from such content or information . The inclusion of any third party link does not imply endorsement or recommendation by Pfizer and Segmento Farma.
						</p>

						<h2>Medical Information</h2>

						<p>
							This website may contain general information relating to various medical conditions and their treatment. Such information is provided for informational purposes only and is not meant to substitute for advice provided by your physician or other qualified health professional. You should not use the information contained herein for diagnosing a health or fitness problem or disease. You should always consult a doctor or other qualified health professional.
						</p>
						 
						<h2>Claims to Look Futher</h2>

						<p>
							This website may contain statements to look further that are subject to risks and uncertainties that could cause actual results to differ from those observed, including risks detailed in Pfizer's annual reports filed with the Security and Exchange Commission (the "SEC" Securities and Exchange Commission), including Pfizer's most recent report filed with the SEC.
						</p>

						<h2>Non-Confidential Information</h2>

						<p>
							Subject to any applicable terms and conditions set in our Privacy Policy, any communication or other material you send to us through the internet or post on the website by electronic mail or otherwise, such as any questions, comments, suggestions or similar, is and will be considered non-confidential and nonproprietary and Pfizer and Segmento Farma shall have no obligation of any kind with respect to such information. Pfizer and Segmento Farma shall be free to use any ideas, concepts, know-how or techniques contained in such communication for any purpose, including but not limited to, developing, manufacturing and marketing products.
						</p>

						<h2>Trademarks</h2>

						<p>
							All product names, whether or not appearing in large print or with the trademark symbol, are trademarks of Pfizer, its affiliates, related companies or its licensors or joint venture partners, unless otherwise noted. The use or misuse of these trademarks or any materials, except as permitted herein, is expressly prohibited and may be a violation of copyright law, trademark law, the law of privacy and publicity, and communications regulations and statutes. Please be advised that Pfizer actively and aggressively enforces its intellectual property rights to the fullest extent of the law.
						</p>
						 
						<h2>Copyright</h2>
						 
						<p>
							The entire content of this website is subject to copyright protection. Copyright © 2012 Pfizer inc. All rights reserved. The content of this website may not be copied differently from commercial individual reference with all copyright or other proprietary notices retained, and thereafter may not be recopied, reproduced or otherwise redistributed. Except as expressly provided above, you may not otherwise copy, display, download, distribute, modify, reproduce, republish or retransmit any information, text and / or documents contained on this website or any portion thereof in any electronic medium or in hard copy or create any derivative work based on such images, text or documents, without the express written consent of Pfizer. Nothing contained herein shall be construed as conferring by implication, estoppel judicial or otherwise any license or right under any patent or trademark of Pfizer, or any third party.
						</p>

						<h2>Applicable Legislation</h2>

						<p>
							This website and its contents have been prepared and will be governed according to the laws and regulations of the Federative Republic of Brazil. Although the information contained in this website is accessible to users in other locations, the information pertaining to Pfizer products are intended for use only by healthcare professionals in compliance with legislation and regulatory requirements in force. Other countries may have laws, regulatory requirements and medical practices that differ from those of Brazil. This site contains links to other sites produced by the various operating divisions and subsidiaries, some of which, outside of Brazil. These sites may have information that is appropriate only to that particular country of origin. Pfizer reserves the right to limit the provision of their products or services to any person, geographic region or jurisdiction and / or to limit the quantities or any products or services that we provide. The fact that there is a mention of a product or service at a Pfizer site of any of its divisions, subsidiaries or affiliates outside Brazil do not imply that such product or service will be available in the specific location of the user. The user should be based on the information created specifically for your location. Any offer for any product or service made in this website shall be deemed invalid in places where legally prohibited. Any legal action or proceeding related to this website will be the jurisdiction of the city of São Paulo, State of Sao Paulo.
						</p>

						<h2>Several</h2>

						<p>
							If any provision of this Agreement is held illegal, invalid or unenforceable, then that provision shall be severable without affecting the enforceability of all remaining provisions. Pfizer and Pharma Segment reserve the right to alter or delete materials from this website at any time at its discretion.
						</p>

						<h2>Privacy Policy</h2>

						<p>
							Pfizer and Segmento Farma recognize the importance of protecting the privacy of information we may collect from our online users. To this end, commit  to use its best efforts to respect the privacy of its online users. They intend to balance our legitimate business interests in collecting and using information received from and about you with your reasonable expectations of privacy. Then see how Pfizer and Segmento Farma dealing with information obtained about you when you visit our website.
						</p>
						 
						<h2>Collected Data</h2>
						 
						<p>
							Some websites from Pfizer send some bits of information to a user's computer called "cookies". When a user returns to the same site, cookies are sent back only to the website that sent him. Cookies can tell us how and when pages in a website are visited and by how many people. This technology does not collect personal identifiable information. The information collected is in an aggregate, non-identifiable. With the information we collect through this technology, we hope to improve our sites.
						</p>

						<h2>Personal Identifiable Information</h2>

						<p>
							Pfizer and Segmento Farma collect personally identifiable information such as names, addresses, email addresses, etc.., Only when voluntarily provided by a user of a website. Pfizer and Segmento Farma will notify you as to the uses we intend to make such information.	
						</p>

						<h2>Links to Other Sites</h2>

						<p>
							As a resource to our users, Pfizer and Segmento Farma provide links to other websites. We try to carefully choose websites which we believe are useful and meet our high standards.
						</p>
						<p>
							However, due to the rapid change of power, we can not guarantee the standards of every website link we provide or be responsible for the content of sites other than Pfizer.
						</p>

						<h2>Children</h2>

						<p>
							Not specifically collect information about children.
						</p>

						<h2>Changes</h2>

						<p>
							Any changes to our privacy policy will be communicated promptly on this page.
						</p>	
					</div>
					<div class="disc" id="espanhol">
						<h2>Condiciones de uso</h2>
						
						<p>
							Sea usted bienvenido al website Reumatoline. La información contenida es este sirio es requerida solo para fines educativos y de información general.
						</p>

						<p>
							Por favor, lea y analice estas Condiciones de uso atentamente antes de ingresar o usar este sitio. Ingresando o usando este sitio, usted reconoce que leyó, entendió y está de acuerdo con el Acuerdo de Condiciones de Uso. Si usted no está de acuerdo con las Condiciones de Uso, usted no debe ingresar a este sitio.
						</p>

						<h2>Uso del sitio</h2>

						<p>
							La información requerida en este tipo de sitios es para fines educativos y de información general. Determinadas secciones de este website se destinan a audiencias particulares, incluyendo empleados de Pfizer, consumidores y accionistas, así como miembros de la comunidad de la salud y público en general. Su acceso y uso de la información contenida en el website están sujetos a este Acuerdo de Condiciones de Uso. Ingresando y usando este website, usted acepta, sin limitaciones o calificación, este Acuerdo de Condiciones de Uso.
						</p>
						 
						<h2>Contenido</h2>

						<p>
							Pfizer y Segmento Farma hicieron esfuerzos razonables para incluir información precisa y actualizada en este website pero no ofrece ninguna garantía o representación de ningún tipo en cuanto a su precisión, aceptación general y total. Usted está de acuerdo con ingresar y usar este website y el contenido del mismo por su propia cuenta y riesgo. Pfizer y Segmento Farma niegan todas las garantías de negociabilidad de adecuación a una finalidad particular. Ni Pfizer ni cualquier parte involucrada en la creación, producción o distribución de este website podrá ser responsabilizada por cualesquier daño que surja del acceso, uso o incapacidad de usar este website, o cualesquier error u omisión en el contenido del mismo. Pfizer tampoco responderá por daños o virus que puedan infectar el computador u otros bienes producto del acceso del usuario a este sitio o de la navegación por el mismo o por descargar cualesquier material, datos, textos, imágenes, videos o audios incluidos en el sitio.
						</p>

						<h2>Identificación</h2>

						<p>	
							Usted está de acuerdo con identificar, defender y no perjudicar a Pfizer y a Segmento Farma,  a sus dirigentes, directores, empleados, agentes, surtidores y otros socios de y contra todas las pérdidas, despensas, daños y costos, incluyendo honorarios legales razonables, resultantes de cualquier violación por su parte de estas Condiciones de Uso.
						</p>

						<h2>Privacidad</h2>

						<p>
							Pfizer y Segmento Farma respetan la privacidad de los usuarios de sus websites. Por favor, consulte la Política de Privacidad de Pfizer que explica los derechos y responsabilidades de los usuarios en relación con la información que es divulgada en este website.
						</p>

						<h2>Websites y Links de Terceros</h2>
						 
						<p>
							Este website puede contener links o referencias a otros websites mantenidos por terceros sobre los cuales Pfizer y Segmento Farma no tienen control o tampoco endosa cualquiera de los sites a los cuales su site tiene un link  y considerando que Pfizer y Segmento Farma no verificar ninguno de los sitios referidos, el usuario está consciente de que Pfizer y Segmento Farma no se responsabilizan por el contenido de páginas en  sitios ajenos. Tales links son proporcionados meramente como una conveniencia. De la misma forma, este website puede ser ingresado a partir de links de terceros sobre los cuales Pfizer y Segmento Farma no tienen ningún control. Pfizer y Segmento Farma no ofrecen ninguna garantía o representaciones de cualquier tipo en cuanto a precisión, aceptación general o total de cualquier información contendida en esos websites y no debe ser responsabilizada por ningún daño o perjuicio de cualquier tipo que surja a partir de tal contenido o información. La inclusión de cualquier link de terceros no implica el endoso o recomendación por parte de Pfizer y Segmento Farma.
						</p>

						<h2>Información médica</h2>

						<p>
							Este website puede contener información general relacionada con varias condiciones médicas y su tratamiento. Tales informaciones son proporcionadas para fines educativos solamente y no significa que sustituyas la orientación ofrecida por un médico u otro profesional de salud calificado. Usted no debe utilizar la información contenida aquí para diagnosticar un problema de salud o aptitud o dolencia. Usted debe siempre consultar un médico u otro profesional de la salud calificado.
						</p>
						 
						<h2>Declaraciones para Ver Adelante</h2>

						<p>
							Este website puede contener declaraciones para ver adelante que están sujetas a riesgos y dudas que pueden hacer que resultados reales sean diferentes de aquellos observados, incluyendo riesgos detallados en los informes anuales de Pfizer archivados con la Comisión de Seguridad y Permuta (la Secretes and Exchange Comisión, SEC), incluyendo el informe de Pfizer más recientemente archivado en la SEC.
						</p>

						<h2>Información no confidencial</h2>

						<p>
							Sujeta a cualesquier término aplicable y condiciones establecidas en nuestra Política de Privacidad, cualquier comunicación u otro material que usted nos envíe a través de Internet o divulgue en el website por correo electrónico u otro forma como cualesquier pregunta, comentario, sugerencia o similar es y será considerada como no confidencial y no estará protegida y Pfizer y Segmento Farma no tendrán ninguna obligación de cualquier tipo en relación con dicha información. Pfizer y Segmento Farma están en la libertad de usar cualquier idea, concepto, know-how o técnicas contenidas en tal comunicación para cualquier finalidad, incluyendo, aunque no limitada, el desarrollo, fabricación y comercialización de productos.
						</p>

						<h2>Marcas registradas</h2>

						<p>
							Todos los nombres de productos, aparezcan o no en formato grande o con el símbolo de la marca, son marcas registradas de Pfizer, sus afiliadas, empresas relacionadas o sus detentadores de licencia o socios de emprendimiento conjunto, a menos que sea expresado de otra forma. El uso o uso impropio de estas marcas registradas o de cualquier otro material, excepto cuando sea permitido, está expresamente prohibido y puede ser una violación de la legislación de derechos de autor, ley de marca registrada, la ley de privacidad y publicidad y reglamentos y estatutos de comunicaciones. Por favor, tenga que en cuenta que Pfizer ejerce activa y agresivamente sus derechos de propiedad intelectual en la más amplia extensión de la ley.
						</p>
						 
						<h2>Derechos de autor (Copyrights)</h2>
						 
						<p>
							El contenido entero de este website está sujeto a la protección de derechos de autor. Copyright © 2012 Pfizer Inc. Todos los derechos reservados. El contenido de este website no puede ser copiado de forma diferente de la referencia individual comercial como todos los derechos de autor u otras notas de propiedad retenida, y después, no puede ser recopiado, reproducido o redistribuido de otra forma. Excepto cuando esté prohibido encima, usted no debe copiar, mostrar, descargar, distribuir, modificar, reproducir, republicar o retransmitir de otra forma cualquier información, texto y/o documentos contenidos en este website o cualquier parte de este en cualquier medio electrónico o en disco duro, o crear cualquier trabajo derivado con base en nuestras imágenes, texto o documentos, sin el consentimiento expreso por escrito de Pfizer. Nada contenido aquí será interpretado como conferido mediante implicación, interdicción judicial o, caso contrario, cualquier licencia o derecho sobre cualquier patente o marca registrada de Pfizer, o de cualquier tercero.
						</p>

						<h2>Legislación aplicable</h2>

						<p>
							Este website y su contenido fueron elaborados y serán regidos de acuerdo con las leyes y reglamentos de la República Federal de Brasil. Sin embargo, la información contenida en este website será accesible a usuarios de otras localidades, la información perteneciente a productos Pfizer se destina al uso de profesionales de la salud solamente obedeciendo la legislación y las exigencias de regulación en vigor. Otros países pueden tener leyes, exigencias regulatorias y prácticas médicas que difieren de las de Brasil. Este sitio posee links con otros sitios producidos por las distintas divisiones operantes y subsidiarias, algunas de las cuales están fuera de Brasil. Esos sitios pueden tener información que sea apropiada solamente para aquel país en particular. Pfizer se reserva el derecho de limitar el proporcionamiento de sus productos o servicios o cualesquier producto o servicio que ofrezcamos. El hecho de que haya una mención a un producto o servicio de Pfizer en un sitio de cualquiera de sus divisiones, subsidiarias o coligada fuera de Brasil no significa que el producto o servicio referido estará disponible en la localidad específica del usuario. El usuario deberá tener como base la información creada específicamente para su localidad. Cualquier oferta para cualquier producto o servicio hecha en este sitio será considerada como inválida en los locales donde fuera legalmente prohibida. Cualquier acción legal o procedimiento relacionado con este website tendrá como campo competente la ciudad de São Paulo, Estado de São Paulo.
						</p>

						<h2>Diversos</h2>

						<p>
							Si cualquier disposición de este Acuerdo fuera considerada ilegal, inválida o inexequible entonces esa disposición deberá ser cortada sin afectar la asequibilidad de todas las disposiciones remanentes. Pfizer y Segmento Farma se reservan el derecho de alterar o excluir materiales de este website en cualquier momento, según su criterio.
						</p>

						<h2>Política de privacidad</h2>

						<p>
							Pfizer y Segmento Farma reconocer la importancia de proteger la privacidad de la información que puede recolectar de sus usuarios on-line. Con esta finalidad se compromete a emplear sus mejores esfuerzos para respetar la privacidad de sus usuarios on-line. Pfizer y Segmento Farma pretenden equilibrar sus verdaderos intereses comerciales en la recolección y utilización de información recibida por usted y sobre usted, con sus expectativas razonables de privacidad. A continuación observe la manera como Pfizer y Segmento Farma lidian con la información obtenida sobre usted al visitar nuestro website.
						</p>
						 
						<h2>Datos recolectados</h2>
						 
						<p>
							Algunos websites de Pfizer envían algunos bits de información al computador de un usuario, denominados “cookies”. Cuando un usuario retorna al mismo sitio, las cookies son enviadas de vuelta solamente para el website que lo envió. Las cookies pueden decir cómo y cuándo las páginas de un website son visitadas y por cuántas personas. Esa tecnología no recolecta información identificada como personal; la información recolectada está en una forma acumulada y no identificable. Con la información que obtenemos mediante esta tecnología esperamos perfeccionar nuestros sitios.
						</p>

						<h2>Información identificable como personal</h2>

						<p>
							Pfizer y Segmento Farma recolectan información identificable como personal como nombres, direcciones electrónicas, etc. solamente cuando es proporcionada de forma voluntaria por un usuario de un website. Pfizer y Segmento Farma lo notificarán en cuanto a la utilización que pretende hacer de esa información personal.	
						</p>

						<h2>Links para otros sites</h2>

						<p>
							Como recurso para sus usuarios, Pfizer y Segmento Farma proporcionan links para otros websites. Intentamos escoger cuidadosamente websites que pensamos son útiles y que atienden a nuestros altos patrones.
						</p>
						<p>
							Entretanto, debido a su rápido poder de cambio, no podemos garantizar los patrones de cada link de website que proporcionamos, ni nos podemos responsabilizar por el contenido de sites que no sean de Pfizer.
						</p>

						<h2>Niños</h2>

						<p>
							No recolectamos, específicamente, información de niños.
						</p>

						<h2>Alteraciones</h2>

						<p>
							Cualesquier alteración en nuestra Política de Privacidad será comunicada en esta página de inmediato.
						</p>
					</div>
				</div>
			</label>

			<label class="check">
				<input type="checkbox" name="aceite" id="aceite" value="1" checked required>Li e aceito os termos acima | Leí y acepto los términos de arriba | Have read and accept the above terms
			</label>

		</div>

		<div class="coluna preferencias">

			<h1 style="font-size:16px;">MINHAS PREFERÊNCIAS | MIS PREFERENCIAS | MY PREFERENCES</h1>

			<span class="label">idioma da interface | idioma de la interfaz | interface language</span>
			<div class="box short">
				<label><input required type="radio" value="1" <?if($dados[0]->idioma_interface==1)echo" checked"?> name="idioma_interface">português | portugués | portuguese</label><label><input required type="radio" value="3" <?if($dados[0]->idioma_interface==3)echo" checked"?> name="idioma_interface">espanhol | español | spanish</label>
			</div>

			<span class="label">
				exibir sempre conteúdo apenas em:<br>
				exhibir siempre contenidos solo en:<br>
				always display content only:<br>
			</span>
			<div class="box short">
				<label><input required type="radio" value="1" <?if($dados[0]->idioma_conteudo==1)echo" checked"?> name="idioma_conteudo">português | portugués | portuguese</label><label><input required type="radio" value="3" <?if($dados[0]->idioma_conteudo==3)echo" checked"?> name="idioma_conteudo">espanhol | español | spanish</label><label><input required type="radio"  <?if($dados[0]->idioma_conteudo==4)echo" checked"?> value="4" name="idioma_conteudo">ambos | ambos | both</label>
			</div>

			<span class="label">
				exibir apenas conteúdos relacionados aos temas:<br>
				mostrar sólo contenido relacionado con los temas:<br>
				display only content related to topics:<br>
			</span>
			<div class="box conteudos">
				<?php foreach ($temas as $key => $value): ?>
					<label><input type="checkbox" name="temas[]" <?if(in_array($value->id, $usuarios_temas))echo" checked"?> value="<?=$value->id?>"><?=$value->titulo?> | <?=$value->titulo_es?> | <?=$value->titulo_en?></label>	
				<?php endforeach ?>
				
			</div>
			
			
		</div>

		<div id="dialog"></div>

		<input type="submit" value="<?=traduz('SALVAR PREFERÊNCIAS')?>">

	</form>

</div><div>


<script defer>
	$('document').ready( function(){

		<?php if($this->session->flashdata('erro-form')):?>
			alert("<?php echo $this->session->flashdata('erro-form')?>");
		<?php endif;?>

		$('.troca-linguagem').click( function(e){
			e.preventDefault();
			var ling = $(this).attr('data-target');
			$('.texto .disc').css('overflow-y', 'hidden');
			$('.texto .disc:visible').fadeOut('normal', function(){
				$('.texto .disc#'+ling).fadeIn('normal', function(){
					$(this).css('overflow-y', 'scroll');
				});
			});
		});

		$('#cad-form').submit( function(){
			if($('#input-nome').val()==''){
				alert('Informe seu nome.');
				$('#input-nome').focus();
				return false;
			}
			if($('#select-especialidade').val()==''){
				alert('Informe sua especialidade.');
				$('#select-especialidade').focus();
				return false;
			}
			if($('#input-crm').val()==''){
				alert('Informe seu CRM.');
				$('#input-crm').focus();
				return false;
			}
			if($('#input-pais').val()==''){
				alert('Informe seu país.');
				$('#input-pais').focus();
				return false;
			}
			if($('#aceite').val()==''){
				alert('Para se cadastrar é necessário concordar com os termos de serviço.');
				$('#aceite').focus();
				return false;
			}
		});

	});
</script>