<div class="login">
	
	<h1><?=traduz('IDENTIFIQUE-SE')?></h1>

	<!-- TRADUZIR -->

	<?=traduz('TEXTO_IDENTIFIQUE-SE')?>

	<form name="login" action="login/logar">
		<input type="text" name="login" placeholder="<?=traduz('LOGIN')?>" required><input type="password" name="<?=traduz('SENHA')?>" placeholder="<?=traduz('SENHA')?>" required><input type="submit" value="OK">
	</form>
	<a href="recuperacao" class="recuperacao" title="<?=traduz('esqueci minha senha')?>"><?=traduz('esqueci minha senha')?> &raquo;</a>
 	
 	<br><br><br>

 	<?=traduz('TEXTO_CADASTRE-SE')?>

 	<a href="cadastro" class="cadastro" title="<?=traduz('Cadastre-se!')?>">
 		<span class="maior"><?=traduz('Cadastre-se!')?></span><br>
 		<?=traduz('Acesso a todos os conteúdos!')?>
 	</a>

 	<?=traduz('Agradecemos a compreensão.')?>

	<div id="separador"></div>

	<!-- TRADUZIR -->
	<div class="disclaimer-cadastro">
		<?=traduz('REGULAMENTO_ANVISA')?>
	</div>

</div>

<style type="text/css">
	@font-face {
	    font-family: 'Myriad Web Pro Cond';
	    src: url('../css/fontface/myriadwebpro-condensed-webfont.eot');
	    src: url('../css/fontface/myriadwebpro-condensed-webfont.eot?#iefix') format('embedded-opentype'),
	         url('../css/fontface/myriadwebpro-condensed-webfont.woff') format('woff'),
	         url('../css/fontface/myriadwebpro-condensed-webfont.ttf') format('truetype'),
	         url('../css/fontface/myriadwebpro-condensed-webfont.svg#myriad_web_pro_condensedRg') format('svg');
	    font-weight: normal;
	    font-style: normal;
	}

	.login{
		text-align:center;
		color:#293F66;
		font-family:'Verdana','Arial',sans-serif;
		font-size:12px;
	}
	.login h1{
		font-size:30px;
		font-family:'Myriad Web Pro Cond','Verdana','Arial', sans-serif;
	}
	.login form{
		margin:15px auto 10px auto;
		width:600px;
	}
	.login form input[type="password"],
	.login form input[type="text"]{
		margin:0;
		padding:0;
		border:1px #293F66 solid;
		-webkit-border-radius:3px;
		-moz-border-radius:3px;
		-ms-border-radius:3px;
		border-radius:3px;
		text-align:center;
		margin-right:3px;
		width:250px;
		height:20px;
		line-height:20px;
	}
	.login form input[type="submit"]{
		-webkit-border-radius:3px;
		-moz-border-radius:3px;
		-ms-border-radius:3px;
		border-radius:3px;		
		width:40px;
		text-align:center;
		height:20px;
		line-height:20px;
		border:0;
		padding:0;
		margin:0;
		background-color:#293F66;
		color:#FFF;
		font-size:12px;
		vertical-align:top;
		-webkit-transition:color .3s ease-in;  
	   	-moz-transition:color .3s ease-in;  
	   	-o-transition:color .3s ease-in;  
	   	transition:color .3s ease-in;		
		font-family:'Myriad Web Pro Cond','Verdana','Arial', sans-serif;
	}
	.login form input[type="submit"]:hover{ color:#00B0DC; }
	.login a.recuperacao{
		font-family:'Verdana','Arial', sans-serif;
		font-size:10px;
		text-decoration:none;
		color:#293F66;
	}
	.login a.recuperacao:active, .login a.recuperacao:visited{color:#293F66;}
	.login a.recuperacao:hover{text-decoration:underline;}
	.login form input[type="submit"]:hover{ cursor:pointer; }
	.login a.cadastro{
		display:block;
		margin:15px auto;
		background-color:#293F66;
		color:#00B0DC;
		width:260px;
		height:36px;
		line-height:150%;
		padding:12px 0;
		text-decoration:none;
		-webkit-border-radius:5px;
		-moz-border-radius:5px;
		-ms-border-radius:5px;
		border-radius:5px;
		-webkit-transition:all .3s ease-in;  
	   	-moz-transition:all .3s ease-in;  
	   	-o-transition:all .3s ease-in;  
	   	transition:all .3s ease-in;		
	}
	.login a.cadastro:active, .login a.cadastro:visited{color:#00B0DC}
	.login a.cadastro:hover{ color:#fff; }
	.login a.cadastro .maior{font-size:20px;}
	.login #separador{
		height:1px;
		background-color:#293F66;
		margin:30px 0;
	}
	.login .disclaimer-cadastro{
		font-size:10px;
	}
	.login .fail{
		padding:7px;
		color:#FF2A55;
		font-family:'Verdana','Arial', sans-serif;
		font-size:10px;
	}
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<link rel='stylesheet' href='<?=base_url('css/fancybox/fancybox.css')?>'>
<script src="<?=base_url('js/fancybox.js')?>"></script>
<script defer>
var BASE = '<?=base_url()?>';
function abreRecuperarSenha(){
    $.fancybox(BASE+'login/recuperacao',{
      padding       : 50,
      modal         : false,
      width         : 800,
      height        : 500,
      overlayColor  : '#445779',
      type          : 'iframe'
    });	
}

$('document').ready( function(){

	$(".login a").click(function(e) {
	    e.preventDefault();
	    var link = $(this).attr("href");
	    if(link != 'recuperacao')
	    	window.parent.location = link;
	    else
	    	abreRecuperarSenha();
	});

	$('.login form').on('submit', function(e){
		e.preventDefault();
		var destino = $(this).attr('action');
		var usuario = $(this).find('input[type="text"]').val();
		var senha = $(this).find('input[type="password"]').val();
		$.post(BASE+destino, {
			login : usuario,
			senha   : senha
		}, function(retorno){
			if(retorno == 1){
				window.parent.location = BASE+'home';
			}else{

				var idioma = $('body').attr('data-lang');
				if (idioma == 'pt')
					var texto = "Login Incorreto. Verifique Nome de Usuário e Senha.";
				else
					var texto = "Login Incorreto. Verifique Nome de Usuário e Senha.";
				
				if($('.fail').length == 0){
					$('.login form').append($("<div class='fail'>"+texto+"</div>"));
					$('.fail').delay(2000).fadeOut('normal', function(){
						$(this).remove();
					});
				}
			}
		});
	})
});
</script>