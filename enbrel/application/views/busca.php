<div class="main-busca">

	<?php if ($videos || $noticias || $washington): ?>

		<h1><?=traduz('Resultado da busca para')?> : <span class="azul"><?=$termo?></span></h1>
		
		<?php if ($videos): ?>

			<div class="tipo-resultado">
				<div class="titulo"><?=traduz('videos')?></div>
				<div class="linha"></div>
			</div>

			<?php foreach ($videos as $key => $value): ?>
				
				<a href="videos/detalhes/<?=$value->slug?>" title="<?=$value->titulo?>">
					<img src="<?=$value->thumbnail?>" alt="<?=$value->titulo?>">
					<div class="titulo"><?=$value->titulo?></div>
					<div class="palestrante"><?=$value->palestrante?></div>
					<div class="data"><?=formataData($value->data, 'mysql2br')?></div>
					<div class="olho"><?=word_limiter($value->olho, 30)?></div>
				</a>

			<?php endforeach ?>

		<?php endif ?>

		<?php if ($noticias): ?>

			<div class="tipo-resultado">
				<div class="titulo"><?=traduz('noticias')?></div>
				<div class="linha"></div>
			</div>

			<?php foreach ($noticias as $key => $value): ?>
				
				<a href="noticias/detalhes/<?=$value->slug?>" title="<?=$value->titulo?>">
					<?php if ($value->imagem): ?>
						<img src="_imgs/noticias/<?=$value->imagem?>" alt="<?=$value->titulo?>">	
					<?php endif ?>
					<div class="titulo"><?=$value->titulo?></div>
					<div class="olho"><?=$value->olho?></div>
					<div class="data"><?=formataData($value->data, 'mysql2br')?></div>
				</a>

			<?php endforeach ?>

		<?php endif ?>

		<?php if ($washington): ?>

			<div class="tipo-resultado">
				<div class="titulo"><?=traduz('noticias')?></div>
				<div class="linha"></div>
			</div>

			<?php foreach ($washington as $key => $value): ?>
				
				<a href="washington/detalhes/<?=$value->slug?>" title="<?=$value->titulo?>">
					<?php if ($value->imagem): ?>
						<img src="_imgs/washington/<?=$value->imagem?>" alt="<?=$value->titulo?>">	
					<?php endif ?>
					<div class="titulo"><?=$value->titulo?></div>
					<div class="olho"><?=$value->olho?></div>
					<div class="data"><?=formataData($value->data, 'mysql2br')?></div>
				</a>

			<?php endforeach ?>

		<?php endif ?>

	<?php else: ?>

		<h1><?=traduz('Nenhum resultado encontrado para')?> : <span class="azul"><?=$termo?></span></h1>

	<?php endif ?>

</div>