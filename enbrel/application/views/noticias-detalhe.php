<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">

	<h1><?=traduz('NOTÍCIAS')?></h1>

	<article>

		<a href="javascript:history.back(-1)" class="voltar" title="">&laquo; <?=traduz('voltar')?></a>

		<div class="data"><div><?=formataData($detalhe[0]->data, 'mysql2br')?></div></div>
		<div class="texto-detalhe">
			<div class="autor"><?=$detalhe[0]->nome_autor?></div>
			<h1><?=$detalhe[0]->titulo?></h1>
			<div class="olho"><?=$detalhe[0]->olho?></div>
			<?php if ($detalhe[0]->imagem): ?>
				<img src="_imgs/noticias/<?=$detalhe[0]->imagem?>" class="capa">
			<?php endif ?>
			<?=relativizaUrl($detalhe[0]->texto)?>

			<?php if ($galeria): ?>
				<div class="galeria">
					<?$i = 0?>
					<?php foreach ($galeria as $key => $value): ?>
						<?$i++?>
						<a href="_imgs/noticias/<?=$value->imagem?>" rel="galeria" title="<?=traduz('Ampliar')?>" class="fancy <?if($i%3==0 && $i > 0)echo" ultima"?>"><img src="_imgs/noticias/thumbs/<?=$value->imagem?>"></a>		
					<?php endforeach ?>
				</div>
			<?php endif ?>
		</div>

		<div id="navegacao">
			<a href="javascript:history.back(-1)" class="voltar" title="">&laquo; <?=traduz('voltar')?></a>
			<?php if ($noticia_anterior): ?>
				<a href="noticias/detalhes/<?=$noticia_anterior?>" class="nav first" title="<?=traduz('notícia anterior')?>">&laquo; <?=traduz('notícia anterior')?></a>	
			<?php endif ?>
			<?php if ($proxima_noticia): ?>
				<a href="noticias/detalhes/<?=$proxima_noticia?>" class="nav" title="<?=traduz('próxima notícia')?>"><?=traduz('próxima notícia')?> &raquo;</a>	
			<?php endif ?>
		</div>

		<div id="filtro-noticias">
			<span><?=traduz('Buscar outras notícias')?>:</span>
			<select id="sel-categoria">
				<option value=""><?=traduz('FILTRAR POR CATEGORIA DE NOTÍCIAS')?></option>
				<?php if ($categorias): ?>
					<?php foreach ($categorias as $key => $value): ?>
						<option value="<?=$value->slug?>"><?=$value->titulo?></option>
					<?php endforeach ?>
				<?php else: ?>
					<option value=""><?=traduz('Nenhuma Categoria')?></option>
				<?php endif ?>
			</select>

			<form name="form-busca" method="post" action="noticias/busca">
				<input type="text" name="termo" placeholder="<?=traduz('BUSCAR EM NOTÍCIAS')?>">
				<input type="submit" value="&raquo;">
			</form>
		</div>

	</article>

	<aside>
		<div class="sidebar-titulo"><?=traduz('Últimos vídeos do evento')?>:</div>

		<div id="cycle-this">
			
			<ul>
				<?$contador=0;?>
				<?php foreach ($sidebar as $key => $value): ?>
					<li>
						<a href="videos/detalhes/<?=$value->slug?>" title="<?=$value->titulo?>">
							<img src="<?=$value->thumbnail?>">
							<div class="texto">
								<div class="titulo"><?=$value->titulo?></div>
								<div class="palestrante"><?=$value->palestrante?></div>
								<div class="data"><?=formataData($value->data, 'mysql2br')?></div>
							</div>
						</a>
					</li>
					<?$contador++?>
					<?if($contador == 12){
						echo "</ul><ul>";
						$contador = 0;
					}?>
				<?php endforeach ?>
			</ul>

		</div>

		<div id="botao-proxima">
			<a href="#" title="">&raquo;</a>
		</div>

		<a href="videos" title="Fazer nova busca" class="busca"><?=traduz('FAZER NOVA BUSCA DE VÍDEOS')?> &raquo;</a>		
	</aside>

	<script defer>
	$('document').ready( function(){

		$('#sel-categoria').change( function(){
			if($(this).val() != ''){
				window.location = BASE+'noticias/categoria/'+$(this).val();
			}
		});

	});
	</script>