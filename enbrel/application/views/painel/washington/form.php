<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar <?=$titulo?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir <?=$unidade?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/categorias')?>" class="lista">Listar Categorias</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/categorias_form')?>" class="add">Inserir Categorias</a>	
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Categoria da Notícia<br>
			<select name="id_categoria">
				<option>Selecione uma Categoria</option>
				<?php foreach ($categorias as $key => $value): ?>
					<option value="<?=$value->id?>" <?if($value->id==$registro->id_categoria)echo" selected"?>><?=$value->titulo?></option>
				<?php endforeach ?>
			</select>
		</label>

		<label>Idioma da Notícia<br>
			<select name="id_idioma" required>
				<option value="">Selecione um Idioma</option>
				<?php foreach ($idiomas as $key => $value): ?>
					<option value="<?=$value->id?>" <?if($registro->id_idioma==$value->id)echo" selected"?>><?=$value->titulo?></option>
				<?php endforeach ?>
			</select>
		</label>		

		<label>Título<br>
		<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label>

		<label>Data<br>
		<input type="text" name="data" id="datepicker" required value="<?=formataData($registro->data, 'mysql2br')?>"></label>

		<label>Olho<br>
		<textarea class="olho" name="olho"><?=$registro->olho?></textarea></label>

		<label>Texto<br>
		<textarea class="texto" name="texto"><?=$registro->texto?></textarea></label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Categoria da Notícia<br>
			<select name="id_categoria">
				<option>Selecione uma Categoria</option>
				<?php foreach ($categorias as $key => $value): ?>
					<option value="<?=$value->id?>"><?=$value->titulo?></option>
				<?php endforeach ?>
			</select>
		</label>

		<label>Idioma da Notícia<br>
			<select value="" name="id_idioma" required>
				<option>Selecione um Idioma</option>
				<?php foreach ($idiomas as $key => $value): ?>
					<option value="<?=$value->id?>"><?=$value->titulo?></option>
				<?php endforeach ?>
			</select>
		</label>		

		<label>Título<br>
		<input type="text" name="titulo" required autofocus></label>

		<label>Data<br>
		<input type="text" name="data" id="datepicker" required></label>

		<label>Olho<br>
		<textarea class="olho" name="olho"></textarea></label>

		<label>Texto<br>
		<textarea class="texto" name="texto"></textarea></label>

		<input type="hidden" name="id_autor" value="<?=$this->session->userdata('id')?>">

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>