<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar <?=$titulo?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir <?=$unidade?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/categorias')?>" class="lista active">Listar Categorias</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/categorias_form')?>" class="add">Inserir Categorias</a>
</div>

<div id="mensagem" class="alerta"></div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Título</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td><?=$value->titulo?></td>
				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/categorias_form/'.$value->id)?>">Editar</a></td>
				<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/categorias_excluir/'.$value->id)?>">Excluir</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhuma Categoria de Notícias Cadastrada</h2>

<?endif;?>