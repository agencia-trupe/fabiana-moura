<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar <?=$titulo?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir <?=$unidade?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/categorias')?>" class="lista">Listar Categorias</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/categorias_form')?>" class="add active">Inserir Categorias</a>	
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/categorias_alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título da Categoria em Português<br>
		<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label>

		<label>Título da Categoria em Espanhol<br>
		<input type="text" name="titulo_es" required value="<?=$registro->titulo_es?>"></label>			

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/categorias_inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título da Categoria em Português<br>
		<input type="text" name="titulo" required autofocus></label>

		<label>Título da Categoria em Espanhol<br>
		<input type="text" name="titulo_es" required></label>		

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>