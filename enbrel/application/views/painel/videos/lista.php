<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar <?=$titulo?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir <?=$unidade?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/temas')?>" class="lista">Listar Temas</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/temas_form')?>" class="add">Inserir Tema</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/aovivo')?>" class="aovivo">Divulgar Transmissão ao Vivo</a>
</div>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista<?if($mostrar=='todos')echo" active"?>">Todos Os Vídeos</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/index/preaprovados')?>" class="lista<?if($mostrar=='preaprovados')echo" active"?>">Vídeos Pré Aprovados</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/index/aprovados')?>" class="lista<?if($mostrar=='aprovados')echo" active"?>">Vídeos Aprovados</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/index/aguardando')?>" class="lista<?if($mostrar=='aguardando')echo" active"?>">Vídeos Aguardando Aprovação</a>
</div>

<div id="mensagem" class="alerta"></div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Aprovado</th>
				<th>Título</th>
				<th>Olho</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td>
					<?php if ($value->pre_aprovado==1 || ($value->texto_aprovado && $value->video_aprovado==1)): ?>
						<img src="_imgs/layout/aprovado.png">
					<?php else: ?>
						<img src="_imgs/layout/naoaprovado.png">
					<?php endif ?>
				</td>
				<td><?=$value->titulo?></td>
				<td><?=word_limiter($value->olho, 10)?></td>

				<?php if ($this->session->userdata('tipo_usuario')==1 || $this->session->userdata('tipo_usuario')==4): ?>

					<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
					<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>

				<?php elseif($this->session->userdata('tipo_usuario')==2 && ($value->pre_aprovado==1 || ($value->video_aprovado==0 && $value->texto_aprovado==0))): ?>

					<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
					<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>

				<?php else: ?>
					<td></td>
					<td></td>
				<?php endif ?>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhum Vídeo</h2>

<?endif;?>