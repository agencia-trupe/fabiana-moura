<h1><?=$titulo?></h1>

<div id="mensagem" class="alerta"></div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Aprovado</th>
				<th>Título</th>
				<th>Olho</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td>
					<?php if ($value->pre_aprovado==1 || ($value->texto_aprovado && $value->video_aprovado==1)): ?>
						<img src="_imgs/layout/aprovado.png">
					<?php else: ?>
						<img src="_imgs/layout/naoaprovado.png">
					<?php endif ?>
				</td>

				<td><?=$value->titulo?></td>
				<td><?=word_limiter($value->olho, 10)?></td>

				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/aprovarVideo/'.$value->id)?>">Aprovar</a></td>

			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhum Vídeo</h2>

<?endif;?>