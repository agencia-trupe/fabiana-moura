<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar <?=$titulo?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active"><?=(isset($registro) && $registro)?'Alterar':'Inserir'?> <?=$unidade?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/temas')?>" class="lista">Listar Temas</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/temas_form')?>" class="add">Inserir Tema</a>	
	<a href="<?=base_url('painel/'.$this->router->class.'/aovivo')?>" class="aovivo">Divulgar Transmissão ao Vivo</a>
</div>

<?if (isset($registro) && $registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Tema do Vídeo<br>
			<select name="id_tema" required>
				<option value="">Selecione uma Categoria</option>
				<?php foreach ($categorias as $key => $value): ?>
					<option value="<?=$value->id?>" <?if($registro->id_tema==$value->id)echo" selected"?>><?=$value->titulo?></option>
				<?php endforeach ?>
			</select>
		</label>

		<label>Idioma do Vídeo<br>
			<select name="id_idioma" required>
				<option value="">Selecione um Idioma</option>
				<?php foreach ($idiomas as $key => $value): ?>
					<option value="<?=$value->id?>" <?if($registro->id_idioma==$value->id)echo" selected"?>><?=$value->titulo?></option>
				<?php endforeach ?>
			</select>
		</label>		

		<label>Título<br>
		<input type="text" name="titulo" value="<?=$registro->titulo?>" required></label>

		<label>Data<br>
		<input type="text" name="data" id="datepicker" value="<?=formataData($registro->data, 'mysql2br')?>" required></label>

		<label>Link do Vídeo (VIMEO)<br>
			<div id="alerta" class="mensagem" style="display:block">
				O Link do vídeo para o vimeo deve estar nos formatos:<br>
				<i>url completa: http://vimeo.com/5284565</i><br>
				ou<br>
				<i>somente o id do vídeo: 5284565</i>
			</div>
			<input type="text" name="url" required value="<?=$registro->url?>">
		</label>

		<label>Olho<br>
		<textarea class="olho" name="olho"><?=$registro->olho?></textarea></label>

		<label>Texto<br>
		<textarea class="texto-semimg" name="texto"><?=$registro->texto?></textarea></label>

		<label>Palestrante<br>
		<input type="text" name="palestrante" required value="<?=$registro->palestrante?>"></label>

		Conteúdo Relacionado<br>
		<div id="rel-target">

			<a href="#" class="add">Adicionar Conteúdo</a>

			<?php if ($relacionamentos): ?>

				<?php foreach ($relacionamentos as $key => $value): ?>

					<div class="relacionamento">
						<label>Tipo de Conteúdo<br>
							<select class="tipo" name="rel_tipo[]">
								<option value="">--</option>
								<option value="noticia" <?if($value->tipo_relacionamento=='noticia')echo" selected"?>>Notícia do Site</option>
								<option value="externo" <?if($value->tipo_relacionamento=='externo')echo" selected"?>>Link Externo</option>
								<option value="washington" <?if($value->tipo_relacionamento=='washington')echo" selected"?>>Washington DC</option>
							</select>
						</label>

						<div class="lbl-externo" <?if($value->tipo_relacionamento=='externo')echo"style='display:block;'"?>>
							<label class="lbl-titulo">Título do Link<br>
								<input type="text" name="rel_titulo[]" value="<?=$value->titulo?>">
							</label>
							<label class="lbl-endereco">Endereço<br>
								<input type="text" name="rel_endereco[]" value="<?=$value->endereco?>">
							</label>
						</div>

						<label class="lbl-noticia" <?if($value->tipo_relacionamento=='noticia')echo"style='display:block;'"?>>Link da Notícia<br>
							<select name="rel_noticia[]">
								<option value="">--</option>
								<?php if ($noticias): ?>
									<?php foreach ($noticias as $key => $valuenot): ?>
										<option value="<?=$valuenot->id?>" <?if($valuenot->id==$value->endereco)echo" selected"?>><?=$valuenot->titulo?></option>
									<?php endforeach ?>							
								<?php else: ?>
									<option value="">Nenhuma Notícia Cadastrada</option>
								<?php endif ?>
							</select>
						</label>

						<label class="lbl-washington" <?if($value->tipo_relacionamento=='washington')echo"style='display:block;'"?>>Link do conteúdo Washington DC<br>
							<select name="rel_washington[]">
								<option value="">--</option>
								<?php if ($washington): ?>
									<?php foreach ($washington as $key => $valuewash): ?>
										<option value="<?=$valuewash->id?>" <?if($valuewash->id==$value->endereco)echo" selected"?>><?=$valuewash->titulo?></option>
									<?php endforeach ?>							
								<?php else: ?>
									<option value="">Nenhum Conteúdo Cadastrado</option>
								<?php endif ?>
							</select>
						</label>								

						<div class="remover_relacionamento_placeholder">
							<a href="#" class="remover_relacionamento">remover relacionamento</a>
						</div>
					</div>		

				<?php endforeach ?>

			<?php endif ?>

			<div class="relacionamento vazio">
				<label>Tipo de Conteúdo<br>
					<select class="tipo" name="rel_tipo[]">
						<option value="">--</option>
						<option value="noticia">Notícia do Site</option>
						<option value="washington">Link do conteúdo Washington DC</option>
						<option value="externo">Link Externo</option>
					</select>
				</label>

				<div class="lbl-externo">
					<label class="lbl-titulo">Título do Link<br>
						<input type="text" name="rel_titulo[]">
					</label>
					<label class="lbl-endereco">Endereço<br>
						<input type="text" name="rel_endereco[]">
					</label>
				</div>
				
				<label class="lbl-noticia">Link da Notícia<br>
					<select name="rel_noticia[]">
						<option value="">--</option>
						<?php if ($noticias): ?>
							<?php foreach ($noticias as $key => $value): ?>
								<option value="<?=$value->id?>"><?=$value->titulo?></option>
							<?php endforeach ?>							
						<?php else: ?>
							<option value="">Nenhuma Notícia Cadastrada</option>
						<?php endif ?>
					</select>
				</label>

				<label class="lbl-washington">Link do conteúdo Washington DC<br>
					<select name="rel_washington[]">
						<option value="">--</option>
						<?php if ($washington): ?>
							<?php foreach ($washington as $key => $value): ?>
								<option value="<?=$value->id?>"><?=$value->titulo?></option>
							<?php endforeach ?>							
						<?php else: ?>
							<option value="">Nenhum Conteúdo Cadastrado</option>
						<?php endif ?>
					</select>
				</label>		

				<div class="remover_relacionamento_placeholder notyet">
					<a href="#" class="remover_relacionamento">remover relacionamento</a>
				</div>				
			</div>

		</div>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<div id="aprovacao-video">
			<label style="color:#005747;"><input type="radio" name="pre_aprovado" value="1">O Vídeo não precisa de aprovação</label>
			<label style="color:#6C0A09;"><input type="radio" name="pre_aprovado" value="0">O Vídeo precisa ser aprovado antes de ser publicado</label>
		</div>

		<div id="hidden-part">

			<label>Tema do Vídeo<br>
				<select name="id_tema" required>
					<option value="">Selecione uma Categoria</option>
					<?php foreach ($categorias as $key => $value): ?>
						<option value="<?=$value->id?>"><?=$value->titulo?></option>
					<?php endforeach ?>
				</select>
			</label>

			<label>Idioma do Vídeo<br>
				<select value="" name="id_idioma" required>
					<option>Selecione um Idioma</option>
					<?php foreach ($idiomas as $key => $value): ?>
						<option value="<?=$value->id?>"><?=$value->titulo?></option>
					<?php endforeach ?>
				</select>
			</label>		

			<label>Título<br>
			<input type="text" name="titulo" required></label>

			<label>Data<br>
			<input type="text" name="data" id="datepicker" required></label>

			<label>Link do Vídeo (VIMEO)<br>
				<div id="alerta" class="mensagem" style="display:block">
					O Link do vídeo para o vimeo deve estar nos formatos:<br>
					<i>url completa: http://vimeo.com/5284565</i><br>
					ou<br>
					<i>somente o id do vídeo: 5284565</i>
				</div>
				<input type="text" name="url" required>
			</label>

			<label>Olho<br>
			<textarea class="olho" name="olho"></textarea></label>

			<label>Texto<br>
			<textarea class="texto-semimg" name="texto"></textarea></label>

			<label>Palestrante<br>
			<input type="text" name="palestrante" required></label>

			<input type="hidden" name="id_autor" value="<?=$this->session->userdata('id')?>">

			Conteúdo Relacionado<br>
			<div id="rel-target">

				<a href="#" class="add">Adicionar Conteúdo</a>

				<div class="relacionamento vazio">
					<label>Tipo de Conteúdo<br>
						<select class="tipo" name="rel_tipo[]">
							<option value="">--</option>
							<option value="noticia">Notícia do Site</option>
							<option value="washington">Washington DC</option>
							<option value="externo">Link Externo</option>
						</select>
					</label>

					<div class="lbl-externo">
						<label class="lbl-titulo">Título do Link<br>
							<input type="text" name="rel_titulo[]">
						</label>
						<label class="lbl-endereco">Endereço<br>
							<input type="text" name="rel_endereco[]">
						</label>
					</div>

					<label class="lbl-noticia">Link da Notícia<br>
						<select name="rel_noticia[]">
							<option value="">--</option>
							<?php if ($noticias): ?>
								<?php foreach ($noticias as $key => $value): ?>
									<option value="<?=$value->id?>"><?=$value->titulo?></option>
								<?php endforeach ?>							
							<?php else: ?>
								<option value="">Nenhuma Notícia Cadastrada</option>
							<?php endif ?>
						</select>
					</label>

					<label class="lbl-washington">Link do conteúdo Washington DC<br>
						<select name="rel_washington[]">
							<option value="">--</option>
							<?php if ($washington): ?>
								<?php foreach ($washington as $key => $value): ?>
									<option value="<?=$value->id?>"><?=$value->titulo?></option>
								<?php endforeach ?>							
							<?php else: ?>
								<option value="">Nenhum Conteúdo Cadastrado</option>
							<?php endif ?>
						</select>
					</label>					

					<div class="remover_relacionamento_placeholder notyet">
						<a href="#" class="remover_relacionamento">remover relacionamento</a>
					</div>
				</div>

			</div>

			<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">

		</div>

	</form>

<?endif ?>

<script defer>
	$('document').ready( function(){
		
		if($('#aprovacao-video').length){	
			$('form').submit( function(){

				$("input, textarea").each( function(){
					if($(this).attr('required') !== undefined){
						if($(this).val() == '' || $(this).val() == $(this).attr('placeholder')){
							alert("Item Obrigatório!");
							return false;
						}
					}
				});

				if($('#aprovacao-video label input[type="radio"]:checked').val() == 1){
					if(!confirm("O Vídeo postado entrará no ar sem aprovação. Confirmar?")){
						return false;
					}
				}
			});
		}

		$('.remover_relacionamento').live('click', function(e){
			e.preventDefault();
			$(this).parent().parent().fadeOut( function(){
				$(this).remove();
			})
		});

		$('.relacionamento select.tipo').live('change', function(){
			if($(this).val() == 'noticia'){
				$(this).parent().parent().find(".lbl-noticia").fadeIn();
				$(this).parent().parent().find(".lbl-externo").hide();
				$(this).parent().parent().find(".lbl-washington").hide();
				$(this).parent().parent().find(".remover_relacionamento_placeholder").fadeIn();
			}else if($(this).val() == 'externo'){
				$(this).parent().parent().find(".lbl-noticia").hide();
				$(this).parent().parent().find(".lbl-washington").hide();
				$(this).parent().parent().find(".lbl-externo").fadeIn();
				$(this).parent().parent().find(".remover_relacionamento_placeholder").fadeIn();
			}else if($(this).val() == 'washington'){
				$(this).parent().parent().find(".lbl-noticia").hide();
				$(this).parent().parent().find(".lbl-externo").hide();
				$(this).parent().parent().find(".lbl-washington").fadeIn();
				$(this).parent().parent().find(".remover_relacionamento_placeholder").fadeIn();
			}
		});

		var relac = $('.relacionamento.vazio').clone();

		$('#rel-target .add').live('click', function(e){
			e.preventDefault();
			clone = $(relac).clone();
			$('#rel-target').append($(clone).removeClass('vazio'));
		});

	});
</script>

