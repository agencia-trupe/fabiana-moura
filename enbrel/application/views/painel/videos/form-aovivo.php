<h1>Transmissões Ao Vivo</h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Vídeos</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Vídeo</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/temas')?>" class="lista">Listar Temas</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/temas_form')?>" class="add">Inserir Tema</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/aovivo')?>" class="aovivo active">Divulgar Transmissão ao Vivo</a>
</div>
<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alteraaovivo/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Transmissão : <br>
		<select name="transmitindo">
			<option value="0" <?if($registro->transmitindo == 0)echo " selected"?>>Não Transmitindo</option>
			<option value="1" <?if($registro->transmitindo == 1)echo " selected"?>>Transmitindo</option>
		</select></label>

		<label>Texto da Chamada em Português: <br>
			<textarea name="texto_pt" class="olho"><?=$registro->mensagem_pt?></textarea>
		</label>

		<label>Texto da Chamada em Espanhol: <br>
			<textarea name="texto_es" class="olho"><?=$registro->mensagem_es?></textarea>
		</label>

		<label>
			URL da transmissão:<br>
			<input type="text" name="url" value="<?=$registro->url?>">
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alteraaovivo')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Transmissão : <br>
		<select name="transmitindo">
			<option value="0">Não Transmitindo</option>
			<option value="1">Transmitindo</option>
		</select></label>

		<label>Texto da Chamada : <br>
			<textarea name="texto" class="olho"></textarea>
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>