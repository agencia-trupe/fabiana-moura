<h1>Aprovar Vídeo</h1>	

<label>Tema do Vídeo<br>
	<input type="text" disabled value="<?=$registro->titulo_tema->titulo?>">
</label>

<label>Idioma do Vídeo<br>
	<input type="text" disabled value="<?=$registro->titulo_idioma?>">
</label>		

<label>Título<br>
<input type="text" disabled value="<?=$registro->titulo?>"></label>

<label>Data<br>
<input type="text" disabled value="<?=formataData($registro->data, 'mysql2br')?>"></label>

<label>Link do Vídeo (VIMEO)<br>
	<input type="text" disabled value="<?=$registro->url?>"><br><br>
	<?=embed($registro->url)?>
</label>

<label>Olho<br>
<div style='margin:5px; padding:10px; background-color:#ccc; font-size:10px;'><?=$registro->olho?></div></label>

<label>Texto<br>
<div style='margin:5px; padding:10px; background-color:#ccc; font-size:10px;'><?=$registro->texto?></div></label>

<label>Palestrante<br>
<input type="text" disabled value="<?=$registro->palestrante?>"></label>

	<?php if ($relacionamentos): ?>

	Conteúdo Relacionado<br>
	<div id="rel-target">

		<?php foreach ($relacionamentos as $key => $value): ?>

			<div class="relacionamento">
				<label>Tipo de Conteúdo<br>
					<?
					if($value->tipo_relacionamento=='noticia')
						$tipo = "Notícia do Site";
					elseif($value->tipo_relacionamento=='externo')
						$tipo = "Link Externo";
					elseif($value->tipo_relacionamento=='washington')
						$tipo = "Washington DC";
					?>
					<input type="text" disabled value="<?=$tipo?>">
				</label>

				<label class="lbl-endereco" style='display:block;'>Endereço<br>
					<?php if ($value->tipo_relacionamento=='noticia' || $value->tipo_relacionamento=='washington'): ?>
						<?php
						$tab = ($value->tipo_relacionamento == 'noticia') ? 'noticias' : 'washington';
						$query = $this->db->get_where($tab, array('id' => $value->endereco))->result();
						$slug = $query[0]->slug;
						$titulo = $query[0]->titulo;
						?>
						<a href="noticias/<?=$slug?>" title="<?=$titulo?>" target="_blank"><?=$titulo?></a>
					<?php else: ?>
						<a href="<?=(strpos($value->endereco, 'http://') === FALSE) ? 'http://'.$value->endereco : $value->endereco?>" target="_blank"><?=$value->endereco?></a>
					<?php endif ?>
				</label>
				
			</div>		

		<?php endforeach ?>

	<?php endif ?>
</div>

<div style="margin:30px auto; text-align:center;">

	<?php if ($registro->pre_aprovado==1 || ($registro->video_aprovado==1 && $registro->texto_aprovado==1)): ?>

		<img src="_imgs/layout/aprovado.png" style="margin:0 3px;"><span style="font-weight:bold; color:#005747">O Vídeo já está Publicado!</span><br><br>
		<a href="painel/videos/form/<?=$registro->id?>" class="edit">Editar este Vídeo</a>

	<?php elseif($registro->video_aprovado==1): ?>

		<img src="_imgs/layout/aprovado.png" style="margin:0 3px;"><span style="font-weight:bold; color:#005747">O Vídeo já está Aprovado!</span><br><br>
		<a href="painel/aprovar/confirmaAprovacao/<?=$registro->id?>" class="aprovar">Aprovar Publicação deste Vídeo (Vídeo e Texto)</a>
		<a href="painel/aprovar/confirmaAprovacao/<?=$registro->id?>/texto" class="aprovar">Aprovar Somente o Texto</a>
		<a href="painel/videos/form/<?=$registro->id?>" class="edit">Editar este Vídeo</a>

	<?php elseif($registro->texto_aprovado==1): ?>

		<img src="_imgs/layout/aprovado.png" style="margin:0 3px;"><span style="font-weight:bold; color:#005747">O Texto já está Aprovado!</span><br><br>
		<a href="painel/aprovar/confirmaAprovacao/<?=$registro->id?>" class="aprovar">Aprovar Publicação deste Vídeo (Vídeo e Texto)</a>
		<a href="painel/aprovar/confirmaAprovacao/<?=$registro->id?>/video" class="aprovar">Aprovar Somente o Vídeo</a>
		<a href="painel/videos/form/<?=$registro->id?>" class="edit">Editar este Vídeo</a>

	<?php else: ?>

		<a href="painel/aprovar/confirmaAprovacao/<?=$registro->id?>" class="aprovar">Aprovar Publicação deste Vídeo (Vídeo e Texto)</a>
		<a href="painel/aprovar/confirmaAprovacao/<?=$registro->id?>/video" class="aprovar">Aprovar Somente o Vídeo</a>
		<a href="painel/aprovar/confirmaAprovacao/<?=$registro->id?>/texto" class="aprovar">Aprovar Somente o Texto</a>
		<a href="painel/videos/form/<?=$registro->id?>" class="edit">Editar este Vídeo</a>

	<?php endif ?>
</div>

<div id="alerta" class="mensagem" style="display:block; min-height:30px; height:auto;">
	O Vídeo será Publicado após a Aprovação do Vídeo e do Texto.<br>
	Após a Publicação, somente os perfis de Avaliador poderão alterá-lo.
</div>

<input type="button" class="voltar" value="VOLTAR">