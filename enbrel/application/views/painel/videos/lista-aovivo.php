<h1>Transmissões Ao Vivo</h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Vídeos</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Vídeo</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/temas')?>" class="lista">Listar Temas</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/temas_form')?>" class="add">Inserir Tema</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/aovivo')?>" class="aovivo active">Divulgar Transmissão ao Vivo</a>
</div>

<div id="mensagem" class="alerta"></div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Transmitindo</th>
				<th>Mensagem</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td>
					<?php
					switch($value->transmitindo){
						case 1:
							echo "<span class='transmitindo-verde'>TRANSMITINDO</span>";
							break;
						case 0:
							echo "<span class='transmitindo-vermelho'>NÃO TRANSMITINDO</span>";
							break;
					}
					?>
				</td>
				<td>
					<?=word_limiter($value->mensagem_pt, 10)?><br>
					<?=word_limiter($value->mensagem_es, 10)?>
				</td>
				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/formaovivo/'.$value->id)?>">Editar</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?endif;?>