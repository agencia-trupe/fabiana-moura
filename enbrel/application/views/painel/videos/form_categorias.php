<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar <?=$titulo?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir <?=$unidade?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/temas')?>" class="lista">Listar Temas</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/temas_form')?>" class="add active">Inserir Tema</a>	
	<a href="<?=base_url('painel/'.$this->router->class.'/aovivo')?>" class="aovivo">Divulgar Transmissão ao Vivo</a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/temas_alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título do Tema em Português<br>
		<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label>

		<label>Título do Tema em Espanhol<br>
		<input type="text" name="titulo_es" value="<?=$registro->titulo_es?>"></label>

		<label>Título do Tema em Inglês<br>
		<input type="text" name="titulo_en" value="<?=$registro->titulo_en?>"></label>		

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/temas_inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título do Tema em Português<br>
		<input type="text" name="titulo" required autofocus></label>

		<label>Título do Tema em Espanhol<br>
		<input type="text" name="titulo_es" required></label>

		<label>Título do Tema em Inglês<br>
		<input type="text" name="titulo_en" required></label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>