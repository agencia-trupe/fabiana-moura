<h1>Relatórios</h1>

<div id="submenu">
	<a href="<?=base_url('painel/relatorios/aprovados')?>" class="lista active">Vídeos Aprovados</a>
	<?php if (isAdmin()): ?>
		<a href="<?=base_url('painel/relatorios/estatisticas')?>" class="lista">Estatísticas de Acesso</a>
		<a href="<?=base_url('painel/download')?>" class="lista">Extrair Cadastros</a>
	<?php endif ?>
</div>

<div id="alerta" class="mensagem" style="display:block; margin:10px 0;">Clique sobre o vídeo para obter informações de alteração</div>

<div style="padding:10px 5px">
	<a href="painel/relatorios/aprovados/<?if($ordenacao == 'data_postagem')echo"titulo/ASC"; else echo "data_postagem" ?>">Ordenar por <?if($ordenacao == 'data_postagem')echo" nome"; else echo "data de postagem" ?></a>
</div>

<?php if ($videos_aprovados): ?>

	<table class="relat">
		<thead>
			<tr>
				<th>Título do Vídeo</th>
				<th>Enviado Por</th>
				<th>Texto Aprovado Por</th>
				<th>Vídeo Aprovado Por</th>
			</tr>
		</thead>

		<tbody>

			<?php foreach ($videos_aprovados as $key => $value): ?>
				<tr class="canclick">
					<td><?=$value->titulo?><br></td>
					<td><?=$value->id_autor?> (<?=mysqlDT2view($value->data_postagem)?>)</td>
					<td><?= ($value->pre_aprovado == 1) ? 'vídeo pré-aprovado' : $value->texto_aprovado_por?> (<?=mysqlDT2view($value->texto_aprovado_em)?>)</td>
					<td><?= ($value->pre_aprovado == 1) ? 'vídeo pré-aprovado' : $value->video_aprovado_por?> (<?=mysqlDT2view($value->video_aprovado_em)?>)</td>
				</tr>
				<tr class="hid-table">
					<td colspan="4">
						<?php if ($value->alteracoes): ?>
							<?php foreach ($value->alteracoes as $alts): ?>
								<?=mysqlDT2view($alts->data_alteracao)?>&nbsp;&bull;&nbsp;<?=$alts->id_usuario?>&nbsp;&bull;&nbsp;<?=$alts->acao?>&nbsp;&bull;&nbsp;<?=$alts->titulo_registro?><br>
							<?php endforeach ?>							
						<?php else: ?>
							Nenhuma alteração no log
						<?php endif ?>
					</td>
				</tr>
			<?php endforeach ?>	

		</tbody>

	</table>

<?php else: ?>

	<h2>Nenhum vídeo aprovado</h2>

<?php endif ?>
