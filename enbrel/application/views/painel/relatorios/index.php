<h1>Relatórios</h1>

<div id="submenu">
	<a href="<?=base_url('painel/relatorios/aprovados')?>" class="lista">Vídeos Aprovados</a>
	<?php if (isAdmin()): ?>
		<a href="<?=base_url('painel/relatorios/estatisticas')?>" class="lista">Estatísticas de Acesso</a>
		<a href="<?=base_url('painel/download')?>" class="lista">Extrair Cadastros</a>
	<?php endif ?>
</div>