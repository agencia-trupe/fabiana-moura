<h1>Estatísticas</h1>

<div id="submenu">
	<a href="<?=base_url('painel/relatorios/aprovados')?>" class="lista">Vídeos Aprovados</a>
	<?php if (isAdmin()): ?>
		<a href="<?=base_url('painel/relatorios/estatisticas')?>" class="lista active">Estatísticas de Acesso</a>
		<a href="<?=base_url('painel/download')?>" class="lista">Extrair Cadastros</a>
	<?php endif ?>
</div>

<h1 class='reltit'>Número de Usuários Cadastrados : <?=$total_usuarios?></h1>

<h1 class='reltit'>Número de Vídeos : <?=$total_videos?> (<?=$total_videos_aprovados?> aprovados)</h1>

<h1 class='reltit'>Número de Vídeos por categorias : </h1>
<table class='little-table'>
	<thead>
		<th>Categoria</th>
		<th>Número de Vídeos</th>
	</thead>
	<tbody>
		<?php foreach ($categorias as $key => $value): ?>
			<tr>
				<td><?=$value->titulo?></td>
				<td><?=$value->numero_videos?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>

<h1 class='reltit'>Total de Notícias Cadastradas : <?=$noticias?></h1>

<h1 class='reltit'>Número de Notícias por categorias : </h1>
<table class='little-table'>
	<thead>
		<th>Categoria</th>
		<th>Número de Notícias</th>
	</thead>
	<tbody>
		<?php foreach ($noticias_categorias as $key => $value): ?>
			<tr>
				<td><?=$value->titulo?></td>
				<td><?=$value->num_noticias?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>

<h1 class='reltit'>Visualizações de Vídeos por categorias : </h1>
<table class='little-table'>
	<thead>
		<th>Categoria</th>
		<th>Número de Visualizações</th>
	</thead>
	<tbody>
		<?php foreach ($contador_categoria as $key => $value): ?>
			<tr>
				<td><?=$key?></td>
				<td><?=$value?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>

<h1 class='reltit'>Visualizações de Vídeos : </h1>
<table class='little-table'>
	<thead>
		<th>Título do Vídeo</th>
		<th>Número de Visualizações</th>
	</thead>
	<tbody>
		<?php foreach ($contador_videos as $key => $value): ?>
			<?php if ($key): ?>
				<tr>
					<td><?=$key?></td>
					<td><?=$value?></td>
				</tr>
			<?php endif ?>			
		<?php endforeach ?>
	</tbody>
</table>