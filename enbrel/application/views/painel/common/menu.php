
<!-- Shell -->
<div id="shell">
	
	<!-- Header -->
	<div id="header">
		<h1><a href="<?=base_url('painel/home')?>">Painel - Reumato Online</a></h1>
		<div class="right">
			<p>Bem vindo, <strong><?=$this->session->userdata('username')?></strong></p>
			<p class="small-nav"><?if($this->session->userdata('tipo_usuario')==1):?><a href="<?=base_url('painel/usuarios_painel')?>" <?if($this->router->class=='usuarios_painel')echo " class='active'"?>>Usuários do Painel Administrativo</a> / <?php endif;?><a href="painel/relatorios" title="Relatórios">Relatórios</a> / <a href="<?=base_url('painel/home/logout')?>">Log Out</a></p>
		</div>
	</div>
	<!-- End Header -->
	
	<?php
	switch($this->session->userdata('tipo_usuario')){
		// ADMINISTRADOR
		case 1:?>
				<div id="navigation">
					<ul>
					    <li><a href="<?=base_url('painel/home')?>" <?if($this->router->class=='home')echo " class='active'"?>><span>Início</span></a></li>
					    <li><a href="<?=base_url('painel/slides')?>" <?if($this->router->class=='slides')echo " class='active'"?>><span>Slides</span></a></li>
					    <li><a href="<?=base_url('painel/banners')?>" <?if($this->router->class=='banners')echo " class='active'"?>><span>Banners</span></a></li>
					    <li><a href="<?=base_url('painel/noticias')?>" <?if($this->router->class=='noticias')echo " class='active'"?>><span>Notícias</span></a></li>
					    <li><a href="<?=base_url('painel/washington')?>" <?if($this->router->class=='washington')echo " class='active'"?>><span>Washington DC</span></a></li>
					    <li><a href="<?=base_url('painel/aprovar')?>" <?if($this->router->class=='aprovar')echo " class='active'"?>><span>Aprovar Vídeos</span></a></li>
					    <li><a href="<?=base_url('painel/videos')?>" <?if($this->router->class=='videos')echo " class='active'"?>><span>Vídeos</span></a></li>
					</ul>
				</div>
			<?php break;
		// VIDEOMAKER
		case 2:?>
				<div id="navigation">
					<ul>
					    <li><a href="<?=base_url('painel/home')?>" <?if($this->router->class=='home')echo " class='active'"?>><span>Início</span></a></li>
					    <li><a href="<?=base_url('painel/videos')?>" <?if($this->router->class=='videos')echo " class='active'"?>><span>Vídeos</span></a></li>
					</ul>
				</div>
			<?php break;
		// REDATOR
		case 3:?>
				<div id="navigation">
					<ul>
					    <li><a href="<?=base_url('painel/home')?>" <?if($this->router->class=='home')echo " class='active'"?>><span>Início</span></a></li>
					    <li><a href="<?=base_url('painel/noticias')?>" <?if($this->router->class=='noticias')echo " class='active'"?>><span>Notícias</span></a></li>
					    <li><a href="<?=base_url('painel/washington')?>" <?if($this->router->class=='washington')echo " class='active'"?>><span>Washington DC</span></a></li>
					</ul>
				</div>
			<?php break;
		// AVALIADOR
		case 4:?>
				<div id="navigation">
					<ul>
					    <li><a href="<?=base_url('painel/home')?>" <?if($this->router->class=='home')echo " class='active'"?>><span>Início</span></a></li>
					    <li><a href="<?=base_url('painel/aprovar')?>" <?if($this->router->class=='aprovar')echo " class='active'"?>><span>Aprovar Vídeos</span></a></li>
					    <li><a href="<?=base_url('painel/videos')?>" <?if($this->router->class=='videos')echo " class='active'"?>><span>Vídeos</span></a></li>
					</ul>
				</div>
			<?php break;
	}?>
	
	
	<!-- Content -->
	<div id="content">

	<?if(isset($mostrarerro_mensagem) && $mostrarerro_mensagem):?>
		<div class="mensagem" id="erro"><?=$mostrarerro_mensagem?></div>
	<?elseif(isset($mostrarsucesso_mensagem) && $mostrarsucesso_mensagem):?>
		<div class="mensagem" id="sucesso"><?=$mostrarsucesso_mensagem?></div>
	<?endif;?>