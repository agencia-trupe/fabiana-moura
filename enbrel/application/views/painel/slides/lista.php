<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar Slides</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Slides</a>
</div>


<div id="alerta" class="mensagem"></div>


<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Imagem</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td><img src="_imgs/slides/<?=$value->imagem?>" style="width:150px;"></td>
				<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhum Slide Cadastrado</h2>

<?endif;?>