<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Banners</a>
	<?php if ($registro): ?>
		<a href="<?=base_url('painel/'.$this->router->class.'/form/'.$registro->id)?>" class="add active">Alterar Banner</a>
	<?php elseif($this->db->get('banners')->num_rows() == 0): ?>
		<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Alterar Banner</a>	
	<?php endif ?>
	
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título do Banner<br>
		<input type="text" name="titulo" autofocus value="<?=$registro->titulo?>"></label>

		<label>Link do Banner<br>
		<input type="text" name="destino" required value="<?=$registro->destino?>"></label>

		<label>Imagem do Banner <i>(860px x 130px)</i><br>
			<img src="_imgs/banners/<?=$registro->imagem?>"><br>
		<input type="file" name="userfile" required></label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título do Banner<br>
		<input type="text" name="titulo" autofocus></label>

		<label>Link do Banner<br>
		<input type="text" name="destino" required></label>

		<label>Imagem do Banner <i>(860px x 130px)</i><br>
		<input type="file" name="userfile" required></label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>