<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar Banners</a>
	<?if(!$registros || sizeof($registros) == 0):?>
		<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Banner</a>
	<?endif;?>
</div>

<?if($registros && sizeof($registros) > 0):?>
<div id="alerta" class="mensagem" style="display:block;">
	O número máximo de banners cadastrados ao mesmo tempo é 1
</div>
<?php endif;?>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Título</th>
				<th>Imagem</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td><?=$value->titulo?></td>
				<td><img src="_imgs/banners/<?=$value->imagem?>" style="width:400px;"></td>
				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
				<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhum Banner Cadastrado</h2>

<?endif;?>