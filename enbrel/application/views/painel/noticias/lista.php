<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar <?=$titulo?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir <?=$unidade?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/categorias')?>" class="lista">Listar Categorias</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/categorias_form')?>" class="add">Inserir Categorias</a>
</div>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista <?if(!$idioma)echo' active'?>">Todas as Notícias</a> <a href="<?=base_url('painel/'.$this->router->class.'/index/pt')?>" class="lista <?if($idioma=='pt')echo' active'?>">Somente Notícias em Português</a> <a href="<?=base_url('painel/'.$this->router->class.'/index/es')?>" class="lista <?if($idioma=='es')echo' active'?>">Somente Notícias em Espanhol</a>
</div>

<div id="mensagem" class="alerta"></div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Título</th>
				<th>Olho</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td><?=$value->titulo?></td>
				<td><?=word_limiter($value->olho, 10)?></td>
				<td><a class="imagens" href="<?=base_url('painel/'.$this->router->class.'/imagens/'.$value->id)?>">Imagens</a></td>
				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
				<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhuma Notícia Cadastrada</h2>

<?endif;?>