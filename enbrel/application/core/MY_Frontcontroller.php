<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class MY_Frontcontroller extends CI_controller {

    var $headervar;
    var $footervar;
    var $menuvar;
    
    function __construct($css = '', $js = '') {
        parent::__construct();
        
        $this->headervar['load_css'] = $css;
        $this->headervar['load_js'] = $js;
        //$this->output->enable_profiler(TRUE);
        
        if(!preg_match('/^FacebookExternalHit\/.*?/i',$_SERVER['HTTP_USER_AGENT'])){
            if(!$this->session->userdata('logado')){
                redirect('publichome');
            }else{
                logAtividade();
            }
        }
    }
    
    function _output($output){

        $this->headervar['slides'] = $this->db->get('slides')->result();
        $this->footervar['banner'] = $this->db->get('banners', 1, 0)->result();

        echo $this->load->view('common/header', $this->headervar, TRUE).
             $this->load->view('common/menu', $this->menuvar, TRUE).
             $output.
             $this->load->view('common/footer', $this->footervar, TRUE);
    }

    function criaQuery($excluir_atual = FALSE){

        $query = "SELECT * FROM videos WHERE (pre_aprovado = 1 OR (texto_aprovado = 1 AND video_aprovado = 1))";

        if($this->session->userdata('idioma_conteudo') != 4)
            $query .= " AND (id_idioma = '".$this->session->userdata('idioma_conteudo')."' OR id_idioma = 4)";
       

        if($this->session->userdata('busca-palestrante') || isset($_POST['palest'])){

            if($this->session->userdata('busca-palestrante') && $this->session->userdata('busca-palestrante') != '')
                $termo = $this->session->userdata('busca-palestrante');

            if(isset($_POST['palest']) && $this->input->post('palest') != ''){
                $termo = $this->input->post('palest');
                $this->session->set_userdata('busca-palestrante', $termo);
            }elseif(isset($_POST['palest']) && $this->input->post('palest') == ''){
                $termo = FALSE;
                $this->session->unset_userdata('busca-palestrante');
            }
            
            if($termo){
                $query .= " AND palestrante LIKE '%".$termo."%'";
            }   
        }

        if($this->session->userdata('busca-tema') || isset($_POST['id_tema'])){

            if($this->session->userdata('busca-tema') && $this->session->userdata('busca-tema') != ''){
                $termo = $this->session->userdata('busca-tema');
            }

            if(isset($_POST['id_tema']) && $this->input->post('id_tema') != '0'){
                $termo = $this->input->post('id_tema');
                $this->session->set_userdata('busca-tema', $termo);
            }elseif(isset($_POST['id_tema']) && $this->input->post('id_tema') == '0'){
                $termo = FALSE;
                $this->session->unset_userdata('busca-tema');
            }
            
            if($termo){
                $query .= " AND id_tema = '".$termo."'";
            }else{
                $qry_temas = $this->db->get_where('usuarios_temas', array('id_usuario' => $this->session->userdata('id')))->result();
                if(sizeof($qry_temas)){
                    $query .= " AND";
                    $or = false;
                    foreach ($qry_temas as $key => $value) {
                        $query .= ($or) ? " OR id_tema = '".$value->id_videos_categorias."'" : " (id_tema = '".$value->id_videos_categorias."'";
                        $or = true;
                    }
                    $query .= ")";
                }               
            }

        }else{
            $qry_temas = $this->db->get_where('usuarios_temas', array('id_usuario' => $this->session->userdata('id')))->result();
            if(sizeof($qry_temas)){
                $query .= " AND";
                $or = false;
                foreach ($qry_temas as $key => $value) {
                    $query .= ($or) ? " OR id_tema = '".$value->id_videos_categorias."'" : " (id_tema = '".$value->id_videos_categorias."'";
                    $or = true;
                }
                $query .= ")";
            }
        }

        if(($this->session->userdata('busca-data-inicio') && $this->session->userdata('busca-data-termino')) || (isset($_POST['data_inicio']) && isset($_POST['data_termino']))){

            if($this->session->userdata('busca-data-inicio') && $this->session->userdata('busca-data-inicio') != ''){
                $inicio = $this->session->userdata('busca-data-inicio');
            }
            if($this->session->userdata('busca-data-termino') && $this->session->userdata('busca-data-termino') != ''){
                $termino = $this->session->userdata('busca-data-termino');
            }           

            if(isset($_POST['data_inicio']) && $this->input->post('data_inicio') != ''){
                $inicio = $this->input->post('data_inicio');
                $this->session->set_userdata('busca-data-inicio', $inicio);
            }elseif(isset($_POST['data_inicio']) && $this->input->post('data_inicio') == ''){
                $inicio = false;
                $this->session->unset_userdata('busca-data-inicio');
            }

            if(isset($_POST['data_termino']) && $this->input->post('data_termino') != ''){
                $termino = $this->input->post('data_termino');
                $this->session->set_userdata('busca-data-termino', $termino);
            }elseif(isset($_POST['data_termino']) && $this->input->post('data_termino') == ''){
                $termino = false;
                $this->session->unset_userdata('busca-data-termino');
            }           

            if($inicio && $termino){
                $query .= " AND data BETWEEN '".formataData($inicio, 'br2mysql')."' AND '".formataData($termino, 'br2mysql')."'";
            }
            
        }

        if($excluir_atual){
            $query .= " AND id != '".$excluir_atual."'";
        }

        if($this->session->userdata('busca-ordem') || $this->input->post('ordem')){
            
            if($this->session->userdata('busca-ordem') && $this->session->userdata('busca-ordem') != '')
                $termo = $this->session->userdata('busca-ordem');

            if(isset($_POST['ordem']) && $this->input->post('ordem') != ''){
                $termo = $this->input->post('ordem');
                $this->session->set_userdata('busca-ordem', $termo);
            }elseif(isset($_POST['ordem']) && $this->input->post('ordem') == ''){
                $termo = false;
                $this->session->unset_userdata('busca-ordem');
            }

            if($termo){
                if($termo == 'asc')
                    $query .= " ORDER BY data ASC";
                elseif($termo == 'desc')
                    $query .= " ORDER BY data DESC";
            }else{
                $query .= " ORDER BY data DESC";
            }
            
        }else{
            $query .= " ORDER BY data DESC";
        }

        return $query;
    }

}
?>