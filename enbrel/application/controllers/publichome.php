<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publichome extends CI_Controller {

    function __construct() {
        parent::__construct();
                
    }

    function index(){

        $ling = $this->session->userdata('language');

        if($ling != '1' && $ling != '3')
            $ling = '4';

    	$menu['slides'] = $this->db->get('slides')->result();
		$footer['banner'] = $this->db->get('banners', 1, 0)->result();


        // NOVA QUERY - VIDEO DESTAQUE
        if($ling != 4)
            $query = "SELECT * FROM videos WHERE (pre_aprovado = 1 OR (texto_aprovado = 1  AND video_aprovado = 1)) AND (id_idioma = $ling OR id_idioma = 4) ORDER BY data DESC LIMIT 0, 1";
        else
            $query = "SELECT * FROM videos WHERE (pre_aprovado = 1 OR (texto_aprovado = 1  AND video_aprovado = 1)) ORDER BY data DESC LIMIT 0, 1";
        $video_destaque = $this->db->query($query)->result();
        //==========================//

        $data['video_destaque'] = $video_destaque[0];

        // NOVA QUERY - VIDEOS DA LISTA
        if($ling != 4)
            $query = "SELECT * FROM videos WHERE (pre_aprovado = 1 OR (texto_aprovado = 1  AND video_aprovado = 1)) AND (id_idioma = $ling OR id_idioma = 4) ORDER BY data DESC LIMIT 1, 5";
        else
            $query = "SELECT * FROM videos WHERE (pre_aprovado = 1 OR (texto_aprovado = 1  AND video_aprovado = 1)) ORDER BY data DESC LIMIT 1, 5";
        $data['video_lista'] = $this->db->query($query)->result();
        //==========================//

        // NOVA QUERY - NOTÍCIAS
        if($ling != 4)
            $query = "SELECT * FROM noticias WHERE id_idioma = $ling || id_idioma = 4 ORDER BY data DESC LIMIT 0, 3";
        else
            $query = "SELECT * FROM noticias ORDER BY data DESC LIMIT 0, 3";
        $data['noticias'] = $this->db->query($query)->result();
        //==========================//


        foreach ($data['noticias'] as $key => $value) {
            $query_imagens = $this->db->get_where('noticias_imagens', array('id_parent' => $value->id))->result();
            if(sizeof($query_imagens) > 0)
                $value->imagem = $query_imagens[0]->imagem;
            else
                $value->imagem = FALSE;
        }

        $data['aovivo'] = $this->db->get('video_aovivo', 1, 0)->result();

		$this->load->view('public/common/header');
    	$this->load->view('public/common/menu', $menu);
    	$this->load->view('public/index', $data);
    	$this->load->view('public/common/footer', $footer);
    }

}