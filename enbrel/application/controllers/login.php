<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct(){
   		parent::__construct();
    }

    function index(){

        if($this->session->userdata('logado'))
            redirect('home');

        $menu['slides'] = $this->db->get('slides')->result();
        $footer['banner'] = $this->db->get('banners', 1, 0)->result();

    	$this->load->view('public/common/header');
    	$this->load->view('public/common/menu', $menu);
    	$this->load->view('login');
    	$this->load->view('public/common/footer', $footer);
    }



    function modal(){
    	$this->load->view('login');
    }



    function recuperacao(){
        $this->load->view('public/recuperacao');
    }



    function logar(){
        $this->load->library('login_user');

    	if($this->session->flashdata('temp-user') && $this->session->flashdata('temp-pass')){
            $usuario = $this->session->flashdata('temp-user');
            $senha = criptografar($this->session->flashdata('temp-pass'));
        }else{
            $usuario = $this->input->post('login');
            $senha = criptografar($this->input->post('senha'));
        }

        if(!$this->input->is_ajax_request()){

            if(!$this->login_user->login('', $usuario, $senha)){
                $this->session->set_flashdata('login-fail', TRUE);
                redirect('publichome', 'refresh');
            }else{
                logAcesso();
                redirect('home', 'refresh');
            }

        }else{

            if(!$this->login_user->login('', $usuario, $senha)){
                echo 0;
            }else{
                logAcesso();
                echo 1;
            }
        }

    }

    function recuperar(){

        $email = $this->input->post('login');

        $query = $this->db->get_where('usuarios', array('email' => $email));

        if($query->num_rows() == 0){
            echo 0;
        }else{
            $fetch = $query->result();
            $this->enviaSenha($email, $fetch[0]->idioma_interface);
            echo 1;
        }

    }


    function logar_pos_cadastro(){

        $this->load->library('login_user');

        $usuario = $this->session->flashdata('temp-user');
        $senha = criptografar($this->session->flashdata('temp-pass'));        

        if(!$this->login_user->login('', $usuario, $senha))
            $this->session->set_flashdata('errlogin', true);
        else
            logAcesso();

        redirect('home');
    }



    function logout(){
        $this->load->library('login_user');
        $this->login_user->logout();
        redirect('home');
    }



    function cadastro(){

        $data['padrao'] = default_lang();

        $data['ESPECIALIDADES'] = array(
            'Alergia e Imunologia',
            'Anestesiologia',
            'Angiologia',
            'Cardiologia',
            'Cirurgia Vascular',
            'Clínica Médica',
            'Coloproctologia',
            'Dermatologia',
            'Endocrinologia e Metabologia',
            'Gastroenterologia',
            'Geriatria',
            'Ginecologia e Obstetrícia',
            'Hematologia e Hemoterapia',
            'Infectologia',
            'Mastologia',
            'Nefrologia',
            'Neurologia',
            'Oftalmologia',
            'Oncologia',
            'Ortopedia e Traumatologia',
            'Otorrinolaringologia',
            'Pediatria',
            'Pneumologia',
            'Psiquiatria',
            'Reumatologia',
            'Urologia'
        );

        $data['temas'] = $this->db->get('videos_categorias')->result();
        $menu['slides'] = $this->db->get('slides')->result();
        $footer['banner'] = $this->db->get('banners', 1, 0)->result();

        $this->load->view('public/common/header');
        $this->load->view('public/common/menu', $menu);
        $this->load->view('public/cadastro', $data);
        $this->load->view('public/common/footer', $footer);
    }

    function excluirUsuario(){
        $email = "daniela@segmentofarma.com.br";

        if($email){

            $qry_senha = $this->db->get_where('usuarios', array('email' => $email))->result();

            if(!isset($qry_senha[0]))
                return false;

            $ITV_pID = "fc34d063-8d9d-4e8d-958e-8f3a39e81525";
            $ITV_login = $email;
            $ITV_senha = $qry_senha[0]->senha;

            $ITV = array(
                'pID'   => $ITV_pID,
                'login' => $ITV_login,
                'senha' => $ITV_senha
            );

            if(in_array('curl', get_loaded_extensions())) {
                $url = 'http://itv.netpoint.com.br/reumatoonline/adm/deleteUsuario.asp';
                $ch = curl_init($url);
                 
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $ITV);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                 
                $response = curl_exec($ch);
                curl_close($ch);

                if(is_array($response))
                    print_r($response);
                else
                    echo $response;
            }
        }else{
            echo "Nenhum email";
        }
    }

    function cadastrar(){

        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $senha = $this->input->post('senha');
        $especialidade = $this->input->post('especialidade');
        $crm = $this->input->post('crm');
        $pais = $this->input->post('pais');
        $aceite = $this->input->post('aceite');
        $idioma_interface = $this->input->post('idioma_interface');
        $idioma_conteudo = $this->input->post('idioma_conteudo');
        $temas = $this->input->post('temas');

        if($nome == '' || $senha == '' || $email == '' || $especialidade == '' || $crm == '' || $pais == '' || $aceite == ''){

            $this->session->set_flashdata('erro-form', 'Todos os campos são obrigatórios!');
            redirect('login/cadastro', 'refresh');

        }else{

            if($this->db->get_where('usuarios', array('email' => $email))->num_rows() > 0){
                $this->session->set_flashdata('erro-form', "O email já está em uso! Caso tenha esquecido a senha. Clique em 'Esqueci a senha' no menu superior.");
                redirect('login/cadastro', 'refresh');  
            }

            $ITV_pID = "fc34d063-8d9d-4e8d-958e-8f3a39e81525";
            $ITV_nome = $nome;
            $ITV_login = $email;
            $ITV_senha = $senha;

            $ITV = array(
                'pID'   => $ITV_pID,
                'nome'  => $ITV_nome,
                'login' => $ITV_login,
                'senha' => criptografar($ITV_senha)
            );

            if(in_array('curl', get_loaded_extensions())) {
                $url = 'http://itv.netpoint.com.br/reumatoonline/adm/insertUsuario.asp';
                $ch = curl_init($url);
                 
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $ITV);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                 
                $response = curl_exec($ch);
                curl_close($ch);
            }

            if(!$idioma_interface || $idioma_interface == '')
                $idioma_interface = default_lang();

            if(!$idioma_conteudo || $idioma_conteudo == '')
                $idioma_conteudo = default_lang();

            $insert_user = $this->db->set('nome', $nome)
                                    ->set('email', $email)
                                    ->set('senha', criptografar($senha))
                                    ->set('id_especialidade', $especialidade)
                                    ->set('crm', $crm)
                                    ->set('pais', $pais)
                                    ->set('idioma_interface', $idioma_interface)
                                    ->set('idioma_conteudo', $idioma_conteudo)
                                    ->insert('usuarios');

            $id_usuario = $this->db->insert_id();

            if($temas){

                foreach ($temas as $key => $value) {

                    $this->db->set('id_usuario', $id_usuario)
                             ->set('id_videos_categorias', $value)
                             ->insert('usuarios_temas');
                }
            }


            $this->session->set_flashdata('temp-user', $email);
            $this->session->set_flashdata('temp-pass', $senha);
            redirect('login/logar_pos_cadastro');
        }

    } 

    function teste(){
        $ITV_pID = "fc34d063-8d9d-4e8d-958e-8f3a39e81525";
        $ITV_nome = 'teste'.rand(1,50);
        $ITV_login = 'teste@email.com'.rand(1,50);
        $ITV_senha = criptografar('teste');

        echo "Cadastro de Usu&aacute;rio Teste (concatenado com n&uacute;mero aleat&oacute;rio para evitar repeti&ccedil;&atilde;o)<br>";
        echo "Nome : $ITV_nome <br>";
        echo "Login : $ITV_login <br>";
        echo "pID : $ITV_pID <br>";
        echo "Senha (criptografada) : $ITV_senha <br><br>";
        echo "Dados sendo enviados por POST para : http://itv.netpoint.com.br/reumatoonline/adm/insertUsuario.asp<br>";
        echo "Resposta:<br>";

        $ITV = array(
            'pID'   => $ITV_pID,
            'nome'  => $ITV_nome,
            'login' => $ITV_login,
            'senha' => $ITV_senha
        );

        
        $url = 'http://itv.netpoint.com.br/reumatoonline/adm/insertUsuario.asp';
        $ch = curl_init($url);
         
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $ITV);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         
        $response = curl_exec($ch);
        curl_close($ch);
        echo "<pre>";
        var_dump($response);
        echo "</pre>";
    }



    function enviaSenha($email, $idioma = 1){

        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        $emailconf['protocol'] = 'mail';
        //$emailconf['smtp_host'] = 'smtp.arteriaatelie.com';
        //$emailconf['smtp_user'] = 'noreply@arteriaatelie.com';
        //$emailconf['smtp_pass'] = 'arteria22';
        //$emailconf['smtp_port'] = 587;
        //$emailconf['crlf'] = "\r\n";
        //$emailconf['newline'] = "\r\n";

        $this->load->library('email');
        $this->email->initialize($emailconf);

        $novaSenha = randomPassword();

        $this->db->set('senha', criptografar($novaSenha))->where('email', $email)->update('usuarios');
        $from = 'noreply@reumatoonline.com';
        $fromname = 'Recuperação de Senha';

        if($idioma != 1){
            $msg1 = "Su contraseña temporal para acceder al sistema es";
            $msg2 = "Ingrese con su nombre de usuario y visite nuestro sitio web";
        }else{
            $msg1 = "Sua senha temporária para acesso ao sistema é";
            $msg2 = "Faça o login visitando nosso site";
        }

        $to = $email;
        $bc = FALSE;
        //$bcc = FALSE;

        $bcc = 'bruno@trupe.net';

        $assunto = 'Recuperação de Senha';

        $email = "<html>
                    <head>
                        <style type='text/css'>
                            .tit{
                                font-weight:bold;
                                font-size:14px;
                                color:#5DB8C9;
                                font-family:Arial;
                            }
                            .val{
                                color:#000;
                                font-size:12px;
                                font-family:Arial;
                            }
                        </style>
                    </head>
                    <body>                   

                    $msg1 : $novaSenha<br>
                    $msg2 : <a href='http://www.reumatoonline.com'>reumatoonline.com</a>
                    </body>
                    </html>";

        $this->email->from($from, $fromname);
        $this->email->to($to);

        if($bc)
            $this->email->cc($bc);
        if($bcc)
            $this->email->bcc($bcc);

        $this->email->reply_to($email);
        $this->email->subject($assunto);
        $this->email->message($email);
        $this->email->send();
    }
}
