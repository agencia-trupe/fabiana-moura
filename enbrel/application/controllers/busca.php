<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Busca extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index($termo = '', $pag = 0){

    	if(isset($_POST['termo']))
    		$termo = $this->input->post('termo');

    	if(!$termo)
    		redirect('home');

    	
    	$data['qry_videos'] = "SELECT * FROM videos WHERE MATCH(titulo, olho, texto, palestrante) AGAINST ('%".$termo."%') AND (pre_aprovado = 1 OR (video_aprovado = 1 AND texto_aprovado = 1))";
    	if($this->session->userdata('idioma_conteudo') != 4){
    		$query .= " AND (id_idioma = ".$this->session->userdata('idioma_conteudo')." OR id_idioma = 4)";
    	}

    	$data['qry_noticias'] = "SELECT * FROM noticias WHERE MATCH(titulo, olho, texto) AGAINST ('%".$termo."%')";
    	if($this->session->userdata('idioma_conteudo') != 4){
    		$query .= " AND (id_idioma = ".$this->session->userdata('idioma_conteudo')." OR id_idioma = 4)";
    	}

    	$data['qry_washington'] = "SELECT * FROM washington WHERE MATCH(titulo, olho, texto) AGAINST ('%".$termo."%')";
    	if($this->session->userdata('idioma_conteudo') != 4){
    		$query .= " AND (id_idioma = ".$this->session->userdata('idioma_conteudo')." OR id_idioma = 4)";
    	}

    	$data['videos'] = $this->db->query($data['qry_videos'])->result();
    	$data['noticias'] = $this->db->query($data['qry_noticias'])->result();
    	$data['washington'] = $this->db->query($data['qry_washington'])->result();
        $data['termo'] = $termo;

        if ($data['noticias']) {
            foreach ($data['noticias'] as $key => $value) {
                $qry_img = $this->db->get_where('noticias_imagens', array('id_parent' => $value->id))->result();
                if(isset($qry_img[0]))
                    $value->imagem = $qry_img[0]->imagem;
                else
                    $value->imagem = false;
            }
        }

        if ($data['washington']) {
            foreach ($data['washington'] as $key => $value) {
                $qry_img = $this->db->get_where('washington_imagens', array('id_parent' => $value->id))->result();
                if(isset($qry_img[0]))
                    $value->imagem = $qry_img[0]->imagem;
                else
                    $value->imagem = false;
            }
        }        

    	$this->load->view('busca', $data);
    }

}