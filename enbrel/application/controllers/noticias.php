<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

   		limpaSessao();
    }

    function index($pag = 0){

    	$this->session->set_userdata('origem-noticia', 'lista');

    	$query = "SELECT * FROM noticias";
    	if($this->session->userdata('idioma_conteudo') != 4){
    		$query .= " WHERE (id_idioma = ".$this->session->userdata('idioma_conteudo')." OR id_idioma = 4)";
    	}

    	$data['sidebar'] = $this->db->query($this->criaQuery())->result();
    	$data['categorias'] = $this->db->order_by('titulo', 'asc')->get('noticias_categorias')->result();

        $this->load->library('pagination');
        $config['base_url'] = base_url('noticias/index');
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['prev_link'] = FALSE;
        $config['next_link'] = FALSE;
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['num_links'] = 10;
        $config['total_rows'] = $this->db->query($query)->num_rows();

        $this->pagination->initialize($config); 
        $data['paginacao'] = $this->pagination->create_links();       	

        $query .= " ORDER BY data DESC LIMIT $pag, ".$config['per_page'];
    	$data['noticias'] = $this->db->query($query)->result();

    	$data['debug'] = $this->db->last_query();

    	foreach ($data['noticias'] as $key => $value) {
    		$qry_imagem = $this->db->get_where('noticias_imagens', array('id_parent' => $value->id))->result();
    		$value->imagem = isset($qry_imagem[0]) ? $qry_imagem[0]->imagem : FALSE;
    		$qry_autor = $this->db->get_where('usuarios_painel', array('id' => $value->id_autor))->result();
    		$value->nome_autor = isset($qry_autor[0]) ? $qry_autor[0]->nome : 'Autor não encontrado';
    	}
    	$data['filtro'] = FALSE;

    	$this->load->view('noticias', $data);
    }

    function categoria($categoria, $pag = 0){

    	$this->session->set_userdata('origem-noticia', 'categoria');
    	
    	if(!$categoria)
    		redirect('noticias');

    	$search = $this->db->get_where('noticias_categorias', array('slug' => $categoria))->result();
    	if(!$search[0])
    		redirect('noticias');

    	$data['categoria_atual'] = $search[0];

    	$data['sidebar'] = $this->db->query($this->criaQuery())->result();
    	$data['categorias'] = $this->db->order_by('titulo', 'asc')->get('noticias_categorias')->result();

    	$query = "SELECT * FROM noticias WHERE id_categoria = ".$data['categoria_atual']->id;
    	if($this->session->userdata('idioma_conteudo') != 4){
    		$query .= " AND (id_idioma = ".$this->session->userdata('idioma_conteudo')." OR id_idioma = 4)";
    	}    	

        $this->load->library('pagination');
        $config['base_url'] = base_url('noticias/categoria/'.$categoria);
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $config['prev_link'] = FALSE;
        $config['next_link'] = FALSE;
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['num_links'] = 10;
        $config['total_rows'] = $this->db->query($query)->num_rows();
        $this->pagination->initialize($config); 
        $data['paginacao'] = $this->pagination->create_links();       	

        $query .= " ORDER BY data DESC LIMIT $pag, ".$config['per_page'];
    	$data['noticias'] = $this->db->query($query)->result();

    	foreach ($data['noticias'] as $key => $value) {
    		$qry_imagem = $this->db->get_where('noticias_imagens', array('id_parent' => $value->id))->result();
    		$value->imagem = isset($qry_imagem[0]) ? $qry_imagem[0]->imagem : FALSE;
    		$qry_autor = $this->db->get_where('usuarios_painel', array('id' => $value->id_autor))->result();
    		$value->nome_autor = isset($qry_autor[0]) ? $qry_autor[0]->nome : 'Autor não encontrado';
    	}
    	$data['filtro'] = $data['categoria_atual']->titulo;

    	$this->session->set_userdata('origem-termo', $data['categoria_atual']->id);

    	$this->load->view('noticias', $data);
    }

    function busca($termo = '', $pag = 0){

    	if(isset($_POST['termo']))
    		$termo = $this->input->post('termo');

    	if(!$termo)
    		redirect('noticias');

    	$this->session->set_userdata('origem-noticia', 'busca');
    	$this->session->set_userdata('origem-termo', $termo);
    	
    	$data['sidebar'] = $this->db->query($this->criaQuery())->result();
    	$data['categorias'] = $this->db->order_by('titulo', 'asc')->get('noticias_categorias')->result();

    	$query = "SELECT * FROM noticias WHERE MATCH(titulo, olho, texto) AGAINST ('%".$termo."%')";
    	if($this->session->userdata('idioma_conteudo') != 4){
    		$query .= " AND (id_idioma = ".$this->session->userdata('idioma_conteudo')." OR id_idioma = 4)";
    	}

        $this->load->library('pagination');
        $config['base_url'] = base_url('noticias/categoria/'.$termo);
        $config['per_page'] = 1;
        $config['uri_segment'] = 4;
        $config['prev_link'] = FALSE;
        $config['next_link'] = FALSE;
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['num_links'] = 10;
        $config['total_rows'] = $this->db->query($query)->num_rows();
        $this->pagination->initialize($config); 
        $data['paginacao'] = $this->pagination->create_links();       	

        $query .= " ORDER BY data DESC LIMIT $pag, ".$config['per_page'];
    	$data['noticias'] = $this->db->query($query)->result();

    	foreach ($data['noticias'] as $key => $value) {
    		$qry_imagem = $this->db->get_where('noticias_imagens', array('id_parent' => $value->id))->result();
    		$value->imagem = isset($qry_imagem[0]) ? $qry_imagem[0]->imagem : FALSE;
    		$qry_autor = $this->db->get_where('usuarios_painel', array('id' => $value->id_autor))->result();
    		$value->nome_autor = isset($qry_autor[0]) ? $qry_autor[0]->nome : 'Autor não encontrado';
    	}
    	$data['filtro'] = $termo;

    	$this->load->view('noticias', $data);
    }

    function detalhes($slug = false){
    	if(!$slug)
    		redirect('noticias');

    	$data['detalhe'] = $this->db->get_where('noticias', array('slug' => $slug))->result();
    	
    	if(!$data['detalhe'])
    		redirect('noticias');

		foreach ($data['detalhe'] as $key => $value) {
    		$qry_imagem = $this->db->get_where('noticias_imagens', array('id_parent' => $value->id))->result();
    		$value->imagem = isset($qry_imagem[0]) ? $qry_imagem[0]->imagem : FALSE;
    		$qry_autor = $this->db->get_where('usuarios_painel', array('id' => $value->id_autor))->result();
    		$value->nome_autor = isset($qry_autor[0]) ? $qry_autor[0]->nome : 'Autor não encontrado';
    	}    	

    	$data['galeria'] = $this->db->get_where('noticias_imagens', array('id_parent' => $data['detalhe'][0]->id))->result();
    	$data['sidebar'] = $this->db->query($this->criaQuery())->result();
    	$data['categorias'] = $this->db->order_by('titulo', 'asc')->get('noticias_categorias')->result();

    	$data['noticia_anterior'] = $this->linkNavegacao($data['detalhe'][0]->data, $data['detalhe'][0]->id, 'prev');
    	$data['proxima_noticia'] = $this->linkNavegacao($data['detalhe'][0]->data, $data['detalhe'][0]->id, 'next');

    	$this->load->view('noticias-detalhe', $data);
    }

    function linkNavegacao($data_atual, $id_atual, $direcao){
    	$origem = $this->session->userdata('origem-noticia');
    	$termo = $this->session->userdata('origem-termo');

    	$query = "SELECT * FROM noticias";

    	if($this->session->userdata('idioma_conteudo') != 4)
    		$query .= " WHERE (id_idioma = ".$this->session->userdata('idioma_conteudo')." OR id_idioma = 4)";
    	else
    		$query .= " WHERE 1 = 1";

    	switch ($origem) {
    		case 'lista':
    			break;
    		case 'categoria':
    			if($termo != '')
    				$query .= " AND id_categoria = ".$termo;
    			break;
    		case 'busca':
    			if($termo != '')
    				$query .= " AND MATCH(titulo, olho, texto) AGAINST('%".$termo."%')";
    			break;
    		default:
    			return false;
    			break;
    	}

    	if($direcao == 'next')
    		$query .= " AND id != '$id_atual' AND data > '$data_atual' ORDER BY data ASC, id DESC LIMIT 1";
    	else
    		$query .= " AND id != '$id_atual' AND data < '$data_atual' ORDER BY data DESC, id ASC LIMIT 1";

    	$execute = $this->db->query($query)->result();

    	if(sizeof($execute) > 0)
    		return $execute[0]->slug;
    	else
    		return false;
    }

    // function fill(){
    // 	for ($i=0; $i < 60; $i++) { 
    // 		$this->db->set('id_categoria', rand(1, 8))
    // 				 ->set('data', rand(2011, 2012).'/'.rand(0,1).rand(1,2).'/'.rand(0, 2).rand(1, 9))
    // 				 ->set('titulo', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit')
    // 				 ->set('slug', 'lorem_ipsum_dolor_sit_amet,_consectetur_adipisicing_elit_'.$i)
    // 				 ->set('olho', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
    // 				 ->set('texto', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.')
    // 				 ->set('id_autor', 8)
    // 				 ->set('data_postagem', date('Y-m-d H:i:s'))
    // 				 ->set('id_idioma', rand(2,3))
    // 				 ->insert('noticias');
    // 		echo "Registro ".($i + 1).'<br>';
    // 	}
    // }
}