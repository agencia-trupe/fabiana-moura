<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index($pag=0){

    	$data['palestrantes'] = $this->db->select('palestrante')->distinct()->get('videos')->result();
    	$data['temas'] = $this->db->get('videos_categorias')->result();

    	$query = $this->criaQuery();

        $this->load->library('pagination');
        $config['base_url'] = base_url('videos/index');
        $config['per_page'] = 15;
        $config['uri_segment'] = 3;
        $config['prev_link'] = FALSE;
        $config['next_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['first_link'] = FALSE;
        $config['num_links'] = 10;
        $config['total_rows'] = $this->db->query($query)->num_rows();
        $this->pagination->initialize($config); 
        $data['paginacao'] = $this->pagination->create_links();        

        $query .= " LIMIT $pag, ".$config['per_page'];

        $data['query'] = $query . " - " .$config['total_rows'];
        $data['videos'] = $this->db->query($query)->result();

        if($this->session->userdata('busca-ordem') || $this->input->post('ordem')){
        	$data['ordem'] = ($this->input->post('ordem') != '') ? $this->input->post('ordem') : $this->session->userdata('busca-ordem');
        }else{
        	$data['ordem'] = FALSE;
        }

        if($this->session->userdata('busca-palestrante') || $this->input->post('palest')){
        	$data['palest'] = ($this->input->post('palest') != '') ? $this->input->post('palest') : $this->session->userdata('busca-palestrante');
        }else{
        	$data['palest'] = FALSE;
        }

        if($this->session->userdata('busca-tema') || $this->input->post('id_tema')){
        	$data['tema'] = ($this->input->post('id_tema') != '') ? $this->input->post('id_tema') : $this->session->userdata('busca-tema');
        }else{
        	$data['tema'] = FALSE;
        }

		if(($this->session->userdata('busca-data-inicio') && $this->session->userdata('busca-data-termino')) || ($this->input->post('data_inicio') && $this->input->post('data_termino'))){
			$data['dt_inicio'] = $this->input->post('data_inicio') != '' ? $this->input->post('data_inicio') : $this->session->userdata('busca-data-inicio');
			$data['dt_termino'] = $this->input->post('data_termino') != '' ? $this->input->post('data_termino') : $this->session->userdata('busca-data-termino');
		}else{
			$data['dt_inicio'] = false;
			$data['dt_termino'] = false;
		}

    	$this->load->view('videos', $data);
    }

    function detalhes($slug){

        logAtividadeVideos();

        $data['video'] = $this->db->get_where('videos', array('slug' => $slug))->result();


        if(!$data['video'])
            redirect('videos');

        $qry_tema = $this->db->get_where('videos_categorias', array('id' => $data['video'][0]->id_tema))->result();
        $data['video'][0]->nome_tema = isset($qry_tema[0]) ? $qry_tema[0]->titulo : 'Tema não encontrado';

        $data['relacionamentos_noticias'] = $this->db->get_where('videos_relacionamentos', array('id_video' => $data['video'][0]->id, 'tipo_relacionamento' => 'noticia'))->result();
        foreach ($data['relacionamentos_noticias'] as $key => $value) {
            $qry_source = $this->db->get_where('noticias', array('id' => $value->endereco))->result();
            $value->source = $qry_source[0];
            $qry = $this->db->get_where('noticias_categorias', array('id' => $value->source->id_categoria))->result();
            $value->titulo_categoria = isset($qry[0]) ? $qry[0]->titulo : 'Categoria não encontrada';
        }

        $data['relacionamentos_washington'] = $this->db->get_where('videos_relacionamentos', array('id_video' => $data['video'][0]->id, 'tipo_relacionamento' => 'washington'))->result();
        foreach ($data['relacionamentos_washington'] as $key => $value) {
            $qry_source = $this->db->get_where('washington', array('id' => $value->endereco))->result();
            $value->source = $qry_source[0];
            $qry = $this->db->get_where('washington_categorias', array('id' => $value->source->id_categoria))->result();
            $value->titulo_categoria = isset($qry[0]) ? $qry[0]->titulo : 'Categoria não encontrada';
        }        
        $data['relacionamentos_links'] = $this->db->get_where('videos_relacionamentos', array('id_video' => $data['video'][0]->id, 'tipo_relacionamento' => 'externo'))->result();

        $data['sidebar'] = $this->db->query($this->criaQuery($data['video'][0]->id))->result();

        $this->load->view('videos-detalhe', $data);

    }
    
}
