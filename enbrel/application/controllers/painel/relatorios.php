<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Relatorios extends MY_Admincontroller {

   /*
      TIPOS DE USUÁRIOS
      1 - Administrador (vê e faz tudo)
      2 - Videomaker (envia vídeos com e sem aprovação e alterar link para vídeos ao vivo)
      3 - Redator (mantêm notícias)
      4 - Avaliador (aprova vídeos e textos e mantêm os vídeo aprovados)
   */

   function __construct(){
   	parent::__construct();
   }

   function index(){
      $this->load->view('painel/relatorios/index');
   }

   function aprovados($campo_ordem = 'data_postagem', $ordem = 'desc', $pag = 0){

      if(!isAdmin())
         redirect('painel/relatorios');

      $data['ordenacao'] = $campo_ordem;
      $query = "SELECT * FROM videos WHERE (pre_aprovado = 1 OR (video_aprovado = 1 AND texto_aprovado = 1)) ORDER BY $campo_ordem $ordem LIMIT $pag, 50";

      $data['videos_aprovados'] = $this->db->query($query)->result();
      
      foreach ($data['videos_aprovados'] as $value) {
         $query_aprovador_texto = $this->db->get_where('usuarios_painel', array('id' => $value->texto_aprovado_por))->result();
         if(!isset($query_aprovador_texto[0]))
            $value->texto_aprovado_por = 'N.E.';
         else
            $value->texto_aprovado_por = $query_aprovador_texto[0]->nome;

         $query_aprovador_video = $this->db->get_where('usuarios_painel', array('id' => $value->video_aprovado_por))->result();
         if(!isset($query_aprovador_video[0]))
            $value->video_aprovado_por = 'N.E.';
         else
            $value->video_aprovado_por = $query_aprovador_video[0]->nome;

         $query_post = $this->db->get_where('usuarios_painel', array('id' => $value->id_autor))->result();
         if(!isset($query_post[0]))
            $value->id_autor = 'N.E.';
         else
            $value->id_autor = $query_post[0]->nome;

         $value->alteracoes = $this->db->get_where('videos_log', array('id_registro' => $value->id))->result();

         foreach ($value->alteracoes as $alt) {

            $query_alteracao = $this->db->get_where('usuarios_painel', array('id' => $alt->id_usuario))->result();
            if(!isset($query_alteracao[0]))
               $alt->id_usuario = 'N.E.';
            else
               $alt->id_usuario = $query_alteracao[0]->nome;
         }
      }

      $this->load->view('painel/relatorios/aprovados', $data);
   }

   function estatisticas(){

      if(!isAdmin())
         redirect('painel/relatorios');

      $data['total_usuarios'] = $this->db->get('usuarios')->num_rows();
      $data['total_videos'] = $this->db->get('videos')->num_rows();
      $data['total_videos_aprovados'] = $this->db->query('SELECT * FROM videos WHERE (pre_aprovado = 1 OR (video_aprovado = 1 AND texto_aprovado = 1))')->num_rows();

      $data['categorias'] = $this->db->get('videos_categorias')->result();
      foreach ($data['categorias'] as $value) {
         $value->numero_videos = $this->db->get_where('videos', array('id_tema' => $value->id))->num_rows();
      }

      $data['noticias'] = $this->db->get('noticias')->num_rows();

      $data['noticias_categorias'] = $this->db->get('noticias_categorias')->result();
      foreach ($data['noticias_categorias'] as $key => $value) {
         $value->num_noticias = $this->db->get_where('noticias', array('id_categoria' => $value->id))->num_rows();
      }

      $contador_categoria = array();
      $contador_videos = array();

      $acessos_videos = $this->db->query("SELECT log_atividade_videos.*, videos_categorias.titulo as 'nome_categoria', videos.titulo as 'nome_video' FROM log_atividade_videos LEFT JOIN videos_categorias ON log_atividade_videos.categoria = videos_categorias.id LEFT JOIN videos ON log_atividade_videos.video_acessado = videos.id")->result();

      foreach ($acessos_videos as $value) {
         
         if(isset($contador_categoria[$value->nome_categoria]))
            $contador_categoria[$value->nome_categoria] = $contador_categoria[$value->nome_categoria] + 1;
         else
            $contador_categoria[$value->nome_categoria] = 1;

         if(isset($contador_videos[$value->nome_video]))
            $contador_videos[$value->nome_video] = $contador_videos[$value->nome_video] + 1;
         else
            $contador_videos[$value->nome_video] = 1;

      }

      $data['contador_videos'] = $contador_videos;
      $data['contador_categoria'] = $contador_categoria;

      $this->load->view('painel/relatorios/estatisticas', $data);
   }

}