<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aprovar extends MY_Admincontroller {

    function __construct(){
	   	parent::__construct();

	   	if(!isAdmin() && !isAvaliador())
	   		redirect('painel');

	   	$this->titulo = 'Vídeos Aguardando Aprovação';
	   	$this->unidade = 'Vídeo';
	   	$this->load->model('videos_model', 'model');
	   	$this->load->model('videos_categorias_model', 'categorias');
    }

    function index(){    	
    	$data['registros'] = $this->model->pegarAguardando();

        $data['mostrar'] = 'preaprovados';
        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $data['campo_1'] = $this->campo_1;
        $data['campo_2'] = $this->campo_2;
        $data['campo_3'] = $this->campo_3;
        $this->load->view('painel/videos/lista_aprovar', $data);    	
    }

    function aprovarVideo($id = false){
    	if(!$id)
    		redirect('painel/'.$this->router->class);
    	else{
    		$data['registro'] = $this->model->pegarPorId($id);
	        $data['relacionamentos'] = $this->db->get_where('videos_relacionamentos', array('id_video'=>$id))->result();
	        if(!$data['registro'])
	            redirect('painel/'.$this->router->class);
    	}

    	$data['registro']->titulo_tema = $this->categorias->pegarPorId($data['registro']->id_tema);
    	$query = $this->db->get_where('idiomas', array('id' => $data['registro']->id_idioma))->result();
    	$data['registro']->titulo_idioma = $query[0]->titulo;

    	$this->load->view('painel/videos/visualizar', $data);
    }

    function confirmaAprovacao($id, $parcial = false){
    	if(!$id)
    		redirect("painel/aprovar/index");

    	if(!$parcial){
		    if($this->model->publicar($id)){
		        $this->session->set_flashdata('mostrarsucesso', true);
		        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Texto e Vídeos aprovados com sucesso. Vídeo Publicado!');
		        $this->model->log($id, "Publicou Registro");
		    }else{
		        $this->session->set_flashdata('mostrarerro', true);
		        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao publicar Vídeo!');
		    }
    	}elseif($parcial=='video'){

    		$consulta = $this->model->pegarPorId($id);
    		if($consulta->texto_aprovado==1)
    			$mensagem = "Vídeo Publicado!";
    		else
    			$mensagem = "Será Publicado após aprovação do Texto.";

	        if($this->model->aprovarVideo($id)){
	            $this->session->set_flashdata('mostrarsucesso', true);
	            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Vídeo aprovado com sucesso. '.$mensagem);
	            $this->model->log($id, "Aprovou Video do Registro");
	        }else{
	            $this->session->set_flashdata('mostrarerro', true);
	            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao aprovar Vídeo!');
	        }
    	}elseif($parcial=='texto'){

    		$consulta = $this->model->pegarPorId($id);
    		if($consulta->video_aprovado==1)
    			$mensagem = "Vídeo Publicado!";
    		else
    			$mensagem = "Será Publicado após aprovação do Vídeo.";    			

		    if($this->model->aprovarTexto($id)){
		        $this->session->set_flashdata('mostrarsucesso', true);
		        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Texto aprovado com sucesso. '.$mensagem);
		        $this->model->log($id, "Aprovou Texto do Registro");
		    }else{
		        $this->session->set_flashdata('mostrarerro', true);
		        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao aprovar Texto!');
		    }
    	}

        redirect('painel/'.$this->router->class.'/index', 'refresh');
    }
}