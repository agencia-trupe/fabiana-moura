<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners extends MY_Admincontroller {

   /*
      TIPOS DE USUÁRIOS
      1 - Administrador (vê e faz tudo)
      2 - Videomaker (envia vídeos com e sem aprovação e alterar link para vídeos ao vivo)
      3 - Redator (mantêm notícias)
      4 - Avaliador (aprova vídeos e textos e mantêm os vídeo aprovados)
   */

   function __construct(){
   	parent::__construct();

      if(!isAdmin())
         redirect('painel/home');
      
   	$this->load->model('banners_model', 'model');
   }

}