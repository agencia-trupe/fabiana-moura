<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Washington extends MY_Admincontroller {

    function __construct(){
	   	parent::__construct();

	   	if(!isAdmin() && !isRedator())
	   		redirect('painel');

	   	$this->titulo = 'Notícias de Washington';
	   	$this->unidade = 'Notícia de Washington';
	   	$this->load->model('washington_model', 'model');
	   	$this->load->model('washington_categorias_model', 'categorias');
    }

    function index($idioma = false){
        $data['registros'] = $this->model->pegarPorIdioma($idioma);

        $data['titulo'] = $this->titulo;
        $data['idioma'] = $idioma;
        $data['unidade'] = $this->unidade;
        $data['campo_1'] = $this->campo_1;
        $data['campo_2'] = $this->campo_2;
        $data['campo_3'] = $this->campo_3;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function form($id = false){
	    if($id){
	        $data['registro'] = $this->model->pegarPorId($id);
	        if(!$data['registro'])
	            redirect('painel/'.$this->router->class);
	    }else{
	        $data['registro'] = FALSE;
	    }

	    $data['titulo'] = $this->titulo;
	    $data['unidade'] = $this->unidade;
	    $data['idiomas'] = $this->db->get('idiomas', 2, 0)->result();
	    $data['categorias'] = $this->categorias->pegarTodos();
	    $this->load->view('painel/'.$this->router->class.'/form', $data);
    }


    function categorias(){
	    $data['registros'] = $this->categorias->pegarTodos();

	    $data['titulo'] = $this->titulo;
	    $data['unidade'] = $this->unidade;
	    $data['campo_1'] = 'Título';
	    $this->load->view('painel/'.$this->router->class.'/lista_categorias', $data);
    }

    function categorias_form($id = false){
	    if($id){
	        $data['registro'] = $this->categorias->pegarPorId($id);
	        if(!$data['registro'])
	            redirect('painel/'.$this->router->class.'/categorias');
	    }else{
	        $data['registro'] = FALSE;
	    }

	    $data['titulo'] = $this->titulo;
	    $data['unidade'] = $this->unidade;
	    $this->load->view('painel/'.$this->router->class.'/form_categorias', $data);
    }

    function categorias_inserir(){
	    if($this->categorias->inserir()){
	        $this->session->set_flashdata('mostrarsucesso', true);
	        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria inserida com sucesso');
	    }else{
	        $this->session->set_flashdata('mostrarerro', true);
	        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir Categoria');
	    }

	    redirect('painel/'.$this->router->class.'/categorias', 'refresh');
    }

    function categorias_alterar($id){
	    if($this->categorias->alterar($id)){
	        $this->session->set_flashdata('mostrarsucesso', true);
	        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria alterada com sucesso');
	    }else{
	        $this->session->set_flashdata('mostrarerro', true);
	        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Categoria');
	    }
	    redirect('painel/'.$this->router->class.'/categorias', 'refresh');
    }
 
    function categorias_excluir($id){
	    if($this->categorias->excluir($id)){
	        $this->session->set_flashdata('mostrarsucesso', true);
	        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria excluido com sucesso');
	    }else{
	        $this->session->set_flashdata('mostrarerro', true);
	        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Categoria');
	    }

	    redirect('painel/'.$this->router->class.'/categorias', 'refresh');
    }

}