<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends MY_Admincontroller {

    function __construct(){
	   	parent::__construct();

	   	if(!isAdmin() && !isVideomaker() && !isAvaliador())
	   		redirect('painel');

	   	$this->titulo = 'Vídeos';
	   	$this->unidade = 'Vídeo';
	   	$this->load->model('videos_model', 'model');
	   	$this->load->model('videos_categorias_model', 'categorias');
    }

    function index($mostrar = 'todos'){

    	if($mostrar == 'preaprovados')
    		$data['registros'] = $this->model->pegarPreAprovados();
    	elseif($mostrar == 'aprovados')
        	$data['registros'] = $this->model->pegarAprovados();
        elseif($mostrar == 'aguardando')
        	$data['registros'] = $this->model->pegarAguardando();
        else
        	$data['registros'] = $this->model->pegarTodos();

        $data['mostrar'] = $mostrar;
        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $data['campo_1'] = $this->campo_1;
        $data['campo_2'] = $this->campo_2;
        $data['campo_3'] = $this->campo_3;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }    

    function form($id = false){
	    if($id){
	        $data['registro'] = $this->model->pegarPorId($id);
	        $data['relacionamentos'] = $this->db->get_where('videos_relacionamentos', array('id_video'=>$id))->result();
	        if(!$data['registro'])
	            redirect('painel/'.$this->router->class);
	    }else{
	        $data['registro'] = FALSE;
	    }

	    if(!isAdmin() && !isAvaliador() && (isVideomaker() && ($data['registro']->pre_aprovado==0 && ($data['registro']->texto_aprovado==1 && $data['registro']->video_aprovado==1))))
	    	redirect('painel/'.$this->router->class);

	    $data['titulo'] = $this->titulo;
	    $data['unidade'] = $this->unidade;
	    $data['categorias'] = $this->categorias->pegarTodos();
	    $data['idiomas'] = $this->db->get('idiomas')->result();
	    $data['noticias'] = $this->db->get('noticias')->result();
	    $data['washington'] = $this->db->get('washington')->result();
	    $this->load->view('painel/'.$this->router->class.'/form', $data);
    }


    function temas(){
	    $data['registros'] = $this->categorias->pegarTodos();

	    $data['titulo'] = $this->titulo;
	    $data['unidade'] = $this->unidade;
	    $data['campo_1'] = 'Título';
	    $this->load->view('painel/'.$this->router->class.'/lista_categorias', $data);
    }

    function temas_form($id = false){
	    if($id){
	        $data['registro'] = $this->categorias->pegarPorId($id);
	        if(!$data['registro'])
	            redirect('painel/'.$this->router->class.'/temas');
	    }else{
	        $data['registro'] = FALSE;
	    }

	    $data['titulo'] = $this->titulo;
	    $data['unidade'] = $this->unidade;
	    $this->load->view('painel/'.$this->router->class.'/form_categorias', $data);
    }

    function temas_inserir(){
	    if($this->categorias->inserir()){
	        $this->session->set_flashdata('mostrarsucesso', true);
	        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Tema inserido com sucesso');
	    }else{
	        $this->session->set_flashdata('mostrarerro', true);
	        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir Tema');
	    }

	    redirect('painel/'.$this->router->class.'/temas', 'refresh');
    }

    function temas_alterar($id){
	    if($this->categorias->alterar($id)){
	        $this->session->set_flashdata('mostrarsucesso', true);
	        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Tema alterada com sucesso');
	    }else{
	        $this->session->set_flashdata('mostrarerro', true);
	        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Tema');
	    }
	    redirect('painel/'.$this->router->class.'/temas', 'refresh');
    }
 
    function temas_excluir($id){
	    if($this->categorias->excluir($id)){
	        $this->session->set_flashdata('mostrarsucesso', true);
	        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Tema excluido com sucesso');
	    }else{
	        $this->session->set_flashdata('mostrarerro', true);
	        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Tema. Verifique se o tema não está em uso.');
	    }

	    redirect('painel/'.$this->router->class.'/temas', 'refresh');
    }

    function aovivo(){
    	$data['registros'] = $this->db->get('video_aovivo')->result();
    	$this->load->view('painel/'.$this->router->class.'/lista-aovivo', $data);
    }

    function formaovivo($id_registro){
    	$qry = $this->db->get_where('video_aovivo', array('id' => $id_registro))->result();

    	if(!isset($qry[0]))
    		redirect('painel/videos/aovivo');

    	$data['registro'] = $qry[0];
    	$this->load->view('painel/'.$this->router->class.'/form-aovivo', $data);	
    }

    function alteraaovivo($id){
    	$update = $this->db->set('transmitindo', $this->input->post('transmitindo'))
    			 		   ->set('mensagem_pt', $this->input->post('texto_pt'))
    			 		   ->set('mensagem_es', $this->input->post('texto_es'))
    			 		   ->set('alterado_por', $this->session->userdata('id'))
    			 		   ->set('ultima_alteracao', date('Y-m-d H:i:s'))
    			 		   ->set('url', $this->input->post('url'))
    			 		   ->where('id', $id)
    			 		   ->update('video_aovivo');

	    if($update){
	        $this->session->set_flashdata('mostrarsucesso', true);
	        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Transmissão alterada com sucesso');
	    }else{
	        $this->session->set_flashdata('mostrarerro', true);
	        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Transmissão.');
	    }

	    redirect('painel/'.$this->router->class.'/aovivo', 'refresh');    	
    }

}