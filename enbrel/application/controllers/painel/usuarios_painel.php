<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_painel extends MY_Admincontroller {

   /*
      TIPOS DE USUÁRIOS
      1 - Administrador (vê e faz tudo)
      2 - Videomaker (envia vídeos com e sem aprovação e alterar link para vídeos ao vivo)
      3 - Redator (mantêm notícias)
      4 - Avaliador (aprova vídeos e textos e mantêm os vídeo aprovados)
   */

   function __construct(){
   	parent::__construct();

      if(!isAdmin())
         redirect('painel/home');
      
   	$this->load->model('usuarios_painel_model', 'usuarios');
   }

   function index(){
   	$data['registros'] = $this->usuarios->pegarTodos();

      $data['titulo'] = 'Usuários';
      $data['unidade'] = "Usuário";
      $data['campo_1'] = "Usuário";
      $data['campo_2'] = "E-mail";
   	$this->load->view('painel/usuarios/lista', $data);
   }

   function form($id = false){
      if($id){
   	   $data['registro'] = $this->usuarios->pegarPorId($id);
         if(!$data['registro'])
            redirect('painel/usuarios_painel');
      }else{
         $data['registro'] = FALSE;
      }

      $data['titulo'] = 'Usuários';
      $data['unidade'] = "Usuário";
   	$this->load->view('painel/usuarios/form', $data);
   }

   function inserir(){
      if($this->usuarios->inserir()){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Usuário inserido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir usuário');
      }

   	redirect('painel/usuarios_painel/index', 'refresh');
   }

   function alterar($id){
      if($this->usuarios->alterar($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Usuário alterado com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar usuário');
      }     
   	redirect('painel/usuarios_painel', 'refresh');
   }

   function excluir($id){
	   if($this->usuarios->excluir($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Usuário excluido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir usuário');
      }

      if($this->session->userdata('id') == $id)
         redirect('painel/home/logout');

   	redirect('painel/usuarios_painel', 'refresh');
   }

}