<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download extends CI_controller {

	function __construct(){
		parent::__construct();

		if(!isAdmin())
			die('Acesso negado');
	}

    function index(){
      	$this->load->dbutil();
      	$this->load->helper('file');

      	$qry = <<<STR
SELECT nome,email,
case
  when id_especialidade = 1 then 'Alergia e Imunologia'
  when id_especialidade = 2 then 'Anestesiologia'
  when id_especialidade = 3 then 'Angiologia'
  when id_especialidade = 4 then 'Cardiologia'
  when id_especialidade = 5 then 'Cirurgia Vascular'
  when id_especialidade = 6 then 'Clinica Medica'
  when id_especialidade = 7 then 'Coloproctologia'
  when id_especialidade = 8 then 'Dermatologia'
  when id_especialidade = 9 then 'Endocrinologia e Metabologia'
  when id_especialidade = 10 then 'Gastroenterologia'
  when id_especialidade = 11 then 'Geriatria'
  when id_especialidade = 12 then 'Ginecologia e Obstetrícia'
  when id_especialidade = 13 then 'Hematologia e Hemoterapia'
  when id_especialidade = 14 then 'Infectologia'
  when id_especialidade = 15 then 'Mastologia'
  when id_especialidade = 16 then 'Nefrologia'
  when id_especialidade = 17 then 'Neurologia'
  when id_especialidade = 18 then 'Oftalmologia'
  when id_especialidade = 19 then 'Oncologia'
  when id_especialidade = 20 then 'Ortopedia e Traumatologia'
  when id_especialidade = 21 then 'Otorrinolaringologia'
  when id_especialidade = 22 then 'Pediatria'
  when id_especialidade = 23 then 'Pneumologia'
  when id_especialidade = 24 then 'Psiquiatria'
  when id_especialidade = 25 then 'Reumatologia'
  when id_especialidade = 26 then 'Urologia'
end as especialidade,
crm, pais,
case
  when idioma_interface = 1 then 'Português'
  when idioma_interface = 3 then 'Espanhol'  
end as idioma_interface,
case
  when idioma_conteudo = 1 then 'Português'
  when idioma_conteudo = 3 then 'Espanhol'
  when idioma_conteudo = 4 then 'Português e Espanhol'
end as idioma_conteudo,
DATE_FORMAT(DATE(data_cadastro), '%d/%m/%Y') as 'data de cadastro'
FROM usuarios ORDER BY nome ASC
STR;
      	$delimiter = ";";
      	$newline = "\r\n";
        
      	$query = $this->db->query($qry);

      	$csv = $this->dbutil->csv_from_result($query, $delimiter, $newline);
      	$name = 'relatorio_cadastros_'.date('d_m_Y').'_'.rand(0,50).'.csv';

      	if(!write_file('_imgs/rel/'.$name, utf8_decode($csv)))
      		die('erro ao escrever o arquivo');

      	header('Content-Description: File Transfer');
      	header('Content-Type: application/octet-stream; charset=UTF-8');
      	header('Content-Disposition: attachment; filename="'.$name.'"');
      	header("Content-Length: " . filesize('_imgs/rel/'.$name));
      	header('Pragma: no-cache');
      	if(readfile("_imgs/rel/".$name)){
        	@unlink("_imgs/rel/".$name);
      	}
    }   

}