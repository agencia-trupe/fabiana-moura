<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Linguagem extends CI_controller {
    
    function __construct() {
        parent::__construct();
    }

    function index($ling = 1) {
        $linguagens = array(1, 3);
        
        if($this->session->userdata('logado')){

            if(in_array($ling, $linguagens)){
                
                $this->db->set('idioma_interface', $ling)
                         ->where('id', $this->session->userdata('id'))
                         ->update('usuarios');

                $this->session->set_userdata('idioma_interface', $ling);

                if($this->session->userdata('redirect')){
                    redirect($this->session->userdata('redirect'));
                }else{
                    redirect('home');    
                }
            }else{
                redirect('home');
            }

        }else{
            
            if(in_array($ling, $linguagens)){
                $this->session->set_userdata('temp_lang', $ling);
            }
            redirect('publichome');
        }
    }
    
    function jstraduz(){
        echo traduz($this->input->post('string'), TRUE);
    }
    
}

?>