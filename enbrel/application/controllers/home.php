<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){

        $cont = $this->session->userdata('idioma_conteudo');
        
        if(isset($cont) && $cont && $cont != 4){

            $query = "SELECT * FROM videos WHERE (pre_aprovado = 1 OR (texto_aprovado = 1  AND video_aprovado = 1)) AND (id_idioma = $cont OR id_idioma = 4) ORDER BY data DESC LIMIT 0, 1";
            $video_destaque = $this->db->query($query)->result();
            
            $query = "SELECT * FROM videos WHERE (pre_aprovado = 1 OR (texto_aprovado = 1  AND video_aprovado = 1)) AND (id_idioma = $cont OR id_idioma = 4) ORDER BY data DESC LIMIT 1, 5";
            $data['video_lista'] = $this->db->query($query)->result();
            
            $query = "SELECT * FROM noticias WHERE id_idioma = $cont || id_idioma = 4 ORDER BY data DESC LIMIT 0, 3";
            $data['noticias'] = $this->db->query($query)->result();
            
        }else{

            $query = "SELECT * FROM videos WHERE (pre_aprovado = 1 OR (texto_aprovado = 1  AND video_aprovado = 1)) ORDER BY data DESC LIMIT 0, 1";
            $video_destaque = $this->db->query($query)->result();

            $query = "SELECT * FROM videos WHERE (pre_aprovado = 1 OR (texto_aprovado = 1  AND video_aprovado = 1)) ORDER BY data DESC LIMIT 1, 5";
            $data['video_lista'] = $this->db->query($query)->result();

            $query = "SELECT * FROM noticias ORDER BY data DESC LIMIT 0, 3";
            $data['noticias'] = $this->db->query($query)->result();
        }

        $data['video_destaque'] = $video_destaque[0];


        foreach ($data['noticias'] as $key => $value) {
            $query_imagens = $this->db->get_where('noticias_imagens', array('id_parent' => $value->id))->result();
            if(sizeof($query_imagens) > 0)
                $value->imagem = $query_imagens[0]->imagem;
            else
                $value->imagem = FALSE;
        }

        $qry_senha = $this->db->get_where('usuarios', array('id' => $this->session->userdata('id')))->result();

        $data['senha_usuario'] = $qry_senha[0]->senha;


        $data['aovivo'] = $this->db->get('video_aovivo', 1, 0)->result();

   		$this->load->view('home', $data);
    }

    function perfil(){

        $data['ESPECIALIDADES'] = array(
            'Alergia e Imunologia',
            'Anestesiologia',
            'Angiologia',
            'Cardiologia',
            'Cirurgia Vascular',
            'Clínica Médica',
            'Coloproctologia',
            'Dermatologia',
            'Endocrinologia e Metabologia',
            'Gastroenterologia',
            'Geriatria',
            'Ginecologia e Obstetrícia',
            'Hematologia e Hemoterapia',
            'Infectologia',
            'Mastologia',
            'Nefrologia',
            'Neurologia',
            'Oftalmologia',
            'Oncologia',
            'Ortopedia e Traumatologia',
            'Otorrinolaringologia',
            'Pediatria',
            'Pneumologia',
            'Psiquiatria',
            'Reumatologia',
            'Urologia'
        );
        
    	$this->headervar['load_css'] = 'cadastro';

    	$data['dados'] = $this->db->get_where('usuarios', array('id' => $this->session->userdata('id')))->result();

    	$query = $this->db->get_where('usuarios_temas', array('id_usuario' => $data['dados'][0]->id))->result();

    	$data['usuarios_temas'] = Array();
    	foreach ($query as $key => $value) {
    		$data['usuarios_temas'][] = $value->id_videos_categorias;
    	}

    	$data['temas'] = $this->db->get('videos_categorias')->result();

        $this->load->view('perfil', $data);
    }

    function atualizarCadastro(){
        $nome = $this->input->post('nome');
        $senha = $this->input->post('senha');
        $especialidade = $this->input->post('especialidade');
        $crm = $this->input->post('crm');
        $pais = $this->input->post('pais');
        $aceite = $this->input->post('aceite');
        $idioma_interface = $this->input->post('idioma_interface');
        $idioma_conteudo = $this->input->post('idioma_conteudo');
        $temas = $this->input->post('temas');

        if($nome == '' || $especialidade == '' || $crm == '' || $pais == '' || $aceite == ''){
            $this->session->set_flashdata('erro-form', 'Todos os campos são obrigatórios!');
            redirect('home/perfil', 'refresh');
        }else{

            $qry_senha = $this->db->get_where('usuarios', array('id' => $this->session->userdata('id')))->result();
            $senha_atual = $qry_senha[0]->senha;

            if($senha != '' && $senha_atual != criptografar($senha)){

                $ITV_pID = "fc34d063-8d9d-4e8d-958e-8f3a39e81525";
                $ITV_login = $this->session->userdata('email');
                $ITV_senha_antiga = $senha_atual;
                $ITV_senha_nova = criptografar($senha);

                $ITV = array(
                    'pID'   => $ITV_pID,
                    'login' => $ITV_login,
                    'senhaAntiga' => $ITV_senha_antiga,
                    'senhaNova' => $ITV_senha_nova
                );

                if(in_array('curl', get_loaded_extensions())) {
                    $url = 'http://itv.netpoint.com.br/reumatoonline/adm/updateUsuario.asp';
                    $ch = curl_init($url);
                     
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $ITV);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                     
                    $response = curl_exec($ch);
                    curl_close($ch);
                }
            }

            if(!$idioma_interface || $idioma_interface == '')
                $idioma_interface = default_lang();

            if(!$idioma_conteudo || $idioma_conteudo == '')
                $idioma_conteudo = default_lang();

            if($senha != '')
            	$this->db->set('senha', criptografar($senha));

            $this->db->set('nome', $nome)
                     ->set('id_especialidade', $especialidade)
                     ->set('crm', $crm)
                     ->set('pais', $pais)
                     ->set('idioma_interface', $idioma_interface)
                     ->set('idioma_conteudo', $idioma_conteudo)
                     ->where('id', $this->session->userdata('id'))
                     ->update('usuarios');

            $this->db->where('id_usuario', $this->session->userdata('id'))->delete('usuarios_temas');

            if($temas){
                foreach ($temas as $key => $value) {
                    $this->db->set('id_usuario', $this->session->userdata('id'))
                             ->set('id_videos_categorias', $value)
                             ->insert('usuarios_temas');
                }
            }

            $this->load->library('login_user');
            $this->login_user->update();

            redirect('home');
        }    	
    }

}