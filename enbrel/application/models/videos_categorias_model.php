<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos_categorias_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'videos_categorias';
		
		$this->dados = array(
			'titulo',
			'titulo_es',
			'titulo_en',
			'slug'
		);
		$this->dados_tratados = array(
			'slug'   => url_title($this->input->post('titulo'), '_', TRUE)
		);
	}

	function inserir(){

        $this->dados_tratados['slug'] = url_title($this->input->post("titulo"), '_', TRUE);

        $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $this->dados_tratados['slug']))->num_rows();
        $add = 1;
        $check_slug = $this->dados_tratados['slug'];
        while($consulta_slug != 0){
            $check_slug = $this->dados_tratados['slug'].'_'.$add;
            $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $this->dados_tratados['slug'] = $check_slug;

		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}
		$insert = $this->db->insert($this->tabela);
		$id_tema = $this->db->insert_id();
		
		$usuarios = $this->db->get('usuarios')->result();
		foreach ($usuarios as $value) {
			$this->db->set('id_usuario', $value->id)
					 ->set('id_videos_categorias', $id_tema)
					 ->insert('usuarios_temas');
		}

		return $insert;
	}

	function alterar($id){

        $this->dados_tratados['slug'] = url_title($this->input->post("titulo"), '_', TRUE);

        $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $this->dados_tratados['slug']))->num_rows();
        $add = 1;
        $check_slug = $this->dados_tratados['slug'];
        while($consulta_slug != 0){
            $check_slug = $this->dados_tratados['slug'].'_'.$add;
            $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $this->dados_tratados['slug'] = $check_slug;

		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}
			
			return $this->db->where('id', $id)->update($this->tabela);
		}
	}

	function excluir($id){
		$exc = $this->pegarPorId($id);
		if($exc !== FALSE){
			if($this->db->get_where('videos', array('id_tema' => $id))->num_rows() == 0){
				return $this->db->where('id', $id)->delete($this->tabela);	
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
}