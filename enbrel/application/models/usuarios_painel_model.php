<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_painel_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'usuarios_painel';

		if($this->input->post('password') != ''){

			$this->dados = array('username', 'password', 'email', 'tipo_usuario', 'nome');
			$this->dados_tratados = array(
				'password' => md5($this->input->post('password'))
			);
			
		}else{
			$this->dados = array('username', 'email', 'tipo_usuario', 'nome');
			$this->dados_tratados = array();
		}
		
	}

}