<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'videos';
		$this->tabela_log = 'videos_log';
	}

	function inserir(){

        $slug = url_title($this->input->post("titulo"), '_', TRUE);
        $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $slug))->num_rows();
        $add = 1;
        $check_slug = $slug;
        while($consulta_slug != 0){
            $check_slug = $slug.'_'.$add;
            $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $slug = $check_slug;

		$this->db->set("titulo", $this->input->post("titulo"))
				 ->set("slug", $slug)
		 		 ->set("id_tema", $this->input->post("id_tema"))
    			 ->set("id_idioma", $this->input->post("id_idioma"))
    			 ->set("data", formataData($this->input->post('data'), 'br2mysql'))
    			 ->set("url", $this->input->post("url"))
    			 ->set("olho", $this->input->post("olho"))
    			 ->set("texto", $this->input->post("texto"))
    			 ->set("palestrante", $this->input->post("palestrante"))
    			 ->set("id_autor", $this->session->userdata("id"))
    			 ->set("data_postagem", date('Y-m-d H:i:s'))
    			 ->set("thumbnail", "'".mysql_real_escape_string($this->videoThumbPrivado($this->input->post('url')))."'", FALSE);

    	$pre_aprovado = $this->input->post('pre_aprovado');

    	if(isset($pre_aprovado) && $pre_aprovado && $pre_aprovado == 1){
    		$this->db->set("video_aprovado", 1)
    				 ->set("texto_aprovado", 1)
    				 ->set("video_aprovado_por", $this->session->userdata("id"))
    				 ->set("video_aprovado_em", date('Y-m-d H:i:s'))
    				 ->set("texto_aprovado_por", $this->session->userdata("id"))
    				 ->set("texto_aprovado_em", date('Y-m-d H:i:s'))
    				 ->set("pre_aprovado", 1);
    	}

    	$insert = $this->db->insert($this->tabela);

    	$id = $this->db->insert_id();

    	$rels = $this->atualizaRelacionamentos($id);

    	return $insert && $rels;
    }

	function alterar($id){

        $slug = url_title($this->input->post("titulo"), '_', TRUE);
        $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $slug))->num_rows();
        $add = 1;
        $check_slug = $slug;
        while($consulta_slug != 0){
            $check_slug = $slug.'_'.$add;
            $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $slug = $check_slug;

		$this->db->set("titulo", $this->input->post("titulo"))
				 ->set("slug", $slug)
		 		 ->set("id_tema", $this->input->post("id_tema"))
    			 ->set("id_idioma", $this->input->post("id_idioma"))
    			 ->set("data", formataData($this->input->post('data'), 'br2mysql'))
    			 ->set("url", $this->input->post("url"))
    			 ->set("olho", $this->input->post("olho"))
    			 ->set("texto", $this->input->post("texto"))
    			 ->set("palestrante", $this->input->post("palestrante"))
    			 ->set("thumbnail", "'".mysql_real_escape_string($this->videoThumbPrivado($this->input->post('url')))."'", FALSE);

    	$update = $this->db->where('id', $id)->update($this->tabela);

    	$rels = $this->atualizaRelacionamentos($id);

    	return $update && $rels;
    }

    function videoThumbPrivado($url){
        if($url != (int) $url)
            $id_video = embed($url, 0, 0, TRUE);
        else
            $id_video = $url;

        $config = array(
            'consumer_key' => 'ef1e0b633859cefcc0223405ab82bd8633c8c855',
            'consumer_secret' => '9880d939f1368352907967401583d86ad12f8625',
            'token_secret' => '0544ec498ada0f06a878255ae3fd3f16d3c9155f',
            'token' => '63863f6ec16ca3cb2fa01e34662d7c6f'
        );

        $this->load->library('vimeo', $config);
        
        try {

            $video_obj = $this->vimeo->call("vimeo.videos.getInfo", array('video_id' => $id_video));            
            return $video_obj->video[0]->thumbnails->thumbnail[2]->_content;    

        } catch (Exception $e) {
            return '';
        }
    }

    function excluir($id){
    	if($this->pegarPorId($id) !== FALSE){
    		$this->db->where('id_video', $id)->delete('videos_relacionamentos');
			return $this->db->where('id', $id)->delete($this->tabela);
		}
    }

    function atualizaRelacionamentos($id_video){

    	$retorno = TRUE;

    	$rel_tipos = $this->input->post("rel_tipo");
    	$rel_enderecos = $this->input->post("rel_endereco");
        $rel_titulo = $this->input->post("rel_titulo");
    	$rel_noticias = $this->input->post("rel_noticia");
        $rel_washington = $this->input->post("rel_washington");

    	$this->db->where('id_video', $id_video)->delete("videos_relacionamentos");

    	foreach ($rel_tipos as $key => $value) {

    		if ($value == 'noticia' && isset($rel_noticias[$key]) && $rel_noticias[$key] != ''){

    			$ins = $this->db->set("id_video", $id_video)
    					    ->set("tipo_relacionamento", $value)
    					    ->set("endereco", $rel_noticias[$key])
    					    ->insert("videos_relacionamentos");
    			if(!$ins)
    				$retorno = FALSE;

    		}elseif($value == 'externo' && (isset($rel_enderecos[$key]) && $rel_enderecos[$key] != '') && (isset($rel_titulo[$key]) && $rel_titulo[$key] != '')){

    			$ins = $this->db->set("id_video", $id_video)
    					     ->set("tipo_relacionamento", $value)
                             ->set('titulo', $rel_titulo[$key])
    					     ->set("endereco", $rel_enderecos[$key])
    					     ->insert("videos_relacionamentos");

    			if(!$ins)
    				$retorno = FALSE;
    		}elseif($value == 'washington' && isset($rel_washington[$key]) && $rel_washington[$key] != ''){

                $ins = $this->db->set("id_video", $id_video)
                            ->set("tipo_relacionamento", $value)
                            ->set("endereco", $rel_washington[$key])
                            ->insert("videos_relacionamentos");
                if(!$ins)
                    $retorno = FALSE;
            }
    	}

    	return $retorno;
    }

    function publicar($id){
        if(!$id)
            return FALSE;

        return $this->db->set("video_aprovado", 1)
                        ->set("texto_aprovado", 1)
                        ->set("video_aprovado_por", $this->session->userdata("id"))
                        ->set("video_aprovado_em", date('Y-m-d H:i:s'))
                        ->set("texto_aprovado_por", $this->session->userdata("id"))
                        ->set("texto_aprovado_em", date('Y-m-d H:i:s'))
                        ->where('id', $id)
                        ->update($this->tabela);
    }

    function aprovarVideo($id){
        if(!$id)
            return FALSE;

        return $this->db->set("video_aprovado", 1)
                        ->set("video_aprovado_por", $this->session->userdata("id"))
                        ->set("video_aprovado_em", date('Y-m-d H:i:s'))
                        ->where('id', $id)
                        ->update($this->tabela);
    }

    function aprovarTexto($id){
        if(!$id)
            return FALSE;

        return $this->db->set("texto_aprovado", 1)
                        ->set("texto_aprovado_por", $this->session->userdata("id"))
                        ->set("texto_aprovado_em", date('Y-m-d H:i:s'))
                        ->where('id', $id)
                        ->update($this->tabela);
    }

    function pegarTodos(){
        return $this->db->order_by('data', 'desc')->get($this->tabela)->result();
    }

    function pegarPreAprovados(){
        return $this->db->query("SELECT * FROM ".$this->tabela." WHERE pre_aprovado = '1' ORDER BY data DESC")->result();
    }

    function pegarAprovados(){
        return $this->db->query("SELECT * FROM ".$this->tabela." WHERE pre_aprovado = '0' AND (texto_aprovado = '1' AND video_aprovado = '1') ORDER BY data DESC")->result();
    }
    
    function pegarAguardando(){
        return $this->db->query("SELECT * FROM ".$this->tabela." WHERE texto_aprovado = '0' OR video_aprovado = '0' ORDER BY data DESC")->result();
    }
}