<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Washington_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'washington';
		$this->tabela_imagens = 'washington_imagens';
		$this->tabela_log = 'washington_log';
		
		$this->dados = array(
			'id_categoria',
			'data',
			'titulo',
			'slug',
			'olho',
			'texto',
			'id_autor',
			'data_postagem',
			'id_idioma'
		);
		$this->dados_tratados = array(
			'data'     => formataData($this->input->post('data'), 'br2mysql'),
			'imagem'   => $this->sobeImagem(),
			'slug'     => url_title($this->input->post('titulo'), '_', TRUE),
			'data_postagem' => date('Y-m-d H:i:s')
		);

		$this->imagemOriginal = array(
			'dir' => '_imgs/washington/',
			'x' => '800',
			'y' => '600',
			'corte' => 'resize',
			'campo' => 'userfile'
		);

		$this->imagemThumb = array(
			'dir' => '_imgs/washington/thumbs/',
			'x' => '225',
			'y' => '135',
			'corte' => 'resize_crop',
			'campo' => 'userfile'
		);		
	}

	function inserir(){

        $this->dados_tratados['slug'] = url_title($this->input->post("titulo"), '_', TRUE);
        $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $this->dados_tratados['slug']))->num_rows();
        $add = 1;
        $check_slug = $this->dados_tratados['slug'];
        while($consulta_slug != 0){
            $check_slug = $this->dados_tratados['slug'].'_'.$add;
            $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $this->dados_tratados['slug'] = $check_slug;

		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}
		return $this->db->insert($this->tabela);
	}

	function alterar($id){

        $this->dados_tratados['slug'] = url_title($this->input->post("titulo"), '_', TRUE);
        $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $this->dados_tratados['slug']))->num_rows();
        $add = 1;
        $check_slug = $this->dados_tratados['slug'];
        while($consulta_slug != 0){
            $check_slug = $this->dados_tratados['slug'].'_'.$add;
            $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $this->dados_tratados['slug'] = $check_slug;
		
		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}
			
			return $this->db->where('id', $id)->update($this->tabela);
		}
	}

}