<?php

function traduz($label, $ajax = FALSE){
    
    $CI =& get_instance();

    if($CI->input->is_ajax_request()){

        if($CI->session->userdata('language'))
            $CI->lang->load($CI->session->userdata('language').'_site', $CI->session->userdata('language'));
        else{
            $id_lang = default_lang();
            if($id_lang == 1)
                $load = 'pt';
            else
                $load = 'es';

            $CI->lang->load($load.'_site', $load);
        }

    }
    
    $return = $CI->lang->line($label);
    
    if($return)
        return $return;
    else
        return $label;
        //return prefixo(str_repeat('non', (strlen($label) - 3) / 3));
}

function default_lang(){
    $ln = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0 , 2);
    
    if($ln == 'pt')
        return 1;
    elseif($ln == 'es')
        return 3;
    else
        return 4;
}

function id_linguagem($str){
    if(strpos('pt', $str) !== FALSE || strpos('PT', $str) !== FALSE){
        return 1;
    }elseif(strpos('en', $str) !== FALSE || strpos('EN', $str) !== FALSE){
        return 3;
    }elseif(strpos('es', $str) !== FALSE || strpos('ES', $str) !== FALSE){
        return 4;
    }else{
        return false;
    }
}

/*
 * Função para adicionar o prefixo definido na sessão de acordo com a linguagem
 */
function prefixo($arg){
    $CI =& get_instance();

    $lang = $CI->session->userdata('idioma_interface');
    switch ($lang) {
        default:
        case 1:
                return "pt_".$arg;
            break;
        case 3:
            return "es_".$arg;
            break;
    }
}

?>