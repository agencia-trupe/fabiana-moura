<?php

function isAdmin(){

	$CI =& get_instance();

	if($CI->session->userdata('tipo_usuario') == 1)
		return TRUE;
	else
		return FALSE;
}

function isVideomaker(){

	$CI =& get_instance();

	if($CI->session->userdata('tipo_usuario') == 2)
		return TRUE;
	else
		return FALSE;
}

function isRedator(){

	$CI =& get_instance();

	if($CI->session->userdata('tipo_usuario') == 3)
		return TRUE;
	else
		return FALSE;	
}

function isAvaliador(){

	$CI =& get_instance();
	
	if($CI->session->userdata('tipo_usuario') == 4)
		return TRUE;
	else
		return FALSE;
}

function criptografar($senha){
	$CI =& get_instance();
	return sha1($senha.$CI->config->item('encryption_key'));
}

function limpaSessao(){
	$CI =& get_instance();
	$CI->session->unset_userdata('busca-palestrante');
	$CI->session->unset_userdata('busca-tema');
	$CI->session->unset_userdata('busca-data-inicio');
	$CI->session->unset_userdata('busca-data-termino');
	$CI->session->unset_userdata('busca-ordem');
}

function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, strlen($alphabet)-1); //use strlen instead of count
        $pass[$i] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function logAcesso(){
	// Logar cada login
	// id_usuario, ip, timestamp

	$CI =& get_instance();

	$dados = array(
		'usuario'   => $CI->session->userdata('id'),
		'ip'        => $_SERVER['REMOTE_ADDR'],
		'timestamp' => date('Y-m-d H:i:s')
	);

	return $CI->db->set('usuario', $dados['usuario'])
			 	  ->set('ip', $dados['ip'])
			 	  ->set('timestamp', $dados['timestamp'])
			 	  ->insert('log_acessos');
}

function logAtividade(){
	// Logar cada acesso depois de logado
	// Usuario, Timestamp, IP address, Current URL, Referrer URL
	
	$CI =& get_instance();

	$dados = array(
		'usuario'   => $CI->session->userdata('id'),
		'ip'        => $_SERVER['REMOTE_ADDR'],
		'timestamp' => date('Y-m-d H:i:s'),
		'url_acessada' => current_url(),
		'url_origem'   => $_SERVER['HTTP_REFERER']
	);

	return $CI->db->set('usuario', $dados['usuario'])
			 	  ->set('ip', $dados['ip'])
			 	  ->set('timestamp', $dados['timestamp'])
			 	  ->set('url_acessada', $dados['url_acessada'])
			 	  ->set('url_origem', $dados['url_origem'])
			 	  ->insert('log_atividade');
}

function logAtividadeVideos(){
	$CI =& get_instance();

	$slug_video = $CI->uri->segment(3);

	$check = $CI->db->get_where('videos', array('slug' => $slug_video))->result();

	if(isset($check[0])){

		$dados = array(
			'usuario'   => $CI->session->userdata('id'),
			'ip'        => $_SERVER['REMOTE_ADDR'],
			'timestamp' => date('Y-m-d H:i:s'),
			'video_acessado' => $check[0]->id,
			'categoria' => $check[0]->id_tema
		);

		return $CI->db->set('usuario', $dados['usuario'])
				 	  ->set('ip', $dados['ip'])
				 	  ->set('timestamp', $dados['timestamp'])
				 	  ->set('video_acessado', $dados['video_acessado'])
				 	  ->set('categoria', $dados['categoria'])
				 	  ->insert('log_atividade_videos');	
	}
}

?>