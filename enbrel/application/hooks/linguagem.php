<?php

/*
 *  Hook pre-controller para setar a linguagem como pt caso
 *  não esteja definido nenhum valor
 * ALTERADO : Identifica a linguagem do navegador e seta como padrão, 
 * caso não encontre mostra a tela de seleção
 */
function inicia_linguagem(){

    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){

        $CI =& get_instance();

        if($CI->session->userdata('logado')){

            $CI->session->set_userdata('language', $CI->session->userdata('idioma_interface'));
            
        }else{
            
            $ln = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0 , 2);
            
            if($ln == 'pt')
                $padrao = '1';
            else
                $padrao = '3';

            if(!$CI->session->userdata('language') && !$CI->session->userdata('temp_lang'))
                $CI->session->set_userdata('language', $padrao);
            elseif($CI->session->userdata('temp_lang'))
                $CI->session->set_userdata('language', $CI->session->userdata('temp_lang'));

        }
        
        // Define o prefixo da tabela a ser usada dependendo da linguagem
        //$CI->session->set_userdata('prefixo', $CI->session->userdata('language').'_');
        if($CI->session->userdata('language') == 1){
            $ativar = "pt";
            $extenso = "portugues";
        }elseif($CI->session->userdata('language') == 3){
            $ativar = "es";
            $extenso = "espanhol";
        }

        $CI->lang->load($ativar.'_site', $ativar);

    }
}

/*
 *  Redirecionar de volta após requisição
 */
function grava_para_redirecionar(){

    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
        $CI =& get_instance();

        $CI->session->set_userdata('redirect', uri_string());
    }
}

?>