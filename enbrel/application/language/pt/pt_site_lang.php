<?php

$lang['EMAILCONTATO'] = 'contato@reumatonline.com';
$lang['Olá'] = 'Olá';
$lang['ACESSAR MEU CADASTRO | PREFERÊNCIAS'] = 'ACESSAR MEU CADASTRO | PREFERÊNCIAS';
$lang['sair'] = 'sair';
$lang['ver mais'] = 'ver mais';
$lang['ALTERAR MINHAS PREFERÊNCIAS DE EXIBIÇÃO'] = 'ALTERAR MINHAS PREFERÊNCIAS DE EXIBIÇÃO';
$lang['vídeos em ordem decrescente:'] = 'vídeos em ordem decrescente:';
$lang['TODOS OS VÍDEOS'] = 'TODOS OS VÍDEOS';
$lang['Ainda não sou cadastrado. Cadastrar'] = 'Ainda não sou cadastrado. Cadastrar';
$lang['esqueci minha senha'] = 'esqueci minha senha';
$lang['Cadastre-se!'] = 'Cadastre-se!';
$lang['Acesso a todos os conteúdos!'] = 'Acesso a todos os conteúdos!';
$lang['Agradecemos a compreensão.'] = 'Agradecemos a compreensão.';
$lang['LOGIN'] = 'LOGIN';
$lang['SENHA'] = 'SENHA';
$lang['voltar'] = 'voltar';
$lang['Ampliar'] = 'Ampliar';
$lang['notícia anterior'] = 'notícia anterior';
$lang['próxima notícia'] = 'próxima notícia';
$lang['Buscar outras notícias'] = 'Buscar outras notícias';
$lang['FILTRAR POR CATEGORIA DE NOTÍCIAS'] = 'FILTRAR POR CATEGORIA DE NOTÍCIAS';
$lang['Nenhuma Categoria'] = 'Nenhuma Categoria';
$lang['BUSCAR EM NOTÍCIAS'] = 'BUSCAR EM NOTÍCIAS';
$lang['Últimos vídeos do evento'] = 'Últimos vídeos do evento';
$lang['FAZER NOVA BUSCA DE VÍDEOS'] = 'FAZER NOVA BUSCA DE VÍDEOS';
$lang['Exibindo notícias de'] = 'Exibindo notícias de';
$lang['Últimas Notícias'] = 'Últimas Notícias';
$lang['Tema'] = 'Tema';
$lang['CONTEÚDO RELACIONADO'] = 'CONTEÚDO RELACIONADO';
$lang['notícias'] = 'notícias';
$lang['washington DC'] = 'washington DC';
$lang['links'] = 'links';
$lang['Assista Também'] = 'Assista Também';
$lang['Ordenar por'] = 'Ordenar por';
$lang['data decrescente'] = 'data decrescente';
$lang['data crescente'] = 'data crescente';
$lang['Filtrar por'] = 'Filtrar por';
$lang['Selecione'] = 'Selecione';
$lang['até'] = 'até';
$lang['Idioma'] = 'Idioma';
$lang['Exibindo apenas conteúdo em'] = 'Exibindo apenas conteúdo em';
$lang['Para alterar o idioma acesse seu cadastro alterando as'] = 'Para alterar o idioma acesse seu cadastro alterando as';
$lang['Minhas Preferências'] = 'Minhas Preferências';
$lang['FILTRAR'] = 'FILTRAR';
$lang['Exibindo os vídeos recentes com as preferências selecionadas'] = 'Exibindo os vídeos recentes com as preferências selecionadas';
$lang['Nenhum Vídeo Encontrado'] = 'Nenhum Vídeo Encontrado';
$lang['Saiba mais sobre Washington DC'] = 'Saiba mais sobre Washington DC';
$lang['Ver notícias sobre'] = 'Ver notícias sobre';
$lang['Informações sobre o evento:<br>localização, hospedagem e transporte'] = 'Informações sobre o evento:<br>localização, hospedagem e transporte';


$lang['VÍDEOS'] = 'VÍDEOS';
$lang['NOTÍCIAS'] = 'NOTÍCIAS';
$lang['ACR 2012'] = 'ACR 2012';
$lang['WASHINGTON DC'] = 'WASHINGTON DC';
$lang['INSTITUCIONAL'] = 'INSTITUCIONAL';
$lang['CADASTRO'] = 'CADASTRO';
$lang['buscar conteúdos'] = 'buscar conteúdos';

$lang['Todos os direitos reservados'] = 'Todos os direitos reservados';
$lang['Desenvolvimento:'] = 'Desenvolvimento:';

$lang['TEXTO_ACR2012'] = <<<STR
<h2>Foi iniciada a contagem regressiva para o ACR 2012 </h2>
<p>
	O Colégio Americano de Reumatologia (American College of Rheumatology, ACR) é a sociedade médica que visa estimular o progresso  contínuo na reumatologia. São promovidos frequentemente projetos de educação profissional para os reumatologistas de todo o mundo.
</p>
<p>
	Anualmente o Colégio realiza seu congresso, sempre no outono americano, cuja programação científica é devotada às doenças reumáticas. Esse grandioso encontro reúne milhares de reumatologistas e profissionais especializados em artrite reumatoide de todas as partes do globo. 
</p>
<p>
	Neste ano, o congresso acontecerá na capital americana, Washington D.C., entre os dias 10 e 14 de novembro, no Walter E. Washington Convention Center. Nos dias 9 e 10 acontecem os cursos pré-congresso.
</p>
STR;

$lang['TEXTO_INSTITUCIONAL'] = <<<STR
<h2>Sobre a iniciativa deste Portal</h2>
<p>
	Inovação, pesquisa, desenvolvimento e comprometimento com a saúde fazem parte do DNA da Pfizer desde o início de sua atuação. Dessa forma, ela é, mais uma vez, pioneira em oferecer aos médicos uma cobertura em tempo real do maior congresso da reumatologia: o ACR.
</p>
<p>
	Uma equipe composta por câmeras, produtores e jornalistas bilíngues já está preparando-se para embarcar em novembro em busca da realização de um grande projeto.
</p>
<p>
	O congresso termina dia 14 de novembro, mas nosso trabalho não para. Elaboraremos um jornal impresso com highlights dos temas de maior destaque e impacto na reumatologia, que será distribuído gratuitamente pela força de vendas da Pfizer.
</p>
<p>
	Se você é reumatologista ou tem um interesse particular pela área, não deixe de se cadastrar e acompanhar diariamente as novidades que prepararemos, sempre compromissados com a ética e com a notícia.
</p>
STR;


$lang['TEXTO_IDENTIFIQUE-SE'] = <<<STR
Caro Usuário,
<br><br>
Em conformidade com as regras de acesso da ANVISA, as informações aqui disponibilizadas são restritas aos profissionais médicos, por isso faz-se necessário o controle de acesso por meio do cadastramento prévio de usuários.
<br><br><br><br>
Caso já esteja cadastrado, basta informar os dados nos campos indicados.
STR;

$lang['TEXTO_CADASTRE-SE'] = <<<STR
Caso ainda não seja cadastrado, clique no botão abaixo:<br>
STR;

$lang['REGULAMENTO_ANVISA'] = <<<STR
<strong>REGULAMENTO ANVISA</strong> Em 17 de desembro de 2008 entrou em vigor a RDC N. 96/2008 que regulamenta sobre propagandas, mensagens publicitarias e promocionais e outras práticas, cujo objeto seja a divulgação, promoção e/ou comercializão de medicamentos de produção nacional e importados, em quaisquer formas e meios de sua veiculação. A integra da RDC N. 96/2008 pode ser obtida no site da ANVISA.
STR;

$lang['TEXTO_WASHINGTON_1'] = <<<EAD
<p>
	Local do evento: Walter E. Washington Convention Center.<br>
	801 Mount Vernon Place Northwest Washington,<br>
	DC 20001, Estados Unidos
</p>
<p>
	Tel.: (202) 249-3000
</p>
EAD;

$lang['TEXTO_WASHINGTON_2'] = <<<STR
<p>
	A comissão organizadora do congresso negociou tarifas especiais com 52 hotéis na região central de Washington. Para desfrutar de tais tarifas, a reserva dos quartos deve ser feita diretamente com o ACR Housing. Além disso, essa reserva garante também internet wi-fi gratuita, confirmação da reserva imediata por e-mail e apoio local da comissão organizadora. Não é necessário pagar no momento da reserva.
</p>
<p>
	Todos os hotéis do ACR Housing terão ônibus diários, em diversos horários, do hotel para o congresso e vice-versa, sem custo nenhum. Se você não estiver hospedado em um dos hotéis oficiais e deseja usufruir do ônibus, poderá adquirir um passe no valor de US$ 40.
</p>
<p>
	Os hotéis Renaissance, Grand Hyatt, Embassy Suites Convention Center, Hamptom Inn Convention Center, Henley Park Hotel, Morrisson Clark Inn são bem próximos ao centro de convenções, sendo possível se deslocar de um a outro caminhando.
</p>
<p>
	Os hotéis com tarifas mais econômicas (inferior a US$ 200 a diária) são o Comfort Inn Convention Center, One Washington Circle, Hotel Lombardy, Holiday Inn Capitol, Holiday Inn Central, Phoenix Park Hotel.
</p>
<p>
	Como os hotéis em Washington costumam ser pequenos, a tendência é que as vagas se esgotem rapidamente. Por isso recomenda-se que a reserva seja feita o quanto antes. Se você ainda não tem as datas de chegada e partida, não se preocupe, você poderá mudá-las posteriormente. E se precisar cancelar, você não será cobrado por isso, desde que comunique com 72 horas de antecedência.
</p>
<p>
	O Walter E. Washington Convention Center está conectado com as linhas amarela e verde do metrô, via a estação Convention Center, localizada na Mt. Vernon Square/7th. A estação Gallery Place/Chinatown, que atende as linhas vermelha, verde e amarela, fica a três quarteirões no sentido sul do centro de convenções.
</p>	
STR;

$lang['TEXTO_RECUPERAÇÃO_SENHA'] = 'Caro Usuário,<br><br>Informe o e-mail de cadastro abaixo para receber uma senha temporária';
$lang['RECUPERAÇÃO DE SENHA'] = 'RECUPERAÇÃO DE SENHA';

$lang['IDENTIFIQUE-SE'] = 'IDENTIFIQUE-SE';
$lang['Veja os vídeos das palestras'] = 'Veja os vídeos das palestras';
$lang['Conheça o ACR2012'] = 'Conheça o ACR2012';
$lang['Notícias de Washington DC'] = 'Notícias de Washington DC';
$lang['Institucional'] = 'Institucional';
$lang['Seu Cadastro'] = 'Seu Cadastro';

$lang['Acompanhe aqui'] = 'Acompanhe aqui';
$lang['transmissões ao vivo diretamente do evento em Washington DC.'] = 'transmissões ao vivo diretamente do evento em Washington DC.';
$lang['Transmissão Ao Vivo'] = 'Transmissão Ao Vivo';
$lang['TRANSMITINDO AGORA:'] = 'TRANSMITINDO AGORA:';
$lang['Palestra nome tal com o palestrante Cicrano de Souza da Silva Santos, especialista em Nome da Especialidade'] = 'Palestra nome tal com o palestrante Cicrano de Souza da Silva Santos, especialista em Nome da Especialidade';
$lang['ACONTECE NO ENBREL'] = 'ACONTECE NO ENBREL';

?>