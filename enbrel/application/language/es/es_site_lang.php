<?php

$lang['EMAILCONTATO'] = 'contacto@reumatonline.com';
$lang['Olá'] = '¡Hola';
$lang['ACESSAR MEU CADASTRO | PREFERÊNCIAS'] = 'MIS PREFERENCIAS';
$lang['sair'] = 'salir';
$lang['ver mais'] = 'ver más';
$lang['ALTERAR MINHAS PREFERÊNCIAS DE EXIBIÇÃO'] = 'CAMBIAR MIS PREFERENCIAS DE EXHIBICIÓN';
$lang['vídeos em ordem decrescente:'] = 'videos en orden descendente:';
$lang['TODOS OS VÍDEOS'] = 'TODOS LOS VIDEOS';
$lang['Ainda não sou cadastrado. Cadastrar'] = 'Todavía no estoy registrado. Registrarse';
$lang['esqueci minha senha'] = 'olvidé mi contraseña';
$lang['Cadastre-se!'] = 'Regístrese!';
$lang['Acesso a todos os conteúdos!'] = 'Acceso a todos los contenidos!';
$lang['Agradecemos a compreensão.'] = 'Agradecemos su comprensión.';
$lang['LOGIN'] = 'INICIAR SESIÓN';
$lang['SENHA'] = 'CONTRASEÑA';
$lang['voltar'] = 'volver';
$lang['Ampliar'] = 'Ampliar';
$lang['notícia anterior'] = 'noticias anterior ';
$lang['próxima notícia'] = 'próxima noticia ';
$lang['Buscar outras notícias'] = 'Buscar otras noticias';
$lang['FILTRAR POR CATEGORIA DE NOTÍCIAS'] = 'FILTRAR POR CATEGORÍAS DE NOTICIAS';
$lang['Nenhuma Categoria'] = 'No Categoría';
$lang['BUSCAR EM NOTÍCIAS'] = 'BUSCAR EN NOTICIAS';
$lang['Últimos vídeos do evento'] = 'Últimos videos del evento';
$lang['FAZER NOVA BUSCA DE VÍDEOS'] = 'HACER UNA NUEVA BÚSQUEDA DE VIDEOS';
$lang['Exibindo notícias de'] = 'Exhibiendo noticias de';
$lang['Últimas Notícias'] = 'Últimas Noticias';
$lang['Tema'] = 'Tema';
$lang['CONTEÚDO RELACIONADO'] = 'CONTENIDO RELACIONADO';
$lang['notícias'] = 'noticias';
$lang['washington DC'] = 'washington DC';
$lang['links'] = 'links';
$lang['Assista Também'] = 'Vea también';
$lang['Ordenar por'] = 'Ordenar por';
$lang['data decrescente'] = 'fecha decreciente';
$lang['data crescente'] = 'fecha creciente';
$lang['Filtrar por'] = 'Filtrar por';
$lang['Selecione'] = 'Seleccione';
$lang['palestrante'] = 'conferencista';
$lang['até'] = 'a';
$lang['Idioma'] = 'Idioma';
$lang['Data'] = 'Digite la fecha';
$lang['Exibindo apenas conteúdo em'] = 'Exhibir solo contenido en';
$lang['Para alterar o idioma acesse seu cadastro alterando as'] = 'Para cambiar el idioma ingrese a su registro cambiando sus preferencias';
$lang['Minhas Preferências'] = 'Mis preferencias';
$lang['FILTRAR'] = 'FILTRAR'; // TRADUZIR //
$lang['Exibindo os vídeos recentes com as preferências selecionadas'] = 'Exhibir los videos recientes de acuerdo con las preferencias seleccionadas';
$lang['Nenhum Vídeo Encontrado'] = 'Nenhum Vídeo Encontrado'; // TRADUZIR //
$lang['Saiba mais sobre Washington DC'] = 'Saiba mais sobre Washington DC'; // TRADUZIR //
$lang['Ver notícias sobre'] = 'Ver noticias sobre ';
$lang['Informações sobre o evento:<br>localização, hospedagem e transporte'] = 'Informaciones sobre el evento:<br>localización, hospedaje y transporte';


$lang['VÍDEOS'] = 'VIDEOS';
$lang['NOTÍCIAS'] = 'NOTICIAS';
$lang['ACR 2012'] = 'ACR 2012';
$lang['WASHINGTON DC'] = 'WASHINGTON DC';
$lang['INSTITUCIONAL'] = 'INSTITUCIONAL';
$lang['CADASTRO'] = 'REGISTRO';
$lang['buscar conteúdos'] = 'buscar contenidos';

$lang['Todos os direitos reservados'] = 'Todos los derechos reservados';
$lang['Desenvolvimento:'] = 'Desarrollo:';

$lang['TEXTO_ACR2012'] = <<<STR
<h2>Inició la cuenta regresiva para el ACR 2012</h2>
<p>
	El Colegio Americano de Reumatología (American College of Rheumatology, ACR) y la sociedad médica tienen como objetivo estimular el progreso continuo en reumatología. Con frecuencia son promovidos proyectos de educación profesional para reumatólogos de todo el mundo.
</p>
<p>
	Anualmente, el Colegio lleva a cabo su congreso, siempre en el otoño americano, cuya programación científica está dedicada a las enfermedades reumáticas. Este grandioso encuentro reúne millares de reumatólogos y profesionales especializados en artritis reumatoide de todas las partes del globo.
</p>
<p>
	Este año, el congreso tendrá lugar en la capital estadounidense, Washington D.C., entre los días 10 y 14 de noviembre en el Walter E. Washington Convention Center. Los días 9 y 10 de noviembre se llevarán a cabo los cursos precongreso.
</p>
STR;

$lang['TEXTO_INSTITUCIONAL'] = <<<STR
<h2>Texto institucional sobre la iniciativa de este portal</h2>
<p>
	Innovación, investigación, desarrollo y compromiso con la salud forman parte del ADN de Pfizer desde el inicio de su actuación. De esta forma, ella es, una vez más, pionera en ofrecer a los médicos una cobertura en tiempo real del mayor congreso de reumatología: el ACR.
</p>
<p>
	Un equipo compuesto por cámaras, productores y periodistas bilingües ya se está preparando para iniciar el noviembre la mejor realización un gran proyecto.
</p>
<p>
	Diariamente serán subidos a la red videos con entrevistas a renombrados personajes y también con el punto de vista de los participantes sobre el evento. Los periodistas también publicarán textos con los principales destacados del día. Detalle: todos los contenidos estarán disponibles en español y en portugués.
</p>
<p>
	El congreso termina el 14 de noviembre, pero nuestro trabajo no se detiene allí. Elaboraremos un periódico impreso con destacados de los temas más relevantes en reumatología, que será distribuido de forma gratuita gracias a la fuerza de ventas de Pfizer.
</p>
<p>
	Si usted es reumatólogo o tiene un interés particular por el área, no deje de registrarse y acompañe diariamente las novedades que prepararemos, siempre comprometidos con la ética de la noticia.
</p>
STR;


$lang['TEXTO_IDENTIFIQUE-SE'] = <<<STR
Querido usuario,
<br><br>
De conformidad con las reglas de acceso de ANVISA, la información aquí depositada estará restringida para los profesionales médicos; por eso es necesario el control de acceso mediante el registro previo de los usuarios.
<br><br><br><br>
En caso de que ya esté registrado, basta informar los datos indicados en cada campo.
STR;

$lang['TEXTO_CADASTRE-SE'] = <<<STR
Caso ainda não seja cadastrado, clique no botão abaixo:<br>
STR;

$lang['REGULAMENTO_ANVISA'] = <<<STR
<strong>REGULACIÓN ANVISA</strong> El 17 de diciembre de 2008 entró en vigencia la RDC No.96/2008 que regula la propaganda, los mensajes publicitarios y promocionales y otras prácticas, cuyo objetivo sea la divulgación y/o la comercialización de medicamentos de producción nacional e importados, en cualesquier forma y medios de vinculación. La totalidad de la RDC No.96/2008 está disponible en el sitio web de ANVISA.
STR;

$lang['TEXTO_WASHINGTON_1'] = <<<EAD
<p>
	Sede del evento: Walter E. Washington Convention Center.<br>
	801 Mount Vernon Place Northwest Washington,<br>
	DC 20001, Estados Unidos
</p>
<p>
	Tel.: (202) 249-3000
</p>
EAD;

$lang['TEXTO_WASHINGTON_2'] = <<<STR
<p>
	La comisión organizadora del congreso negoció tarifas especiales con 53 hoteles en la región central del Washington. Para disfrutar de tales tarifas, la reserva de las habitaciones debe hacerse directamente con el ACR Housing. Además de eso, esa reserva también garantiza servicio de Internet inalámbrico gratuito, confirmación de la reserva inmediata vía e-mail y apoyo local de la comisión organizadora. No es necesario pagar en el momento de la reserva.
</p>
<p>
	Todos los hoteles del ACR Housing tendrán autobuses diarios, en diversos horarios, desde el hotel hasta la sede del congreso y viceversa, sin ningún costo. Si usted no está hospedad en ninguno de los hoteles oficiales y desea utilizar el servicio de autobús, podrá adquirir un pase por valor de US$ 40.
</p>
<p>
	Los hoteles Renaissance, Grand Hyatt, Embassy Suites Convention Center, Hamptom Inn Convention Center, Henley Park Hotel y el Morrisson Clark Inn están cercanos al centro de convenciones, siendo posible ir a pie de un lugar a otro.
</p>
<p>
	Los hoteles con tarifas más económicas (inferiores a US$ 200 diarios) son el Comfort Inn Convention Center, One Washington Circle, Hotel Lombardy, Holiday Inn Capitol, Holiday Inn Central y el Phoenix Park Hotel.
</p>
<p>
	Como los hoteles en Washington acostumbran a ser pequeños, la tendencia es que los cupos se agoten rápidamente. Por eso se recomienda que la reserva se realice cuanto antes. Si usted no tiene las fechas de llegada y salida, no se preocupe, podrá modificarlas posteriormente. Y si necesita cancelar, eso no tendrá ningún costo, siempre y cuando se comunique con 72 horas de anticipación.
</p>
<p>
	El Walter E. Washington Convention Center está conectado con las líneas amarillas y verde del metro, vía a la estación Convention Center, localizada en la Mt. Vernon Square/7th. La estación Gallery Place/Chinatown, donde operan las líneas roja, verde y amarilla, está ubicada a tres cuadras en sentido sur del centro de convenciones.
</p>	
STR;

$lang['TEXTO_RECUPERAÇÃO_SENHA'] = 'Querido usuario,<br><br>Ingrese el mail de registro abajo para recibir una contraseña temporal';
$lang['RECUPERAÇÃO DE SENHA'] = 'RECUPERACIÓN DE CONTRASEÑA';

$lang['IDENTIFIQUE-SE'] = 'IDENTIFÍQUESE';
$lang['Veja os vídeos das palestras'] = 'Veja os vídeos das palestras'; // TRADUZIR //
$lang['Conheça o ACR2012'] = 'Conheça o ACR2012'; // TRADUZIR //
$lang['Notícias de Washington DC'] = 'Notícias de Washington DC'; // TRADUZIR //
$lang['Institucional'] = 'Institucional'; // TRADUZIR //
$lang['Seu Cadastro'] = 'Seu Cadastro'; // TRADUZIR //

$lang['Acompanhe aqui'] = 'Acompanhe aqui'; // TRADUZIR //
$lang['transmissões ao vivo diretamente do evento em Washington DC.'] = 'Siga aquí las transmisiones en vivo del evento en Washington D. C.';
$lang['Transmissão Ao Vivo'] = 'Transmissão Ao Vivo'; // TRADUZIR //
$lang['TRANSMITINDO AGORA:'] = 'TRANSMITIENDO AHORA:';
$lang['Palestra nome tal com o palestrante Cicrano de Souza da Silva Santos, especialista em Nome da Especialidade'] = 'Palestra nome tal com o palestrante Cicrano de Souza da Silva Santos, especialista em Nome da Especialidade'; // TRADUZIR //
$lang['ACONTECE NO ENBREL'] = '¿QUÉ OCURRE EN EL ACR 2012?';

$lang['ACONTECEU NO ACR2012'] = '¿QUÉ OCURRE EN EL ACR 2012?';
$lang['Não há vídeos sendo transmitidos no momento.'] = 'No hay videos transmitida en el momento.';

$lang['SALVAR PREFERÊNCIAS'] = 'Guardar preferencias';
$lang['CADASTRAR'] = 'REGÍSTRESE';

$lang['Nenhum resultado encontrado para'] = 'Ningún resaltado encontrado para';
?>