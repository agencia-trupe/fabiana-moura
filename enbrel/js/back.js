$(document).ready( function(){

    $('.voltar').click( function(){ window.history.back(); });

    $('#datepicker').datepicker();

    if($('.mostrarerro').length || $('.mostrarsucesso').length){
        console.log('erro')
        $('.mensagem').show('normal', function(){
            $(this).delay(4000).slideUp('normal');
        })
    }

    $('input[type="radio"]').change( function(){
        $('input[type="radio"]').each( function(){
            if($(this).is(':checked')){
                $(this).parent().css('font-weight', 'bold');
            }else{
                $(this).parent().css('font-weight', 'normal');
            }
        });
    });

    $('input[type="radio"]:checked').each( function(){
        $(this).parent().css('font-weight', 'bold');
    });

    $('#aprovacao-video input[type="radio"]').change( function(){
        if($('#hidden-part').is(':hidden'))
            $('#hidden-part').fadeIn();
    });

    $('.delete').click( function(){
        if(!confirm('Deseja Excluir o Registro ?'))
            return false;
    });

    $('table.relat tr.canclick').click( function(){
        var atual = $(this);
        if($('.hid-table:visible').length){
            $('.hid-table:visible').slideUp('normal', function(){
                atual.next('.hid-table').slideDown('normal');
            });
        }else{
            atual.next('.hid-table').slideDown('normal');
        }
    });

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "texto",
        theme : "advanced",
        theme_advanced_buttons1 : "bold,italic,underline,bullist,jbimages,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,link,unlink,separator,fullscreen",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_blockformats : "h1, h2, p",
        theme_advanced_toolbar_location : "top",
        plugins: "paste,jbimages,fullscreen",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
        width : 850,
        height: 500
    });

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "texto-semimg",
        theme : "advanced",
        theme_advanced_buttons1 : "bold,italic,underline,bullist,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,link,unlink,separator,fullscreen",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_blockformats : "h1, h2, p",
        theme_advanced_toolbar_location : "top",
        plugins: "paste,fullscreen",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
        width : 850,
        height: 500
    });    

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "olho",
        theme : "advanced",
        theme_advanced_buttons1 : "bold,italic,underline,bullist,separator,link,unlink",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_blockformats : "h1, h2, p",
        theme_advanced_toolbar_location : "top",
        plugins: "paste",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
        width : 650,
        height: 150
    }); 


});