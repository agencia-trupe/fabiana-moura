function abreRecuperarSenha(){
    $.fancybox(BASE+'login/recuperacao',{
      padding       : 50,
      modal         : false,
      width         : 800,
      height        : 500,
      overlayColor  : '#445779',
      type          : 'iframe'
    });	
}


$('document').ready( function(){

	if($('#banners-home img').length > 1){
		$('#banners-home').cycle();
	}

	if($('.datepicker').length > 0){
		$('.datepicker').datepicker();
	}

	if($('#cycle-this ul').length > 1){
		$('#cycle-this').cycle({
			timeout : 0,
			fx : 'scrollHorz',
			next : $('#botao-proxima a'),
			after : function onAfter(curr, next, opts, fwd) {
				var index = opts.currSlide;
				var $ht = $(this).height();
				$(this).parent().animate({height: $ht});
			}
		});
	}else{
		$('#botao-proxima').hide();
	}		

	if($('.fancy').length){
		$('.fancy').fancybox({
			titleShow : false
		});
	}

	$("a[href='recuperacao']").on('click', function(e){
      e.preventDefault();
      abreRecuperarSenha();
    });

});