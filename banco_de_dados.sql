CREATE DATABASE  IF NOT EXISTS `fabiana_moura` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `fabiana_moura`;
-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: fabiana_moura
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `projetos_imagens`
--

DROP TABLE IF EXISTS `projetos_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_parent` int(10) unsigned NOT NULL,
  `imagem` varchar(280) NOT NULL,
  `ordem` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos_imagens`
--

LOCK TABLES `projetos_imagens` WRITE;
/*!40000 ALTER TABLE `projetos_imagens` DISABLE KEYS */;
INSERT INTO `projetos_imagens` VALUES (40,1,'decoracao-boneca-princesa---close-mesa.jpg',3),(41,1,'festa_infantil_cavalos_-_vista_mesa_de_doces.jpg',15),(43,1,'dsc_0231.jpg',0),(52,3,'decoracao_batizado_branco_e_rosa_-_bem_casados.jpg',4),(53,3,'decoracao_batizado_branco_e_rosa_-_mesa_de_doces_.jpg',0),(54,3,'decoracao_batizado_branco_e_rosa_-_salao.jpg',3),(55,3,'img_6277.jpg',2),(60,3,'img_6349.jpg',1),(61,3,'img_6435para_o_site.jpg',5),(62,2,'decoracao_jardim_passaros_passarinhos_-_agua_personalizada.jpg',1),(63,2,'decoracao_jardim_passaros_passarinhos_-_brigadeiro_limao.jpg',2),(64,2,'decoracao_jardim_passaros_passarinhos_-_brownies.jpg',7),(65,2,'decoracao_jardim_passaros_passarinhos_-_mesa_frente.jpg',0),(67,2,'decoracao_jardim_passaros_passarinhos_-_cookie_biscoito_no_palito_decorado.jpg',4),(68,2,'decoracao_jardim_passaros_passarinhos_-_passarinho_verde_tecido.jpg',5),(70,2,'img_1165.jpg',3),(71,1,'festa_circo_brigadeiros.jpg',2),(72,1,'festa_circo_bichinhos_dadinhos.jpg',1),(73,1,'festa_baby_navy_ursinho_marinheiro_mesa_03.jpg',5),(74,1,'festa_baby_navy_ursinho_marinheiro_latinhas_mint_to_be.jpg',6),(75,1,'decoracao_boneca_princesa_-_boneca_princesa2.jpg',4),(76,1,'decoracao_menina_lilas_azul_turquesa_prata_-_detalhes_globos_de_espelho.jpg',13),(77,1,'decoracao_menina_lilas_azul_turquesa_prata_-_cupcakes.jpg',14),(78,1,'festa_infantil_cavalos_-_roda_de_carroca_e_botas_cowboy.jpg',16),(79,1,'festa_infantil_cavalos_-_cavalo_feno_ferradura.jpg',17),(80,1,'festa_decoracao_fazendinha_-_detalhe_mesa_de_doces.jpg',10),(83,1,'festa_decoracao_fazendinha_-_carroca_girassol.jpg',12),(84,1,'decoracao_menina_rosa_pink_sharpay_-_mesa_inteira.jpg',7),(85,1,'decoracao_menina_rosa_pink_sharpay_-_sapatinhos_cristal.jpg',9),(86,1,'decoracao_menina_rosa_pink_sharpay_-_detalhes_cupcakes.jpg',8),(87,1,'img_1401para_o_site.jpg',11),(88,5,'decoracao_bodas_de_perola_-_mesa_de_doces_2.jpg',0),(89,5,'decoracao_bodas_de_perola_-_copos_de_leite.jpg',3),(90,5,'decoracao_bodas_de_perola_-_delphinium_e_prato_raso_com_perolas.jpg',1),(91,5,'decoracao_bodas_de_perola_-_saches_e_gaiola.jpg',5),(92,5,'decoracao_bodas_de_perola_-_garrafas_e_colares_de_perola.jpg',2),(93,5,'decoracao_bodas_de_perola_-_brownies.jpg',4),(94,6,'decoracao_15_anos_debutante_-_hall_de_entrada_frente.jpg',0),(95,6,'decoracao_15_anos_debutante_-_detalhe_porta_guardanapo.jpg',2),(96,6,'decoracao_15_anos_debutante_-_lounge_sofas.jpg',5),(97,6,'decoracao_15_anos_debutante_-_mesa_de_chocolates_frente2.jpg',3),(98,6,'decoracao_15_anos_debutante_-_passarela_adesivada.jpg',1),(99,6,'decoracao_15_anos_debutante_-_detalhes_chocolates.jpg',4),(100,6,'decoracao_15_anos_debutante_-_pista_de_danca.jpg',6),(101,7,'bandeirolas_trudas_no_palito.jpg',5),(102,7,'geleias_na_maletinha.jpg',6),(103,7,'bolinhas_bleu_balcn_rouge.jpg',7),(104,7,'maleta_paris.jpg',8),(106,7,'decoracao_cha_bar_bebidas_marrom_bege_fendi_-_mesa.jpg',0),(107,7,'decoracao_cha_bar_bebidas_marrom_bege_fendi_-_bombinha_doce_de_leite.jpg',1),(108,7,'decoracao_cha_bar_bebidas_marrom_bege_fendi_-_lembrancinha_mini_cachaca_personalizada.jpg',3),(109,7,'decoracao_cha_bar_bebidas_marrom_bege_fendi_-_garrafas_e_barril.jpg',4),(110,7,'img_1543_para_o_site.jpg',2),(111,8,'decoracao_jantar_aniversario_roxo_violeta_amarelo_e_azul_-_arranjo_lirios.jpg',5),(113,8,'decoracao_jantar_aniversario_roxo_violeta_amarelo_e_azul_-_ilha_degustacao_03.jpg',4),(114,8,'decoracao_jantar_aniversario_roxo_violeta_amarelo_e_azul_-_cake_pops.jpg',7),(115,8,'decoracao_jantar_aniversario_roxo_violeta_amarelo_e_azul_-_mesa_de_doces.jpg',6),(116,8,'decoracao_jantar_aniversario_roxo_violeta_amarelo_e_azul_-_mesa_jantar.jpg',8),(117,8,'decoracao_jantar_aniversario_homem_-_ilha_de_degustacao.jpg',0),(118,8,'decoracao_jantar_aniversario_homem_-_arranjo_orquideas.jpg',1),(119,8,'decoracao_jantar_aniversario_homem_-_arranjo_orquideas_imersas_na_agua.jpg',2),(120,8,'decoracao_jantar_aniversario_homem_-_detalhe_bolo_e_arranjp.jpg',3),(121,4,'decoracao_casamento_bucolico_-_caminho_da_noiva.jpg',0),(122,4,'centro_de_mesa_lata_revestida_e_lanterna.jpg',0),(123,4,'decoracao_casamento_bucolico_-_detalhe_bem_casados.jpg',0),(124,4,'decoracao_casamento_bucolico_-_detalhes_gaiolas.jpg',0),(125,4,'decoracao_casamento_bucolico_-_escada_bem_casados_2.jpg',0),(126,4,'decoracao_casamento_bucolico_-_mesa_de_doces_frente.jpg',0),(127,4,'casamento_decoracao_azul_fendi_branco_-_aparador_bem_casadosedit.jpg',0),(128,4,'casamento_decoracao_azul_fendi_branco_-_mesa_de_jantar.jpg',0),(129,4,'casamento_decoracao_azul_fendi_branco_-_lembrancinha_noivinhos.jpg',0),(130,4,'casamento_decoracao_azul_fendi_branco_-_mesa_de_docesmenor.jpg',0);
/*!40000 ALTER TABLE `projetos_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `telefone` varchar(18) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'(11) 98279.9770','contato@fabianamoura.com.br','www.facebook.com/FabianaMouraProjetosPersonalizados');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `midia`
--

DROP TABLE IF EXISTS `midia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `midia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `area` varchar(140) NOT NULL,
  `titulo` varchar(140) NOT NULL,
  `data` date NOT NULL,
  `imagem` varchar(300) NOT NULL,
  `externo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `midia`
--

LOCK TABLES `midia` WRITE;
/*!40000 ALTER TABLE `midia` DISABLE KEYS */;
INSERT INTO `midia` VALUES (14,'infantil','Decoração de Festas Infantis - Editora Casa Dois - Ed n52','2012-11-01','capa_decoracao_de_festas_infantis.jpg','0'),(15,'infantil','Teste externo','2012-02-01','stroker_camaro-69_ssrs_4s.jpg','http://www.google.com');
/*!40000 ALTER TABLE `midia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slides`
--

DROP TABLE IF EXISTS `slides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(280) NOT NULL,
  `ordem` int(10) unsigned NOT NULL DEFAULT '0',
  `area` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slides`
--

LOCK TABLES `slides` WRITE;
/*!40000 ALTER TABLE `slides` DISABLE KEYS */;
INSERT INTO `slides` VALUES (6,'img_4035.jpg',3,'infantil'),(9,'img_1098edit.jpg',5,'infantil'),(10,'img_1255edit.jpg',0,'infantil'),(12,'mari.jpg',4,'infantil'),(13,'img_1454edit.jpg',1,'adulto'),(14,'img_1599edit.jpg',6,'adulto'),(15,'img_2006edit.jpg',0,'adulto'),(18,'img_3938edit.jpg',4,'adulto'),(19,'img_4537edeit.jpg',5,'adulto'),(21,'0',0,''),(22,'0',0,''),(23,'0',0,''),(25,'circo_para_home_02.jpg',1,'infantil'),(26,'pipoquinhas.jpg',2,'infantil');
/*!40000 ALTER TABLE `slides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `midia_imagens`
--

DROP TABLE IF EXISTS `midia_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `midia_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_parent` int(10) unsigned NOT NULL,
  `imagem` varchar(280) NOT NULL,
  `ordem` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `midia_imagens`
--

LOCK TABLES `midia_imagens` WRITE;
/*!40000 ALTER TABLE `midia_imagens` DISABLE KEYS */;
INSERT INTO `midia_imagens` VALUES (13,14,'decoracao_de_festas_infantis_p40_e_41.jpg',0);
/*!40000 ALTER TABLE `midia_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto_empresa` text NOT NULL,
  `texto_fabiana` text NOT NULL,
  `texto_outro` text NOT NULL,
  `area` varchar(45) NOT NULL,
  `imagem` varchar(280) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil`
--

LOCK TABLES `perfil` WRITE;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` VALUES (1,'Seguindo uma forte tendência do mercado, a empresa oferece projetos de decoração totalmente personalizados e exclusivos para festas e eventos traduzindo com muito cuidado e dedicação os sonhos e desejos de cada cliente. Tudo para fugir do óbvio e evitar reprises, os projetos são pensados em cada detalhe, respeitando gostos e tendências sendo concretizados em ambientações mágicas e encantadoras. Prezamos por um atendimento acolhedor e pessoal e por um resultado de qualidade e excelência.','A vocação de Fabiana para o universo das artes e criação foi estimulada desde a infância, quando esteve envolvida em cursos de música, dança e artes plásticas. Cultivou sempre um interesse particular pela decoração direcionando facilmente sua futura opção profissional. Formada pela faculdade de Arquitetura e Urbanismo Mackenzie, iniciou sua carreira atuando na áera de design de interiores quando em 2009 migrou para o segmento de decoração de eventos onde encontrou sua verdadeira paixão.\nHoje, Fabiana realiza-se plenamente transformando a celebração de datas importantes em eventos especiais e inesquecíveis.','Nossos projetos\nseguem uma linha de design\ndiferenciada; são projetos\nrealizados com uma identidade\nvisual, com um conceito atrelado, com\no cuidado sobre o detalhes obtendo\nassim um resultado muito\noriginal, harmônico e de\nbom gosto.','infantil','infantil_fabiana-sem_borda.png'),(2,'Seguindo uma forte tendência do mercado, a empresa oferece projetos de decoração totalmente personalizados e exclusivos traduzindo com muito cuidado e dedicação os sonhos e desejos de cada cliente. Tudo para fugir do óbvio e evitar reprises, os projetos são pensados em cada detalhe, sempre respeitando gostos e tendências sendo concretizados em ambientações mágicas e encantadoras. Prezamos por um atendimento acolhedor e pessoal e por um resultado de qualidade e excelência.','A vocação de Fabiana para o universo das artes e criação foi estimulada desde a infância, quando esteve envolvida em cursos de música, dança e artes plásticas. Cultivou sempre um interesse particular pela decoração direcionando facilmente sua futura opção profissional. Formada pela faculdade de Arquitetura e Urbanismo Mackenzie, iniciou sua carreira atuando na áera de design de interiores quando em 2009 migrou para o segmento de decoração de eventos onde encontrou sua verdadeira paixão.\nHoje, Fabiana realiza-se plenamente transformando a celebração de datas importantes em eventos especiais e inesquecíveis.','Nossos projetos\nseguem uma linha de design\ndiferenciada; são projetos\nrealizados com uma identidade\nvisual, com um conceito atrelado, com\no cuidado sobre o detalhes obtendo\nassim um resultado muito\noriginal, harmônico e de\nbom gosto.','adulto','adulto_fabiana.jpg');
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(140) NOT NULL,
  `email` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `NomeUsuario` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (15,'trupe','d32c9694c72f1799ec545c82c8309c02','contato@email.com'),(16,'fabimoura','23cc16bc0c88b3dff86bdcd9e3bb4605','contato@bouquetboutique.com.br');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos`
--

DROP TABLE IF EXISTS `projetos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `slug` varchar(45) NOT NULL,
  `area` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos`
--

LOCK TABLES `projetos` WRITE;
/*!40000 ALTER TABLE `projetos` DISABLE KEYS */;
INSERT INTO `projetos` VALUES (1,'Aniversários','aniversarios','infantil'),(2,'Chás','chas','infantil'),(3,'Batizados','batizados','infantil'),(4,'Casamentos','casamentos','adulto'),(5,'Bodas','bodas','adulto'),(6,'15 anos','15_anos','adulto'),(7,'Chás','chas','adulto'),(8,'Aniversários','aniversarios','adulto');
/*!40000 ALTER TABLE `projetos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-20 11:11:11
